#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>

enum visualization_style{HEIGHT_COLORED, UNIFORM, GRAY_SCALE, RGB_CUBE, SEMANTIC_LABEL};

class visualizer{

public:
    visualizer(ros::NodeHandle nh,params *params);
    visualizer(ros::NodeHandle nh,params *params,mapping_dict semantic_dict);
    visualizer(ros::NodeHandle nh,params *params,mapping_dict semantic_dict,grid_functions *functions);
    void visualize_pointcloud(scansirgblabel_with_pose &pointcloud);
    void labeled_pointcloud(scansirgblabel_with_pose &pointcloud);
    void add_grid_cells(grid_cells cells);
    void remove_grid_cells();
    void visualize();
    void visualize_cubes();
    void visualize_test();
    std_msgs::ColorRGBA assign_color(double height);
    std_msgs::ColorRGBA assign_color(value &cell, double height);
    mapping_dict semantic_dict_;

private:
    ros::NodeHandle nh_;
    ros::Publisher vis_pub_,vis_pub_pointcloud_,vis_pub_labeledcloud_;
    grid_functions *mygrid_fns;
    params *params_;
    grid_cells cells_;
    visualization_style style_;
};

visualizer::visualizer(ros::NodeHandle nh,params *params):nh_(nh),params_(params){
    vis_pub_=nh_.advertise<visualization_msgs::Marker>("RMAP",1);
    vis_pub_pointcloud_ = nh.advertise<sensor_msgs::PointCloud> ("Point_cloud", 1);
    vis_pub_labeledcloud_ = nh.advertise<sensor_msgs::PointCloud> ("Labeled_point_cloud", 1);
    style_ = SEMANTIC_LABEL;
}

visualizer::visualizer(ros::NodeHandle nh,params *params,mapping_dict semantic_dict):nh_(nh),params_(params),semantic_dict_(semantic_dict){
    vis_pub_=nh_.advertise<visualization_msgs::Marker>("RMAP",1);
    vis_pub_pointcloud_ = nh.advertise<sensor_msgs::PointCloud> ("Point_cloud", 1);
    vis_pub_labeledcloud_ = nh.advertise<sensor_msgs::PointCloud> ("Labeled_point_cloud", 1);
    style_ = SEMANTIC_LABEL;
}

visualizer::visualizer(ros::NodeHandle nh,params *params,mapping_dict semantic_dict,grid_functions *functions):nh_(nh),params_(params),semantic_dict_(semantic_dict),mygrid_fns(functions){
    vis_pub_=nh_.advertise<visualization_msgs::Marker>("RMAP",1);
    vis_pub_pointcloud_ = nh.advertise<sensor_msgs::PointCloud2> ("Point_cloud", 1);
    vis_pub_labeledcloud_ = nh.advertise<sensor_msgs::PointCloud> ("Labeled_point_cloud", 1);
    style_ = SEMANTIC_LABEL;
}


void visualizer::labeled_pointcloud(scansirgblabel_with_pose &pointcloud){

    sensor_msgs::PointCloud pub_cloud;

    pub_cloud.header.frame_id = params_->frame_of_reference_;
    pub_cloud.header.stamp = ros::Time::now();

    pub_cloud.channels.resize(1);
    pub_cloud.channels[0].name = "rgb";

    for(unsigned int i=0;i< pointcloud.size() ;i++){
        for(unsigned int j=0;j< pointcloud[i].second.size(); j++){

            geometry_msgs::Point32 mypoint;
            mypoint.x = pointcloud[i].second[j].x_;
            mypoint.y = pointcloud[i].second[j].y_;
            mypoint.z = pointcloud[i].second[j].z_;
            pub_cloud.points.push_back(mypoint);

            uint8_t r = pointcloud[i].second[j].r_label_, g = pointcloud[i].second[j].g_label_, b = pointcloud[i].second[j].b_label_; // Example: Red color
            uint32_t rgb = ((uint32_t)r << 16 | (uint32_t)g << 8 | (uint32_t)b);
            pub_cloud.channels[0].values.push_back(*reinterpret_cast<float*>(&rgb));
        }

    }


    vis_pub_labeledcloud_.publish(pub_cloud);

}

void visualizer::visualize_pointcloud(scansirgblabel_with_pose &pointcloud){

    sensor_msgs::PointCloud pub_cloud;

    pub_cloud.header.frame_id = params_->frame_of_reference_;
    pub_cloud.header.stamp = ros::Time::now();

    pub_cloud.channels.resize(2);
    pub_cloud.channels[0].name = "rgb";
    pub_cloud.channels[1].name = "intensity";

    for(unsigned int i=0;i< pointcloud.size() ;i++){
        for(unsigned int j=0;j< pointcloud[i].second.size(); j++){

            geometry_msgs::Point32 mypoint;
            mypoint.x = pointcloud[i].second[j].x_;
            mypoint.y = pointcloud[i].second[j].y_;
            mypoint.z = pointcloud[i].second[j].z_;
            pub_cloud.points.push_back(mypoint);

            uint8_t r = pointcloud[i].second[j].r_, g = pointcloud[i].second[j].g_, b = pointcloud[i].second[j].b_; // Example: Red color
            uint32_t rgb = ((uint32_t)r << 16 | (uint32_t)g << 8 | (uint32_t)b);
            pub_cloud.channels[0].values.push_back(*reinterpret_cast<float*>(&rgb));
            pub_cloud.channels[1].values.push_back(pointcloud[i].second[j].intensity_);

        }

    }

    sensor_msgs::PointCloud2 mycloud;
    sensor_msgs::convertPointCloudToPointCloud2(pub_cloud,mycloud);
    vis_pub_pointcloud_.publish(mycloud);
    //vis_pub_pointcloud_.publish(pub_cloud);

}

void visualizer::add_grid_cells(grid_cells cells){
    cells_ = cells;
}

void visualizer::remove_grid_cells(){
    cells_.clear();
}

std_msgs::ColorRGBA visualizer::assign_color(value &cell, double height){

    std_msgs::ColorRGBA color;

    if(style_ == RGB_CUBE){
        int max_color = params_->get_max_color();
        color.r = ((float)cell.second->r_)/((float)max_color);
        color.g = ((float)cell.second->g_)/((float)max_color);
        color.b = ((float)cell.second->b_)/((float)max_color);
        color.a = 1;

    }else if(style_ == SEMANTIC_LABEL){

        // assign labels to each grid cell
        dirichlet_params dist_params = params_->get_dirichlet_params();
        //unsigned int label_index = std::distance(cell.second->label_count_.begin(),std::max_element(cell.second->label_count_.begin(),cell.second->label_count_.end()));
        int dir_label = mygrid_fns->assign_label(cell,dist_params);

        if(dir_label == -1){
            color.r = 0.0;
            color.g = 0.0;
            color.b = 0.0;
            color.a = 1;
            return color;
        }else{
            rgb_label temp_color = semantic_dict_[dir_label];
            color.r = temp_color.r_;
            color.g = temp_color.g_;
            color.b = temp_color.b_;
            color.a = 1;
        }

    }else if(style_ == HEIGHT_COLORED){
        if(height < 0.4){
            color.r = 0.0f;
            color.g = 0.5f;
            color.b = 0.5f;
            color.a = 1.0;
        }
        else if(height > 0.4 && height < 0.7){
            color.r = 0.0f;
            color.g = 0.4f;
            color.b = 0.6f;
            color.a = 1.0;
        }

        else if(height > 0.7 && height < 1.3){
            color.r = 0.0f;
            color.g = 0.2f;
            color.b = 0.8f;
            color.a = 1.0;
        }

        else if(height > 1.3 && height < 2.0){
            color.r = 0.0f;
            color.g = 0.1f;
            color.b = 0.9f;
            color.a = 1.0;
        }

        else if(height > 2.0 && height < 2.8){
            color.r = 0.0f;
            color.g = 0.0f;
            color.b = 1.0f;
            color.a = 1.0;
        }

        else if(height > 2.8 && height < 3.7){
            color.r = 0.0f;
            color.g = 0.2f;
            color.b = 0.8f;
            color.a = 1.0;
        }

        else if(height > 3.7 && height < 4.5){
            color.r = 0.2f;
            color.g = 0.4f;
            color.b = 0.6f;
            color.a = 1.0;
        }

        else if(height > 4.5 && height < 5.4){
            color.r = 0.3f;
            color.g = 0.6f;
            color.b = 0.4f;
            color.a = 1.0;
        }

        else if(height > 5.4 && height < 6.9){
            color.r = 0.4f;
            color.g = 0.8f;
            color.b = 0.2f;
            color.a = 1.0;
        }

        else if(height > 6.9 && height < 7.7){
            color.r = 0.5f;
            color.g = 1.0f;
            color.b = 0.0f;
            color.a = 1.0;
        }

        else if(height > 7.7 && height < 8.5){
            color.r = 0.4f;
            color.g = 1.0f;
            color.b = 0.0f;
            color.a = 1.0;
        }


        else if(height > 8.5 && height < 9.7){
            color.r = 0.2f;
            color.g = 1.0f;
            color.b = 0.0f;
            color.a = 1.0;
        }

        else{
            color.r = 0.0f;
            color.g = 1.0f;
            color.b = 0.0f;
            color.a = 1.0;
        }
    }else if(style_ == UNIFORM){
        cout << " DO SOMETHING " <<endl;
    }

    return color;

}


std_msgs::ColorRGBA visualizer::assign_color(double height){

    std_msgs::ColorRGBA color;

    if(style_ == HEIGHT_COLORED){
        if(height < 0.4){
            color.r = 0.0f;
            color.g = 0.5f;
            color.b = 0.5f;
            color.a = 1.0;
        }
        else if(height > 0.4 && height < 0.7){
            color.r = 0.0f;
            color.g = 0.4f;
            color.b = 0.6f;
            color.a = 1.0;
        }

        else if(height > 0.7 && height < 1.3){
            color.r = 0.0f;
            color.g = 0.2f;
            color.b = 0.8f;
            color.a = 1.0;
        }

        else if(height > 1.3 && height < 2.0){
            color.r = 0.0f;
            color.g = 0.1f;
            color.b = 0.9f;
            color.a = 1.0;
        }

        else if(height > 2.0 && height < 2.8){
            color.r = 0.0f;
            color.g = 0.0f;
            color.b = 1.0f;
            color.a = 1.0;
        }

        else if(height > 2.8 && height < 3.7){
            color.r = 0.0f;
            color.g = 0.2f;
            color.b = 0.8f;
            color.a = 1.0;
        }

        else if(height > 3.7 && height < 4.5){
            color.r = 0.2f;
            color.g = 0.4f;
            color.b = 0.6f;
            color.a = 1.0;
        }

        else if(height > 4.5 && height < 5.4){
            color.r = 0.3f;
            color.g = 0.6f;
            color.b = 0.4f;
            color.a = 1.0;
        }

        else if(height > 5.4 && height < 6.9){
            color.r = 0.4f;
            color.g = 0.8f;
            color.b = 0.2f;
            color.a = 1.0;
        }

        else if(height > 6.9 && height < 7.7){
            color.r = 0.5f;
            color.g = 1.0f;
            color.b = 0.0f;
            color.a = 1.0;
        }

        else if(height > 7.7 && height < 8.5){
            color.r = 0.4f;
            color.g = 1.0f;
            color.b = 0.0f;
            color.a = 1.0;
        }


        else if(height > 8.5 && height < 9.7){
            color.r = 0.2f;
            color.g = 1.0f;
            color.b = 0.0f;
            color.a = 1.0;
        }

        else{
            color.r = 0.0f;
            color.g = 1.0f;
            color.b = 0.0f;
            color.a = 1.0;
        }
    }else if(style_ == UNIFORM){
        cout << " DO SOMETHING " <<endl;
    }

    return color;

}

void visualizer::visualize_cubes(){

    //generate random id
    srand(ros::Time::now().nsec);

    visualization_msgs::Marker cubes;
    cubes.type = visualization_msgs::Marker::CUBE_LIST;
    cubes.header.frame_id = params_->frame_of_reference_;   ///should be odom
    cubes.header.stamp = ros::Time::now();
    cubes.ns = "MapPoints";
    cubes.action = visualization_msgs::Marker::ADD;

    cubes.scale.x = params_->get_resolution();
    cubes.scale.y = params_->get_resolution();
    cubes.scale.z = params_->get_resolution();
    cubes.pose.orientation.w = 1.0;

    for(unsigned int i= 0 ; i< cells_.size();i++){

        // visualize cells over clamping threshold!!
        if(cells_[i].second->log_odds_ < params_->get_log_max_prob_occ_vis() )//&& cells_[i].second->log_odds_ > params_->get_log_min_prob_free() )
            continue;

        // the min corner
        int min_x = bg::get<bg::min_corner, 0>(cells_[i].first);
        int min_y = bg::get<bg::min_corner, 1>(cells_[i].first);
        int min_z = bg::get<bg::min_corner, 2>(cells_[i].first);

        // the max corner
        int max_x = bg::get<bg::max_corner, 0>(cells_[i].first);
        int max_y = bg::get<bg::max_corner, 1>(cells_[i].first);
        int max_z = bg::get<bg::max_corner, 2>(cells_[i].first);


        double x_min = ((double) min_x)*params_->get_resolution();
        double y_min = ((double) min_y)*params_->get_resolution();
        double z_min = ((double) min_z)*params_->get_resolution();
        double x_max = ((double) max_x)*params_->get_resolution();
        double y_max = ((double) max_y)*params_->get_resolution();
        double z_max = ((double) max_z)*params_->get_resolution();

        geometry_msgs::Point p;
        cubes.id = i;

        p.x = (x_min + x_max)/2;
        p.y = (y_min + y_max)/2;
        p.z = (z_min + z_max)/2;

        cubes.points.push_back(p);

        std_msgs::ColorRGBA color = assign_color(cells_[i],p.z);

        cubes.colors.push_back(color);

    }


    vis_pub_.publish(cubes);

}


void visualizer::visualize(){

      //generate random id
      srand(ros::Time::now().nsec);

      visualization_msgs::MarkerArray cube_array;

      int count = 0;

      for(unsigned int i= 0 ; i< cells_.size();i++){

            // visualize cells over clamping threshold!!
          //if(cells_[i].second->log_odds_ < params_->get_log_max_prob_occ_vis() )//&& cells_[i].second->log_odds_ > params_->get_log_min_prob_free() )
          //    continue;

          visualization_msgs::Marker cuboid;
          cuboid.type = visualization_msgs::Marker::CUBE;
          cuboid.header.frame_id = params_->frame_of_reference_;   ///should be odom
          cuboid.header.stamp = ros::Time::now();
          cuboid.ns = "MapPoints";
          cuboid.action = visualization_msgs::Marker::ADD;

          // the min corner
          int min_x = bg::get<bg::min_corner, 0>(cells_[i].first);
          int min_y = bg::get<bg::min_corner, 1>(cells_[i].first);
          int min_z = bg::get<bg::min_corner, 2>(cells_[i].first);

          // the max corner
          int max_x = bg::get<bg::max_corner, 0>(cells_[i].first);
          int max_y = bg::get<bg::max_corner, 1>(cells_[i].first);
          int max_z = bg::get<bg::max_corner, 2>(cells_[i].first);


          double x_min = ((double) min_x)*params_->get_resolution();
          double y_min = ((double) min_y)*params_->get_resolution();
          double z_min = ((double) min_z)*params_->get_resolution();
          double x_max = ((double) max_x)*params_->get_resolution();
          double y_max = ((double) max_y)*params_->get_resolution();
          double z_max = ((double) max_z)*params_->get_resolution();

          cuboid.scale.x = x_max - x_min;
          cuboid.scale.y = y_max - y_min;
          cuboid.scale.z = z_max - z_min;

          geometry_msgs::Point p;
          cuboid.id = i;


          bool cub = false;
          if( ( fabs(cuboid.scale.x  - cuboid.scale.y) < 0.1) && ( fabs(cuboid.scale.y  - cuboid.scale.z) < 0.1) ){
              //cout << points.scale.x << " " << points.scale.y << " " << points.scale.z <<endl;
          }else{
              cub = true;
              count++;
          }

          p.x = (x_min + x_max)/2;
          p.y = (y_min + y_max)/2;
          p.z = (z_min + z_max)/2;

          cuboid.pose.orientation.w = 1.0;


          std_msgs::ColorRGBA color = assign_color(p.z);

            cuboid.color.r = color.r;
            cuboid.color.g = color.g;
            cuboid.color.b = color.b;
            cuboid.color.a = color.a;

            cuboid.pose.position.x = p.x;
            cuboid.pose.position.y = p.y;
            cuboid.pose.position.z = p.z;

            //if(cub){
                //cout << cuboid.scale.x << " " << cuboid.scale.y << " " << cuboid.scale.z <<endl;
                cube_array.markers.push_back(cuboid);
            //}



      }


      cout << " Total count of non cubic cells : " << count <<endl;
      //cout << " Total count: " << cube_array.markers.size() <<endl;

      vis_pub_.publish(cube_array);

}

void visualizer::visualize_test(){

      //generate random id
      srand(ros::Time::now().nsec);

      visualization_msgs::MarkerArray cube_array;

      int count = 0;

      visualization_msgs::Marker cuboid;
      cuboid.type = visualization_msgs::Marker::CUBE_LIST;
      cuboid.header.frame_id = params_->frame_of_reference_;   ///should be odom
      cuboid.header.stamp = ros::Time::now();
      cuboid.ns = "MapPoints";
      cuboid.action = visualization_msgs::Marker::ADD;

      for(unsigned int i= 0 ; i< cells_.size();i++){

            // visualize cells over clamping threshold!!
          //if(cells_[i].second->log_odds_ < params_->get_log_max_prob_occ_vis() )//&& cells_[i].second->log_odds_ > params_->get_log_min_prob_free() )
          //    continue;



          // the min corner
          int min_x = bg::get<bg::min_corner, 0>(cells_[i].first);
          int min_y = bg::get<bg::min_corner, 1>(cells_[i].first);
          int min_z = bg::get<bg::min_corner, 2>(cells_[i].first);

          // the max corner
          int max_x = bg::get<bg::max_corner, 0>(cells_[i].first);
          int max_y = bg::get<bg::max_corner, 1>(cells_[i].first);
          int max_z = bg::get<bg::max_corner, 2>(cells_[i].first);


          double x_min = ((double) min_x)*params_->get_resolution();
          double y_min = ((double) min_y)*params_->get_resolution();
          double z_min = ((double) min_z)*params_->get_resolution();
          double x_max = ((double) max_x)*params_->get_resolution();
          double y_max = ((double) max_y)*params_->get_resolution();
          double z_max = ((double) max_z)*params_->get_resolution();

          cuboid.scale.x = x_max - x_min;
          cuboid.scale.y = y_max - y_min;
          cuboid.scale.z = z_max - z_min;

          geometry_msgs::Point p;
          cuboid.id = i;


          bool cub = false;
          if( ( fabs(cuboid.scale.x  - cuboid.scale.y) < 0.001) && ( fabs(cuboid.scale.y  - cuboid.scale.z) < 0.001) ){
              //cout << points.scale.x << " " << points.scale.y << " " << points.scale.z <<endl;
          }else{
              //cout << points.scale.x << " " << points.scale.y << " " << points.scale.z <<endl;
              cub = true;
              count++;
          }

          p.x = (x_min + x_max)/2;
          p.y = (y_min + y_max)/2;
          p.z = (z_min + z_max)/2;

          cuboid.pose.orientation.w = 1.0;


          std_msgs::ColorRGBA color = assign_color(p.z);

          cuboid.colors.push_back(color);
          cuboid.points.push_back(p);

            //if(!cub){
            //}


      }

      cube_array.markers.push_back(cuboid);

      cout << " Total count: " << count <<endl;
      //cout << " Total count: " << cube_array.markers.size() <<endl;

      vis_pub_.publish(cube_array);

}

//if(non_cub){
    //cout << " Actual bounds in int " <<endl;
    //cout << min_x << " " << min_y << " " << min_z << " " << max_x << " " << max_y << " " << max_z  <<endl;

    //cout << " bounds after multiplying by resoution " <<endl;
    //cout << x_min << " " << y_min << " " << z_min << " " << x_max << " " << y_max << " " << z_max  <<endl;

    //cout << " Centroid " <<endl;
    //cout << p.x << " " << p.y << " " << p.z <<endl;

// }
