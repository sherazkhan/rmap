#ifndef RTREE_PARAMS
#define RTREE_PARAMS

using namespace std;

class params{

public:
    params();

    // old occupancy grid methods..might be required..!!
    inline double get_log_max_prob_occ();
    inline double get_log_min_prob_free();
    double get_resolution();
    bool is_ray_tracing_active();
    inline double get_log_prob_occ();
    inline double get_log_prob_free();
    inline bool model_free_space();
    inline bool get_log_max_prob_occ_vis();
    inline bool get_log_min_prob_free_vis();
    inline double get_fusion_max_log_occ();
    inline double get_fusion_min_log_free();
    inline bool is_fusion_active();

private:
    string image_directory;
    string image_start_label;
    string segmented_image_label;
    string intensity_image_label;
    string depth_image_label;

    // old occupancy grid params..might be required..!!
    double log_p_occ_;
    double log_p_free_;
    double log_max_p_occ_;
    double log_min_p_free_;
    double log_max_p_occ_vis_;
    double log_min_p_free_vis_;
    double fusion_max_log_occ_;
    double fusion_min_log_free_;
    double resolution_;
    bool ray_tracing_;
    bool model_free_space_;
    bool fusion_;
};


params::params(){

    // old occupancy grid param initialization..might be required..!!
    log_p_occ_ = 0.85; // the sensor model parameter P(r_i | z_t)...log probability update for occupied cells
    log_p_free_ = -0.4; // log probability update for free cells
    log_max_p_occ_ = 3.5; // max prob clamping threshold
    log_min_p_free_ = -2; // min prob clamping threshold
    log_max_p_occ_vis_ = 0.0;
    log_min_p_free_vis_ = -0.1;
    fusion_max_log_occ_ = 3.5; // the probability at which occupied cells are allowed to be fused...should be same as log_max_p_occ;
    fusion_min_log_free_ = -2; // the probability at which free cells are allowed to be fused...should be same as log_min_p_free;
    resolution_ = 0.1; // resolution in meters
    ray_tracing_ = false; // enable ray tracing through the grid
    model_free_space_ = false; // explicit modelling of free space?
    fusion_ = true;

}

double params::get_log_prob_occ(){
    return log_p_occ_;
}

double params::get_log_prob_free(){
    return log_p_free_;
}

double params::get_resolution(){
    return resolution_;
}

double params::get_log_max_prob_occ(){
    return log_max_p_occ_;
}

double params::get_log_min_prob_free(){
    return log_min_p_free_;
}

bool params::is_ray_tracing_active(){
    return ray_tracing_;
}

bool params::model_free_space(){
    return model_free_space_;
}

bool params::get_log_max_prob_occ_vis(){
    return log_max_p_occ_vis_;
}

bool params::get_log_min_prob_free_vis(){
    return log_min_p_free_vis_;
}

bool params::is_fusion_active(){
    return fusion_;
}

double params::get_fusion_max_log_occ(){
    return fusion_max_log_occ_;
}

double params::get_fusion_min_log_free(){
    return fusion_min_log_free_;
}

#endif
