#ifndef CUSTOM_POINT_TYPES_H
#define CUSTOM_POINT_TYPES_H

#define PCL_NO_PRECOMPILE
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>

struct PointXYZIRGBLabel
{
    PCL_ADD_POINT4D;
    union
    {
        struct
        {
            float intensity;
        };
        float data_i[4];
    };

    union
    {
        struct
        {
            uint8_t b;
            uint8_t g;
            uint8_t r;
            uint8_t _unused;
        };
        float rgb;
        float data_c[4];
    };

    union
    {
        struct
        {
            uint8_t scan_idx;
            uint8_t class_label;
            uint8_t obj_id;
            uint8_t _unused2;
        };
        float data_l[4];
        float label;
    };

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
} EIGEN_ALIGN16;

POINT_CLOUD_REGISTER_POINT_STRUCT (PointXYZIRGBLabel,
                                   (float, x, x)
                                   (float, y, y)
                                   (float, z, z)
                                   (float, intensity, intensity)
                                   (float, rgb, rgb)
                                   (float, label, label))

typedef pcl::PointXYZI PointTypeIO;
typedef pcl::PointXYZINormal PointTypeFull;

#endif // CUSTOM_POINT_TYPES_H

