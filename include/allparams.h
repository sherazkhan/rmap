#ifndef RTREE_PARAMS
#define RTREE_PARAMS

using namespace std;

typedef std::pair< int, std::vector<int> > concentration_params;
typedef std::vector<concentration_params> dirichlet_params;

class params{

public:
    params();

    // old occupancy grid methods..might be required..!!
    inline double get_log_max_prob_occ();
    inline double get_log_min_prob_free();
    double get_resolution();
    bool is_ray_tracing_active();
    inline double get_log_prob_occ();
    inline double get_log_prob_free();
    inline bool model_free_space();
    inline double get_log_max_prob_occ_vis();
    inline double get_log_min_prob_free_vis();
    inline double get_fusion_max_log_occ();
    inline double get_fusion_min_log_free();
    inline bool is_fusion_active();
    inline double get_max_range();
    inline double get_min_range();
    inline int get_max_color();
    inline int get_status_update();
    inline unsigned int get_max_no_labels();
    inline double get_init_occ();
    void read_dirichlet_params();
    inline dirichlet_params get_dirichlet_params();
    string get_label(int id);
    int get_id(string label);


    string image_directory;
    string pointcloud_directory;
    string image_start_label;
    string segmented_image_label;
    string object_image_label;
    string intensity_image_label;
    string depth_image_label;
    string image_extension;
    string pointcloud_extension;
    string quaternion_directory_;

    // max image index
    int max_image_index_;
    // min image index
    int min_image_index_;
    int sample_pts_;
    string frame_of_reference_;
private:
    // old occupancy grid params..might be required..!!
    dirichlet_params dirichlet_params_;
    unsigned int max_semantic_labels_;
    int status_update_;
    int max_color_;
    int min_color_;
    double max_range_;
    double min_range_;
    double init_log_occ_;
    double log_p_occ_;
    double log_p_free_;
    double log_max_p_occ_;
    double log_min_p_free_;
    double log_max_p_occ_vis_;
    double log_min_p_free_vis_;
    double fusion_max_log_occ_;
    double fusion_min_log_free_;
    double resolution_;
    bool ray_tracing_;
    bool model_free_space_;
    bool fusion_;
};


params::params(){

    image_directory = "/media/liberica_common/IURO/Datasets/Z+F_laser_datasets/Processed_data/PNG/";
    pointcloud_directory = "/media/dataset_drive/";
    image_start_label = "scan_";
    segmented_image_label = "_label";
    object_image_label = "_object";
    intensity_image_label = "_intensity";
    depth_image_label = "_range";
    image_extension = ".png";
    pointcloud_extension = ".pcd";
    frame_of_reference_ = "/base_link";
    quaternion_directory_ = "/media/dataset_drive/";
    // the max number of semantic classes defined...
    max_semantic_labels_ = 11;

    max_image_index_ = 83;
    min_image_index_ = 1;

    // old occupancy grid param initialization...!!
    sample_pts_ = 100000;
    status_update_ = 10000000;
    max_color_ = 255;
    min_color_ = 0;
    max_range_ = 150.0; // max range in meters...
    min_range_ = 1.5; // min range in meters...

   // the initial log occupancy prob
    init_log_occ_ = 0.0;

    log_p_occ_ = 0.85; // the sensor model parameter P(r_i | z_t)...log probability update for occupied cells
    log_p_free_ = -0.4; // log probability update for free cells
    log_max_p_occ_ = 3.5; // max log prob clamping threshold
    log_min_p_free_ = -2; // min log prob clamping threshold
    log_max_p_occ_vis_ = 3.5;
    log_min_p_free_vis_ = -2;

    fusion_max_log_occ_ = 3.5; // the probability at which occupied cells are allowed to be fused...should be the same as log_max_p_occ;
    fusion_min_log_free_ = -2; // the probability at which free cells are allowed to be fused...should be the same as log_min_p_free;
    resolution_ = 0.2; // resolution in meters
    ray_tracing_ = false; // enable ray tracing through the grid
    model_free_space_ = false; // explicit modeling of free space?
    fusion_ = false; // enable or disable fusion in the grid...
    read_dirichlet_params();

}

double params::get_log_prob_occ(){
    return log_p_occ_;
}

double params::get_log_prob_free(){
    return log_p_free_;
}

double params::get_resolution(){
    return resolution_;
}

double params::get_log_max_prob_occ(){
    return log_max_p_occ_;
}

double params::get_log_min_prob_free(){
    return log_min_p_free_;
}

double params::get_log_max_prob_occ_vis(){
    return log_max_p_occ_vis_;
}

double params::get_log_min_prob_free_vis(){
    return log_min_p_free_vis_;
}

bool params::is_ray_tracing_active(){
    return ray_tracing_;
}

bool params::model_free_space(){
    return model_free_space_;
}

bool params::is_fusion_active(){
    return fusion_;
}

double params::get_fusion_max_log_occ(){
    return fusion_max_log_occ_;
}

double params::get_fusion_min_log_free(){
    return fusion_min_log_free_;
}

double params::get_max_range(){
    return max_range_;
}

double params::get_min_range(){
    return min_range_;
}

int params::get_max_color(){
    return max_color_;
}

int params::get_status_update(){
    return status_update_;
}

unsigned int params::get_max_no_labels(){
    return max_semantic_labels_;
}

double params::get_init_occ(){
    return init_log_occ_;
}

void params::read_dirichlet_params(){
    ifstream myfile ("../params.txt");
    string line;

    if (myfile.is_open())
    {
        int line_number = 0;
        while ( getline (myfile,line) )
        {
            std::string token;
            std::istringstream iss(line);

            std::vector<int> temp_params;
            while ( getline(iss, token, ' ') )
            {
                std::istringstream stm;
                stm.str(token);
                int d;
                stm >>d;
                temp_params.push_back(d);
                //dirichlet_params
            }
            concentration_params vals = make_pair(line_number,temp_params);
            dirichlet_params_.push_back(vals);
            line_number++;
            temp_params.clear();
        }

        myfile.close();
    }

}

inline dirichlet_params params::get_dirichlet_params(){
    return dirichlet_params_;
}

string params::get_label(int id){

    std::string label;

    if(id == 0){ // car label
        label = "car";
    }else if(id == 1){ // house label
        label = "house";
    }else if(id == 2){ // street label
        label = "street";
    }else if(id == 3){ // sidewalk label
        label = "side_walk";
    }else if(id == 4){ // bicycle label
        label = "bike";
    }else if(id == 5){ // other label
        label = "other";
    }else if(id == 6){ // tree label
        label = "tree";
    }else if(id == 7){ // pole label
        label = "pole";
    }else if(id == 8){ // sky label
        label = "sky";
    }else if(id == 9){ // grass label
        label = "grass";
    }else if(id == 10 || id == -1){
        label = "nothing";
    }
    return label;
}

int params::get_id(string label){

    int id;

    if(label.compare("car") == 0){ // car label
        id = 0;
    }else if(label.compare("house") == 0){ // house label
        id = 1;
    }else if(label.compare("street") == 0){ // street label
        id = 2;
    }else if(label.compare("side_walk") == 0){ // sidewalk label
        id = 3;
    }else if(label.compare("bike") == 0){ // bicycle label
        id = 4;
    }else if(label.compare("other") == 0){ // other label
        id = 5;
    }else if(label.compare("car") == 0){ // tree label
        id = 6;
    }else if(label.compare("pole") == 0){ // pole label
        id = 7;
    }else if(label.compare("sky") == 0){ // sky label
        id = 8;
    }else if(label.compare("grass") == 0){ // grass label
        id = 9;
    }else if(label.empty()){
        id = -1;
    }
    return id;
}

#endif
