#ifndef RTREE_OCCUPANCY_GRID
#define RTREE_OCCUPANCY_GRID

//////////////

// the Rtree occupancy grid
//////////////
#include <iostream>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/index/rtree.hpp>
/* #include <boost/geometry/index/detail/rtree/utilities/statistics.hpp> */

// to store queries results
#include <vector>
#include <Eigen/Core>

#include <stdlib.h>
#include <stdio.h>

typedef Eigen::Matrix<int, 6, 6> search_mat;
typedef Eigen::Matrix<int, 6, 1> cuboid_corners;

// just for output
#include <iostream>
#include <boost/foreach.hpp>
#include "grid_functions.h"
#include "input_data.h"
#include "color.h"  // namespace Color

enum query_type { INTERSECT, OVERLAP, CONTAINS, DISJOINT, COVERED_BY };

typedef std::pair<value, pcl::Normal> cell_with_normal;
typedef std::vector<cell_with_normal> vector_cells;
typedef std::vector<vector_cells> sorted_objects;

class rtree_occupancy_grid {
   public:
    rtree_occupancy_grid(params *params, grid_functions *grid_fns, int num_children);
    bool insert_observation(point_xyz sensor_position, point_xyzirgblabel mypoint);
    bool update(value &cell_value, bool end_point, bool &prob_thresh_cross, point_xyzirgblabel mypoint);
    bool initialize(rect_cuboid &bounding_box, bool end_point, point_xyzirgblabel mypoint);
    bool insert_points();
    bool is_initialized(rect_cuboid cuboid, grid_cells &initialized_grid_cells);
    void access_all_gridcells(grid_cells &all_cells);
    void access_semantic_class(grid_cells &all_cells, int label);
    bool fuse_cells();
    bool try_fusion(rect_cuboid cell);
    bool remove_grid_cell(value &cell);
    bool remove_grid_cells(grid_cells &cell);
    bool filter_cells(grid_cells &cells);
    bool check_fusion_thresholds(grid_cells actual_cell, grid_cells &returned_cells);
    point_cloud get_query_points(cuboid_corners query_cuboid);
    rtree_stats get_rtree_stats();
    void visualize_all_gridcells();
    grid_cells query(rect_cuboid query_cuboid, query_type type);
    grid_cells query(point_xyz point, query_type type);
    void compute_normals(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud);
    bool cubes(rmap::cuboid_service::Request &req, rmap::cuboid_service::Response &res);
    bool return_cuboids(rmap::cuboid_service::Request &req, rmap::cuboid_service::Response &res);

    // data for sharing---
    grid_cells all_cells_;
    pcl::PointCloud<pcl::Normal> cloud_normals_;
    sorted_objects label_based_;
    sorted_objects object_based_;

   private:
    params *rtree_grid_params_;
    grid_functions *mygrid_fns;
    int num_children_;
    bgi::rtree<value, bgi::dynamic_quadratic> rtree_grid_;
    std::vector<value> cells_fusion_;
};

rtree_occupancy_grid::rtree_occupancy_grid(params *params, grid_functions *grid_fns, int num_children)
    : rtree_grid_params_(params),
      mygrid_fns(grid_fns),
      num_children_(num_children),
      rtree_grid_(bgi::dynamic_quadratic(num_children_)) {
    Color::Modifier green(Color::FG_GREEN);
    Color::Modifier def(Color::FG_DEFAULT);
    cout << green << " Occupancy grid initialized... " << def << std::endl;
}

void rtree_occupancy_grid::compute_normals(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud) {
    // pcl normal estimation
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;

    // set cloud
    ne.setInputCloud(cloud);

    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>());
    ne.setSearchMethod(tree);

    // Output datasets
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>);

    // Use all neighbors in a sphere of radius 3cm
    ne.setRadiusSearch(3);

    // Compute the features
    ne.compute(*cloud_normals);
    cloud_normals_ = *cloud_normals;
}

bool rtree_occupancy_grid::return_cuboids(rmap::cuboid_service::Request &req,
                                          rmap::cuboid_service::Response &res) {
    timeval start;
    timeval stop;

    gettimeofday(&start, NULL);  // start timer

    string find_label = req.label;

    int label = rtree_grid_params_->get_id(find_label);
    unsigned int id = req.object_id;

    // fill message
    rmap::cuboid temp_cuboid;

    if (label == -1 && id == 255) {
        // cout << " Sending all cells " <<endl;

        for (unsigned int i = 0; i < all_cells_.size(); i++) {
            double centre_x, centre_y, centre_z;
            mygrid_fns->get_centroid(all_cells_[i].first, centre_x, centre_y, centre_z);

            string object_label = rtree_grid_params_->get_label(all_cells_[i].second->label_);
            unsigned int object = all_cells_[i].second->object_id_;

            temp_cuboid.label = object_label;
            temp_cuboid.object_id = object;
            temp_cuboid.cuboid.point.x = centre_x * rtree_grid_params_->get_resolution();
            temp_cuboid.cuboid.point.y = centre_y * rtree_grid_params_->get_resolution();
            temp_cuboid.cuboid.point.z = centre_z * rtree_grid_params_->get_resolution();

            temp_cuboid.normal.x = cloud_normals_[i].normal_x;
            temp_cuboid.normal.y = cloud_normals_[i].normal_y;
            temp_cuboid.normal.z = cloud_normals_[i].normal_z;

            res.cuboids.push_back(temp_cuboid);
        }

    } else if (label == -1 && id != 255) {
        // object id based iteration
        // cout << " Sending objects based on id--- Num of cells being sent " << object_based_[id].size() <<endl;

        for (unsigned int i = 0; i < object_based_[id].size(); i++) {
            double centre_x, centre_y, centre_z;
            mygrid_fns->get_centroid(object_based_[id][i].first.first, centre_x, centre_y, centre_z);

            string object_label = rtree_grid_params_->get_label(object_based_[id][i].first.second->label_);
            unsigned int object = object_based_[id][i].first.second->object_id_;

            temp_cuboid.label = object_label;
            temp_cuboid.object_id = object;
            temp_cuboid.cuboid.point.x = centre_x * rtree_grid_params_->get_resolution();
            temp_cuboid.cuboid.point.y = centre_y * rtree_grid_params_->get_resolution();
            temp_cuboid.cuboid.point.z = centre_z * rtree_grid_params_->get_resolution();

            temp_cuboid.normal.x = object_based_[id][i].second.normal_x;
            temp_cuboid.normal.y = object_based_[id][i].second.normal_y;
            temp_cuboid.normal.z = object_based_[id][i].second.normal_z;

            res.cuboids.push_back(temp_cuboid);
        }

    } else if (label != -1) {
        // label based iteration
        // cout << " Sending objects based on label--- Num of cells being sent " << label_based_[label].size() <<endl;

        for (unsigned int i = 0; i < label_based_[label].size(); i++) {
            double centre_x, centre_y, centre_z;
            mygrid_fns->get_centroid(label_based_[label][i].first.first, centre_x, centre_y, centre_z);

            string object_label = rtree_grid_params_->get_label(label_based_[label][i].first.second->label_);
            unsigned int object = label_based_[label][i].first.second->object_id_;

            temp_cuboid.label = object_label;
            temp_cuboid.object_id = object;
            temp_cuboid.cuboid.point.x = centre_x * rtree_grid_params_->get_resolution();
            temp_cuboid.cuboid.point.y = centre_y * rtree_grid_params_->get_resolution();
            temp_cuboid.cuboid.point.z = centre_z * rtree_grid_params_->get_resolution();

            temp_cuboid.normal.x = label_based_[label][i].second.normal_x;
            temp_cuboid.normal.y = label_based_[label][i].second.normal_y;
            temp_cuboid.normal.z = label_based_[label][i].second.normal_z;

            res.cuboids.push_back(temp_cuboid);
        }
    }

    cout << " Number of cuboids sent: " << res.cuboids.size() << endl;

    gettimeofday(&stop, NULL);  // stop timer
    double time_to_insert = (stop.tv_sec - start.tv_sec) + 1.0e-6 * (stop.tv_usec - start.tv_usec);

    cout << time_to_insert << endl;

    return true;
}

bool rtree_occupancy_grid::cubes(rmap::cuboid_service::Request &req, rmap::cuboid_service::Response &res) {
    string find_label = req.label;

    unsigned int id = req.object_id;

    for (unsigned int i = 0; i < all_cells_.size(); i++) {
        string object_label = rtree_grid_params_->get_label(all_cells_[i].second->label_);
        unsigned int object = all_cells_[i].second->object_id_;

        if ((find_label.compare(object_label) == 0) && (id == object) || (find_label.empty() == 1 && id == object) ||
            (find_label.empty() == 1 && id == 255)) {
            double centre_x, centre_y, centre_z;
            mygrid_fns->get_centroid(all_cells_[i].first, centre_x, centre_y, centre_z);

            // fill message
            rmap::cuboid temp_cuboid;

            temp_cuboid.label = object_label;
            temp_cuboid.object_id = object;
            temp_cuboid.cuboid.point.x = centre_x * rtree_grid_params_->get_resolution();
            temp_cuboid.cuboid.point.y = centre_y * rtree_grid_params_->get_resolution();
            temp_cuboid.cuboid.point.z = centre_z * rtree_grid_params_->get_resolution();

            temp_cuboid.normal.x = cloud_normals_[i].normal_x;
            temp_cuboid.normal.y = cloud_normals_[i].normal_y;
            temp_cuboid.normal.z = cloud_normals_[i].normal_z;

            res.cuboids.push_back(temp_cuboid);
        } else {
            continue;
        }
    }

    cout << " Number of cuboids sent: " << res.cuboids.size() << endl;
    return true;
}

bool rtree_occupancy_grid::is_initialized(rect_cuboid bounding_box, grid_cells &initialized_grid_cells) {
    query_type myquery = INTERSECT;

    int in_x = bg::get<bg::min_corner, 0>(bounding_box);
    int in_y = bg::get<bg::min_corner, 1>(bounding_box);
    int in_z = bg::get<bg::min_corner, 2>(bounding_box);

    int out_x = bg::get<bg::max_corner, 0>(bounding_box);
    int out_y = bg::get<bg::max_corner, 1>(bounding_box);
    int out_z = bg::get<bg::max_corner, 2>(bounding_box);

    double mid_x = ((double)(in_x) + (double)(out_x)) / 2;
    double mid_y = ((double)(in_y) + (double)(out_y)) / 2;
    double mid_z = ((double)(in_z) + (double)(out_z)) / 2;

    point_xyz centre_point(mid_x, mid_y, mid_z);

    initialized_grid_cells = query(centre_point, myquery);

    if (initialized_grid_cells.size() == 0) {
        return false;
    } else {
        return true;
    }
}

bool rtree_occupancy_grid::initialize(rect_cuboid &bounding_box, bool end_point, point_xyzirgblabel mypoint) {
    if (end_point) {
        // cout << " Cell is uninitialized " <<endl;
        grid_cell *temp = new grid_cell(rtree_grid_params_->get_init_occ(), mypoint.r_, mypoint.g_, mypoint.b_,
                                        rtree_grid_params_->get_max_no_labels(), mypoint.object_id_);
        temp->intensity_ = mypoint.intensity_;
        temp->log_odds_ += rtree_grid_params_->get_log_prob_occ();
        temp->object_id_ = mypoint.object_id_;
        temp->label_count_[mypoint.label_] += 1;
        temp->num_hits_++;
        value assign = make_pair(bounding_box, temp);
        // insert grid cell in Rtree
        rtree_grid_.insert(assign);
    } else if ((!end_point) && rtree_grid_params_->model_free_space()) {
        // cout << " Cell is uninitialized " <<endl;
        grid_cell *temp = new grid_cell(rtree_grid_params_->get_max_no_labels());
        temp->log_odds_ += rtree_grid_params_->get_log_prob_free();
        temp->label_count_[temp->label_count_.size() - 1] += 1;
        temp->num_misses_++;
        value assign = make_pair(bounding_box, temp);
        // insert grid cell in Rtree
        rtree_grid_.insert(assign);
    }
    return true;
}

bool rtree_occupancy_grid::update(value &cell_value, bool end_point, bool &prob_thresh_cross,
                                  point_xyzirgblabel mypoint) {
    if (end_point) {
        // cout << " Cell already initialized " <<endl;
        // check if it is below the clamping threshold
        if (cell_value.second->log_odds_ < rtree_grid_params_->get_log_max_prob_occ()) {
            // the beam end point volume is always occupied
            cell_value.second->log_odds_ += rtree_grid_params_->get_log_prob_occ();
            cell_value.second->label_count_[mypoint.label_] += 1;
            cell_value.second->num_hits_++;
            int total_count = cell_value.second->num_hits_;
            int old_value_r = cell_value.second->r_;
            int old_value_g = cell_value.second->g_;
            int old_value_b = cell_value.second->b_;
            cell_value.second->r_ = old_value_r + (mypoint.r_ - old_value_r) / (total_count);
            cell_value.second->g_ = old_value_g + (mypoint.g_ - old_value_g) / (total_count);
            cell_value.second->b_ = old_value_b + (mypoint.b_ - old_value_b) / (total_count);

            //update intensity
            cell_value.second->intensity_ = cell_value.second->intensity_ +
                (mypoint.intensity_ - cell_value.second->intensity_) / (total_count);

            // if during update the cell crosses the upper threshold..
            if (cell_value.second->log_odds_ > rtree_grid_params_->get_log_max_prob_occ()) {
                prob_thresh_cross = true;
            }
        }

    } else {
        // cout << " Free cell already initialized " <<endl;
        // check if it is below the clamping threshold
        if (cell_value.second->log_odds_ > rtree_grid_params_->get_log_min_prob_free()) {
            // the beam end point volume is always occupied
            cell_value.second->log_odds_ += rtree_grid_params_->get_log_prob_free();
            cell_value.second->label_count_[cell_value.second->label_count_.size() - 1] += 1;
            cell_value.second->num_misses_++;

            // if during update the cell crosses the upper threshold..
            if (cell_value.second->log_odds_ < rtree_grid_params_->get_log_min_prob_free()) {
                prob_thresh_cross = true;
            }
        }
    }

    return true;
}

bool rtree_occupancy_grid::remove_grid_cell(value &cell) {
    // remove the cell from the grid
    rtree_grid_.remove(cell);

    // delete the pointer to the actual cell data as well
    delete cell.second;
}

bool rtree_occupancy_grid::remove_grid_cells(grid_cells &cells) {
    // remove the cell from the grid
    rtree_grid_.remove(cells.begin(), cells.end());

    // delete the pointer to the actual cell data as well
    for (unsigned int i = 0; i < cells.size(); i++) {
        delete cells[i].second;
    }
}

bool rtree_occupancy_grid::insert_observation(point_xyz sensor_position, point_xyzirgblabel endpoint) {
    point_xyz mypoint(endpoint.x_, endpoint.y_, endpoint.z_);
    ////////////////////////////////////////////////////////////////////////
    //////////////// UPDATING BEAM END POINTS ONLY

    // map the beam end point to the grid first and insert into the Rtree occupancy grid
    rect_cuboid bounding_box = mygrid_fns->map_to_grid(mypoint);

    grid_cells initialized_grid_cells;

    // check if cuboid exists in the grid
    bool initialized = is_initialized(bounding_box, initialized_grid_cells);

    if (!initialized) {
        // initialize grid cell
        initialize(bounding_box, true, endpoint);

    } else if ((initialized) && (initialized_grid_cells.size() == 1)) {
        bool prob_occ_thresh_cross = false;
        // update occupancy value
        update(initialized_grid_cells[0], true, prob_occ_thresh_cross, endpoint);

        // if fusion is allowed and the cells crosses the max occupancy threshold during update
        // mark it for fusion!!!
        if (rtree_grid_params_->is_fusion_active() && prob_occ_thresh_cross) {
            cells_fusion_.push_back(initialized_grid_cells[0]);
        }

    } else if ((initialized) && (initialized_grid_cells.size() > 1)) {
        // should not happen in any case...if this happens some horrible shit happened..
        Color::Modifier red(Color::FG_RED);
        Color::Modifier def(Color::FG_DEFAULT);
        cout << red << " Something is really really WONG...no operation is going to be performed...check the code!!"
             << def << endl;
    }

    /////////////////////////////////////////////////////////////////////////////
    ////////////// RAY TRACING UPDATE (If enabled)
    rect_cuboids traced_cuboids;
    if (rtree_grid_params_->is_ray_tracing_active()) {
        // cout << " Ray tracing is active" <<endl;

        traced_cuboids = mygrid_fns->trace_ray(sensor_position, mypoint);

        for (unsigned int i = 0; i < traced_cuboids.size(); i++) {
            grid_cells initialized_grid_cells;
            // check if cuboid exists in the grid
            bool initialized = is_initialized(traced_cuboids[i], initialized_grid_cells);

            if (!initialized) {
                // initialize grid cell
                initialize(traced_cuboids[i], false, endpoint);

            } else if ((initialized) && (initialized_grid_cells.size() == 1)) {
                // update occupancy value
                bool prob_free_thresh_cross = false;
                update(initialized_grid_cells[0], false, prob_free_thresh_cross, endpoint);

                // if fusion is allowed and the cells crosses the min occupancy threshold during update
                // mark it for fusion!!!
                if (rtree_grid_params_->is_fusion_active() && prob_free_thresh_cross) {
                    cells_fusion_.push_back(initialized_grid_cells[0]);
                }

            } else if ((initialized) && (initialized_grid_cells.size() > 1)) {
                // should not happen in any case...if it enters here some horrible shit happened..
                Color::Modifier red(Color::FG_RED);
                Color::Modifier def(Color::FG_DEFAULT);
                cout << red
                     << " Something is really really WONG...no operation is going to be performed...check the code!!"
                     << def << endl;
            }
        }
    }

    return true;
    std::cout << "Hmmm...what to do with this crap" << std::endl;
}

grid_cells rtree_occupancy_grid::query(rect_cuboid query_cuboid, query_type type) {
    grid_cells mygrid_cells;

    if (type == INTERSECT) {
        rtree_grid_.query(bgi::intersects(query_cuboid), back_inserter(mygrid_cells));
    } else if (type == OVERLAP) {
        rtree_grid_.query(bgi::overlaps(query_cuboid), back_inserter(mygrid_cells));
    } else if (type == CONTAINS) {
        rtree_grid_.query(bgi::within(query_cuboid), back_inserter(mygrid_cells));
    } else if (type == DISJOINT) {
        rtree_grid_.query(bgi::disjoint(query_cuboid), back_inserter(mygrid_cells));
    } else if (type == COVERED_BY) {
        rtree_grid_.query(bgi::covered_by(query_cuboid), back_inserter(mygrid_cells));
    }

    return mygrid_cells;
}

grid_cells rtree_occupancy_grid::query(point_xyz point, query_type type) {
    grid_cells mygrid_cells;

    float_point mypoint(point.x_, point.y_, point.z_);

    Color::Modifier red(Color::FG_RED);
    Color::Modifier def(Color::FG_DEFAULT);

    if (type == INTERSECT) {
        rtree_grid_.query(bgi::intersects(mypoint), back_inserter(mygrid_cells));
    } else if (type == OVERLAP) {
        cout << red << " Not possible " << def << endl;
    } else if (type == CONTAINS) {
        cout << red << " Not possible " << def << endl;
    } else if (type == DISJOINT) {
        cout << red << " Not possible " << def << endl;
    } else if (type == COVERED_BY) {
        cout << red << " Not possible " << def << endl;
    }

    return mygrid_cells;
}

void rtree_occupancy_grid::access_all_gridcells(grid_cells &all_cells) {
    // call to access all cells
    bdru::access_all_cells(rtree_grid_, all_cells);
}

rtree_stats rtree_occupancy_grid::get_rtree_stats() {
    // call to generate statistics
    rtree_stats occupancy_grid_stats = bdru::get_statistics(rtree_grid_);
    return occupancy_grid_stats;
}

bool rtree_occupancy_grid::fuse_cells() {
    // cout << " Trying to SFuse cells here " << endl;

    // send all cells for fusion
    for (unsigned int i = 0; i < cells_fusion_.size(); i++) {
        // check if the state is beyond the threshold and then call the fusion function!!!
        if ((cells_fusion_[i].second->log_odds_ > rtree_grid_params_->get_fusion_max_log_occ()) ||
            (cells_fusion_[i].second->log_odds_ < rtree_grid_params_->get_fusion_min_log_free())) {
            try_fusion(cells_fusion_[i].first);
        }
    }

    // cout << cells_fusion_.size() << endl;
    // clear cells for fusion during next cycle...
    cells_fusion_.clear();
}

point_cloud rtree_occupancy_grid::get_query_points(cuboid_corners query_cuboid) {
    point_cloud all_query_points;

    int width_x = fabs(query_cuboid(3, 0) - query_cuboid(0, 0));
    int width_y = fabs(query_cuboid(4, 0) - query_cuboid(1, 0));
    int width_z = fabs(query_cuboid(5, 0) - query_cuboid(2, 0));

    if (query_cuboid(3, 0) < query_cuboid(0, 0)) {
        query_cuboid(0, 0) = query_cuboid(3, 0);
    }

    if (query_cuboid(4, 0) < query_cuboid(1, 0)) {
        query_cuboid(1, 0) = query_cuboid(4, 0);
    }

    if (query_cuboid(5, 0) < query_cuboid(2, 0)) {
        query_cuboid(2, 0) = query_cuboid(5, 0);
    }

    for (int i = 0; i < width_x; i++) {
        for (int j = 0; j < width_y; j++) {
            for (int k = 0; k < width_z; k++) {
                /// QUERY WITH POINTS
                double centre_x = ((double)(query_cuboid(0, 0) + i) + (double)(query_cuboid(0, 0) + i + 1)) / 2;
                double centre_y = ((double)(query_cuboid(1, 0) + j) + (double)(query_cuboid(1, 0) + j + 1)) / 2;
                double centre_z = ((double)(query_cuboid(2, 0) + k) + (double)(query_cuboid(2, 0) + k + 1)) / 2;

                point_xyz query_point(centre_x, centre_y, centre_z);

                all_query_points.push_back(query_point);
            }
        }
    }

    return all_query_points;
}

bool rtree_occupancy_grid::filter_cells(grid_cells &returned_cells) {
    // remove redundant cells...
    for (unsigned int k = 0; k < returned_cells.size(); k++) {
        int comp_cub1_min_x = bg::get<bg::min_corner, 0>(returned_cells[k].first);
        int comp_cub1_min_y = bg::get<bg::min_corner, 1>(returned_cells[k].first);
        int comp_cub1_min_z = bg::get<bg::min_corner, 2>(returned_cells[k].first);

        // the max corner of the rect cuboid
        int comp_cub1_max_x = bg::get<bg::max_corner, 0>(returned_cells[k].first);
        int comp_cub1_max_y = bg::get<bg::max_corner, 1>(returned_cells[k].first);
        int comp_cub1_max_z = bg::get<bg::max_corner, 2>(returned_cells[k].first);

        for (unsigned int l = k + 1; l < returned_cells.size(); l++) {
            int comp_cub2_min_x = bg::get<bg::min_corner, 0>(returned_cells[l].first);
            int comp_cub2_min_y = bg::get<bg::min_corner, 1>(returned_cells[l].first);
            int comp_cub2_min_z = bg::get<bg::min_corner, 2>(returned_cells[l].first);

            // the max corner of the rect cuboid
            int comp_cub2_max_x = bg::get<bg::max_corner, 0>(returned_cells[l].first);
            int comp_cub2_max_y = bg::get<bg::max_corner, 1>(returned_cells[l].first);
            int comp_cub2_max_z = bg::get<bg::max_corner, 2>(returned_cells[l].first);

            // compare bounds and remove if a cuboid is redundant
            if ((comp_cub1_min_x == comp_cub2_min_x) && (comp_cub1_min_y == comp_cub2_min_y) &&
                (comp_cub1_min_z == comp_cub2_min_z) && (comp_cub1_max_x == comp_cub2_max_x) &&
                (comp_cub1_max_y == comp_cub2_max_y) && (comp_cub1_max_z == comp_cub2_max_z)) {
                returned_cells.erase(returned_cells.begin() + l);
                l--;
            }
        }
    }

    return true;
}

bool rtree_occupancy_grid::check_fusion_thresholds(grid_cells actual_cell, grid_cells &returned_cells) {
    /// declare important variables
    double fusion_max_occ = rtree_grid_params_->get_fusion_max_log_occ();
    double fusion_min_occ = rtree_grid_params_->get_fusion_min_log_free();

    // remove cells which are below or above the fusion thresholds!!!

    for (unsigned int k = 0; k < returned_cells.size(); k++) {
        if ((returned_cells[k].second->log_odds_ < fusion_max_occ) &&
            (returned_cells[k].second->log_odds_ > fusion_min_occ)) {
            // cout << " The probability of cell in the search direction is not above threshold ...so fusion cannot take
            // place... " <<endl;
            // cout << returned_cells[k].second->log_odds_ <<endl;
            returned_cells.erase(returned_cells.begin() + k);
            k--;
        }
    }

    for (unsigned int k = 0; k < returned_cells.size(); k++) {
        if (((actual_cell[0].second->log_odds_ > fusion_max_occ) &&
             (returned_cells[k].second->log_odds_ < fusion_min_occ)) ||
            ((actual_cell[0].second->log_odds_ < fusion_min_occ) &&
             (returned_cells[k].second->log_odds_ > fusion_max_occ))) {
            // cout << " Both cells cannot be merged as one is occupied and the other is free...if modelling of free
            // space is not active...this is weird.. " <<endl;
            returned_cells.erase(returned_cells.begin() + k);
            k--;
        }
    }

    return true;
}

/////////////////////////////////////////////////////////

bool rtree_occupancy_grid::try_fusion(rect_cuboid cell) {
    /// declare all important variables
    double fusion_max_occ = rtree_grid_params_->get_fusion_max_log_occ();
    double fusion_min_occ = rtree_grid_params_->get_fusion_min_log_free();

    // variables for centroid of the cell to be fused
    double centroid_x, centroid_y, centroid_z;
    mygrid_fns->get_centroid(cell, centroid_x, centroid_y, centroid_z);

    // query for cells in that region
    point_xyz query_point(centroid_x, centroid_y, centroid_z);

    query_type q_type = INTERSECT;
    grid_cells actual_cell = query(query_point, q_type);

    // should return 1 or 0 cells

    if (actual_cell.size() == 0) {
        cout << " Weird no cell exists here.. during update we found something " << endl;
    } else if (actual_cell.size() == 1) {
        // confirm is the cell is above the occupancy thresholds of fusion
        if ((actual_cell[0].second->log_odds_ < fusion_max_occ) &&
            (actual_cell[0].second->log_odds_ > fusion_min_occ)) {
            cout << " The probability of cell chosen during update has fallen below the threshold...if ray tracing is "
                    "not active..this is weird... " << endl;

            cout << actual_cell[0].second->log_odds_ << endl;
            cuboid_corners ret_cub_corners;
            ret_cub_corners(0, 0) = bg::get<bg::min_corner, 0>(actual_cell[0].first);
            ret_cub_corners(1, 0) = bg::get<bg::min_corner, 1>(actual_cell[0].first);
            ret_cub_corners(2, 0) = bg::get<bg::min_corner, 2>(actual_cell[0].first);

            // the max corner of the rect cuboid
            ret_cub_corners(3, 0) = bg::get<bg::max_corner, 0>(actual_cell[0].first);
            ret_cub_corners(4, 0) = bg::get<bg::max_corner, 1>(actual_cell[0].first);
            ret_cub_corners(5, 0) = bg::get<bg::max_corner, 2>(actual_cell[0].first);

            cout << ret_cub_corners << endl;

            return false;
        }

        // get search direction...!!
        search_mat search_direction, search_cell;
        cuboid_corners cub_corners;

        // get the search direction based on width and height of cuboid...
        std::vector<unsigned int> search_order;
        bool direction_defined = mygrid_fns->get_search_information(actual_cell[0].first, cub_corners, search_cell,
                                                                    search_direction, search_order);

        if (!direction_defined) {
            cout << " Some case of search direction has not been defined...fix this..." << endl;
            return false;
        }
        // storage of all contained cells
        grid_cells returned_cells;

        // boolean check for fusion possibility
        bool fusion_possible = false;

        // all temp variables required
        cuboid_corners expanded_cuboid;

        // loop over all search directions...and try fusing...
        for (unsigned int i = 0; i < search_order.size(); i++) {
            cuboid_corners shifted_cuboid, specific_dir, expanded_dir;

            // extend the cuboid in the space of search
            specific_dir = search_cell.row(i);
            shifted_cuboid = cub_corners + specific_dir;

            // the overall cuboid bounding the complete region
            expanded_dir = search_direction.row(i);
            expanded_cuboid = cub_corners + expanded_dir;

            ///////////////////////////// CODE to query for cells when the input element is not a cube...
            point_cloud all_query_points = get_query_points(shifted_cuboid);
            query_type q_type = INTERSECT;

            for (unsigned int k = 0; k < all_query_points.size(); k++) {
                grid_cells temp_cells = query(all_query_points[k], q_type);
                returned_cells.insert(returned_cells.end(), temp_cells.begin(), temp_cells.end());
                temp_cells.clear();
            }

            if (returned_cells.size() == 0) {
                // cout << " No cell exists in the search direction " <<endl;
                return false;
            } else if (returned_cells.size() >= 1) {
                // the interesting case in which the search direction cell contains some cells...

                // filter cells...check for redundant cells and if they are above occupancy fusion thresholds...if not
                // then remove them...
                filter_cells(returned_cells);
                check_fusion_thresholds(actual_cell, returned_cells);

                if (returned_cells.size() == 0) {
                    // cout << " No fusion possible...all cells were eliminated during redundancy and threshold checks "
                    // <<endl;
                    return false;
                }

                bool total_coverage = mygrid_fns->check_coverage_and_expansion_generalized(
                        returned_cells, shifted_cuboid, expanded_cuboid, search_order[i]);

                if (total_coverage) {
                    fusion_possible = true;
                    break;
                }
            }

            // prepare variables for next iteration!!
            fusion_possible = false;
            // remove all cuboids for next iteration...
            returned_cells.clear();
            // return false;
        }

        if (fusion_possible) {
            point min_point(expanded_cuboid(0, 0), expanded_cuboid(1, 0), expanded_cuboid(2, 0)),
                    max_point(expanded_cuboid(3, 0), expanded_cuboid(4, 0), expanded_cuboid(5, 0));

            rect_cuboid new_cuboid(min_point, max_point);

            double sum_log_odds = 0.0;

            for (unsigned int n = 0; n < returned_cells.size(); n++) {
                sum_log_odds += returned_cells[n].second->log_odds_;
            }

            double log_odds = (actual_cell[0].second->log_odds_ + sum_log_odds) / (returned_cells.size() + 1);

            grid_cell *temp = new grid_cell();
            temp->log_odds_ = log_odds;

            value assign = make_pair(new_cuboid, temp);

            // remove the fused cell and its relative cells
            remove_grid_cell(actual_cell[0]);
            remove_grid_cells(returned_cells);

            // insert grid cell in Rtree
            rtree_grid_.insert(assign);

            // recursive call
            try_fusion(assign.first);

            return true;

        } else {
            // cout << " No possible fusion cells found " <<endl;
            return false;
        }

        return false;
    } else {
        cout << " This should never happen....something is really really WONG in FUSION code.. " << endl;

        cout << query_point.x_ << " " << query_point.y_ << " " << query_point.z_ << endl;

        cout << " Actual cuboid " << endl;
        // the min corner of the rect cuboid
        int min_x = bg::get<bg::min_corner, 0>(cell);
        int min_y = bg::get<bg::min_corner, 1>(cell);
        int min_z = bg::get<bg::min_corner, 2>(cell);

        // the max corner of the rect cuboid
        int max_x = bg::get<bg::max_corner, 0>(cell);
        int max_y = bg::get<bg::max_corner, 1>(cell);
        int max_z = bg::get<bg::max_corner, 2>(cell);
        cout << min_x << " " << min_y << " " << min_z << " " << max_x << " " << max_y << " " << max_z << endl;

        cout << " Returned cells " << endl;

        cuboid_corners ret_cub_corners;

        for (unsigned int k = 0; k < actual_cell.size(); k++) {
            ret_cub_corners(0, 0) = bg::get<bg::min_corner, 0>(actual_cell[k].first);
            ret_cub_corners(1, 0) = bg::get<bg::min_corner, 1>(actual_cell[k].first);
            ret_cub_corners(2, 0) = bg::get<bg::min_corner, 2>(actual_cell[k].first);

            // the max corner of the rect cuboid
            ret_cub_corners(3, 0) = bg::get<bg::max_corner, 0>(actual_cell[k].first);
            ret_cub_corners(4, 0) = bg::get<bg::max_corner, 1>(actual_cell[k].first);
            ret_cub_corners(5, 0) = bg::get<bg::max_corner, 2>(actual_cell[k].first);

            cout << ret_cub_corners << endl;
            cout << endl;
        }
        return false;
    }

    return false;
}

#endif
