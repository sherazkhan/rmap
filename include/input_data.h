#ifndef INPUT_DATA
#define INPUT_DATA
#include <vector>
#include <iostream>
//#include <octomap/octomap.h>
//#include <octomap/octomap_timing.h>

//using namespace octomap;
using namespace cv;


// C++
// ---------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

// PCL
// ---------------------------------------------------------------------------
#include "pcl/common/transforms.h"
#include "pcl/common/eigen.h"
#include "pcl/common/angles.h"

#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d.h>

using namespace pcl;

// Eigen
// ---------------------------------------------------------------------------

#include <Eigen/Dense>

using namespace Eigen;

// namespace of e57
//using namespace e57;


// PTX File Format
// ---------------------------------------------------------------------------

/*
   Number of points per column
   Number of points per row
   X Y Z (Scanner location)
   R3 R3 R3
   R3 R3 R3
   R3 R3 R3 (3x3 rotation matrix - typically identity)
   R4 R4 R4 R4
   R4 R4 R4 R4
   R4 R4 R4 R4
   R4 R4 R4 R4 (4x4 rotation matrix - typically identity)
   X Y Z Intensity R G B (the coordinate of the point, the intensity of the reflection, and the c X Y Z Intensity R G B
   X Y Z Intensity R G B
   ...
   Repeat N Scans
   ...
*/

// Custom Point Types
// ---------------------------------------------------------------------------
struct PointXYZIRGB
{
PCL_ADD_POINT4D;
union
{
  struct
  {
    float intensity;
  };
  float data_i[4];

  union
  {
    struct
    {
      uint8_t b;
      uint8_t g;
      uint8_t r;
      uint8_t _unused;
    };
    float rgb;
  };
  uint32_t rgba;
};

EIGEN_MAKE_ALIGNED_OPERATOR_NEW
} EIGEN_ALIGN16;

struct PointXYZIRGBNormal
{
PCL_ADD_POINT4D;
PCL_ADD_NORMAL4D;
union
{
  struct
  {
    float intensity;
    float curvature;
  };
  float data_c[4];

  union
  {
    struct
    {
      uint8_t b;
      uint8_t g;
      uint8_t r;
      uint8_t _unused;
    };
    float rgb;
  };
  uint32_t rgba;
};

EIGEN_MAKE_ALIGNED_OPERATOR_NEW
} EIGEN_ALIGN16;

POINT_CLOUD_REGISTER_POINT_STRUCT (PointXYZIRGB,
                                  (float, x, x)
                                  (float, y, y)
                                  (float, z, z)
                                  (float, intensity, intensity)
                                  (float, rgb, rgb))

POINT_CLOUD_REGISTER_POINT_STRUCT (PointXYZIRGBNormal,
                                  (float, x, x)
                                  (float, y, y)
                                  (float, z, z)
                                  (float, normal_x, normal_x)
                                  (float, normal_y, normal_y)
                                  (float, normal_z, normal_z)
                                  (float, curvature, curvature)
                                  (float, intensity, intensity)
                                  (float, rgb, rgb))


struct point_xyz{

    point_xyz(double x, double y, double z):x_(x),y_(y),z_(z){

    }
    double x_;
    double y_;
    double z_;
};

struct point_xyzlabel{

    point_xyzlabel(double x, double y, double z, double label):x_(x),y_(y),z_(z),label_(label){

    }
    double x_;
    double y_;
    double z_;
    double label_; // define labels...0 --> car...1 --> sky etc
};

struct point_xyzirgb{

    point_xyzirgb(double x, double y, double z, float intensity, int r, int g, int b):x_(x),y_(y),z_(z),intensity_(intensity),r_(r),g_(g),b_(b){

    }
    double x_;
    double y_;
    double z_;
    float intensity_;
    int r_;
    int g_;
    int b_;
};

struct point_xyzrgb{

    point_xyzrgb(double x, double y, double z, int r, int g, int b):x_(x),y_(y),z_(z),r_(r),g_(g),b_(b){

    }
    double x_;
    double y_;
    double z_;
    int r_;
    int g_;
    int b_;
};

struct point_xyzirgblabel{

    point_xyzirgblabel(double x, double y, double z, float intensity, int r, int g, int b, int r_label, int g_label, int b_label):x_(x),y_(y),z_(z),intensity_(intensity),r_(r),
        g_(g),b_(b),r_label_(r_label),g_label_(g_label),b_label_(b_label){

    }
    point_xyzirgblabel(double x, double y, double z, float intensity, int r, int g, int b, int r_label, int g_label, int b_label, int label):x_(x),y_(y),z_(z),intensity_(intensity),r_(r),
        g_(g),b_(b),r_label_(r_label),g_label_(g_label),b_label_(b_label),label_(label){

    }
    point_xyzirgblabel(double x, double y, double z, float intensity, int r, int g, int b, int r_label, int g_label, int b_label, int label, int id):x_(x),y_(y),z_(z),intensity_(intensity),r_(r),
        g_(g),b_(b),r_label_(r_label),g_label_(g_label),b_label_(b_label),label_(label),object_id_(id){

    }
    double x_;
    double y_;
    double z_;
    float intensity_;
    int r_;
    int g_;
    int b_;
    int r_label_;
    int g_label_;
    int b_label_;
    int label_; // define labels...0 --> car...1 --> sky etc
    unsigned int object_id_;
};

struct pose{

    pose(double x, double y, double z, double roll, double pitch, double yaw):x_(x),y_(y),z_(z),roll_(roll)
      ,pitch_(pitch),yaw_(yaw){

    }
    double x_;
    double y_;
    double z_;
    double roll_;
    double pitch_;
    double yaw_;
};

struct rgb_label{
    float r_;
    float g_;
    float b_;
};

typedef std::vector<point_xyz> point_cloud;
typedef std::vector<point_xyzrgb> point_cloud_rgb;
typedef std::vector<point_xyzirgb> point_cloud_irgb;
typedef std::vector<point_xyzirgblabel> point_cloud_irgblabel;
typedef std::pair <pose, point_cloud> pointcloud_with_pose;
typedef std::pair <pose, point_cloud_rgb> pointcloudrgb_with_pose;
typedef std::pair <pose, point_cloud_irgb> pointcloudirgb_with_pose;
typedef std::pair <pose, point_cloud_irgblabel> pointcloudirgblabel_with_pose;
typedef std::vector<pointcloud_with_pose> scans_with_pose;
typedef std::vector<pointcloudrgb_with_pose> scansrgb_with_pose;
typedef std::vector<pointcloudirgb_with_pose> scansirgb_with_pose;
typedef std::vector<pointcloudirgblabel_with_pose> scansirgblabel_with_pose;

// class to color mapping
typedef std::map <int,rgb_label> mapping_dict;

class input_data{

public:
    input_data(params *myparams):myparams_(myparams){

        // fill the dictionary that has been decided...!!

        // use this instead of fixed classes ... myparams_->get_max_no_labels;
        int total_classes = myparams_->get_max_no_labels();

        for(unsigned int i=0;i< total_classes;i++){
            if(i == 0){ // car label
                rgb_label label_car;
                label_car.r_ = 1.0;
                label_car.g_ = 0.0;
                label_car.b_ = 0.0;
                semantic_dict[i] = label_car;
            }else if(i == 1){ // house label
                rgb_label label_house;
                label_house.r_ = 0.0;
                label_house.g_ = 1.0;
                label_house.b_ = 0.0;
                semantic_dict[i] = label_house;
            }else if(i == 2){ // street label
                rgb_label street;
                street.r_ = 0.0;
                street.g_ = 0.0;
                street.b_ = 1.0;
                semantic_dict[i] = street;
            }else if(i == 3){ // sidewalk label
                rgb_label side_walk;
                side_walk.r_ = 0.5;
                side_walk.g_ = 0.0;
                side_walk.b_ = 0.0;
                semantic_dict[i] = side_walk;
            }else if(i == 4){ // bicycle label
                rgb_label bike;
                bike.r_ = 0.04;
                bike.g_ = 0.52;
                bike.b_ = 0.72;
                semantic_dict[i] = bike;
            }else if(i == 5){ // other label
                rgb_label other;
                other.r_ = 0.50;
                other.g_ = 0.50;
                other.b_ = 0.50;
                semantic_dict[i] = other;
            }else if(i == 6){ // tree label
                rgb_label tree;
                tree.r_ = 0.00;
                tree.g_ = 0.54;
                tree.b_ = 0.27;
                semantic_dict[i] = tree;
            }else if(i == 7){ // pole label
                rgb_label pole;
                pole.r_ = 0.54;
                pole.g_ = 0.54;
                pole.b_ = 0.00;
                semantic_dict[i] = pole;
            }else if(i == 8){ // sky label
                rgb_label sky;
                sky.r_ = 0.50;
                sky.g_ = 0.50;
                sky.b_ = 0.50;
                semantic_dict[i] = sky;
            }else if(i == 9){ // grass label
                rgb_label grass;
                grass.r_ = 0.13;
                grass.g_ = 0.54;
                grass.b_ = 0.13;
                semantic_dict[i] = grass;
            }else if(i == 10){
                rgb_label nothing;
                nothing.r_ = 0.0;
                nothing.g_ = 0.0;
                nothing.b_ = 0.0;
                semantic_dict[i] = nothing;
            }
        }

    }
    pcl::PointCloud<PointXYZIRGB> ptx2pcd(string ptxPath);
    pcl::PointCloud<PointXYZIRGB> ptx2pcd(string ptxPath,Matrix4f &transformation);
    //point_cloud_irgblabel e57_2_cloud(string path, cv::Mat &image);
    //bool get_pointcloud(int scan_number, scansirgb_with_pose &scan,scansrgb_with_pose &semantic_scan);
    bool get_pointcloud(int scan_number, scansirgblabel_with_pose &labeled_scan);
    bool get_pcl_pointcloud(int scan_number, pcl::PointCloud<PointXYZIRGB> &scan);
    bool get_pcd_pointcloud(int scan_number, scansirgblabel_with_pose &labeled_scan, bool get_all_points);
    int get_label(unsigned int r, unsigned int g, unsigned int b);
    int get_label(char c);
    bool read_quaternion(int scan_number, std::vector<double> &quat);
    scans_with_pose read_octomap_file(std::string graphFilename, int max_scan_no);
    boost::shared_ptr<pcl::visualization::PCLVisualizer> rgbVis (pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud);
    point_cloud get_input();
    mapping_dict semantic_dict;

private:
    params *myparams_;

};

// Application
// ---------------------------------------------------------------------------

pcl::PointCloud<PointXYZIRGB> input_data::ptx2pcd(string ptxPath){
   ifstream ptxFile;

   PointCloud<PointXYZIRGB> cloud;

   int width;
   int height;

   string line;
   Vector3f loc;
   Vector3f rot;
   Matrix3f mx3;
   Matrix4f mx4;

   Vector3f pt;
   Vector3f center;
   float i;
   int r, g, b;
   int count = 0;
   int linecount = 0;

   ptxFile.open(ptxPath.c_str());

   // Read width and height
   ptxFile >> width >> height;

   // Iterate over each scan in file
   while(ptxFile.good()){

       // Reset cloud data
       cloud.width = width*height;
       cloud.height = 1;
       cloud.is_dense = false;
       cloud.points.resize(cloud.width * cloud.height);

       // Read scanner location
       ptxFile >> loc.x() >> loc.y() >> loc.z();

       // Info
       //cout << "Cloud #: " << count << endl;
       //cout << "Size: " << width*height << " [" << width << "x" << height << "]" << endl;
       //cout << "Position: " << loc.x() << ", " << loc.y() << ", " << loc.z() << endl;

       // Read 3x3 rotation matrix
       ptxFile >> mx3(0, 0) >> mx3(0, 1) >> mx3(0, 2);
       ptxFile >> mx3(1, 0) >> mx3(1, 1) >> mx3(1, 2);
       ptxFile >> mx3(2, 0) >> mx3(2, 1) >> mx3(2, 2);

       // Read 4x5 rotation matrix
       ptxFile >> mx4(0, 0) >> mx4(0, 1) >> mx4(0, 2) >> mx4(0, 3);
       ptxFile >> mx4(1, 0) >> mx4(1, 1) >> mx4(1, 2) >> mx4(1, 3);
       ptxFile >> mx4(2, 0) >> mx4(2, 1) >> mx4(2, 2) >> mx4(2, 3);
       ptxFile >> mx4(3, 0) >> mx4(3, 1) >> mx4(3, 2) >> mx4(3, 3);

       // Read rest of last line
       getline(ptxFile, line);

       // Create transformation matrix
       mx4.transposeInPlace();
       Affine3f transform(mx4);

       // Info
       center = transform * Vector3f(0, 0, 0);
       //cout << "Center: " << center.x() << ", " << center.y() << ", " << center.z() << endl;

       vector<string> tokens;
       size_t idx = 0;

       // Whether or not the scan has RGB data
       bool hasColor = true;

       // Iterate over all points
       for (size_t n = 0; n < width*height; ++n){
           getline(ptxFile, line);
           if(!ptxFile.good()){
               cerr << "ERROR: Reading File " << linecount << endl;
               return cloud;
           }

           linecount++;

           // Tokenize line
           tokens.clear();
           istringstream iss(line);
           copy(istream_iterator<string>(iss),
                istream_iterator<string>(),
                back_inserter<vector<string> >(tokens));

           // Get XYZI point data
           pt.x() = atof(tokens[0].c_str());
           pt.y() = atof(tokens[1].c_str());
           pt.z() = atof(tokens[2].c_str());
           i = atof(tokens[3].c_str());

           // Get RGB data if it exists
           if(tokens.size() > 4){
               r = atoi(tokens[4].c_str());
               g = atoi(tokens[5].c_str());
               b = atoi(tokens[6].c_str());
           }

           // If point is NaN skip
           if (pt.x() == 0 && pt.y() == 0 && pt.z() == 0 && i == 0.5)
               continue;

           // Transform point
           pt = transform * pt;

           // Set cloud XYZI point data
           cloud.points[idx].x = pt.x();
           cloud.points[idx].y = pt.y();
           cloud.points[idx].z = pt.z();
           cloud.points[idx].intensity = i;

           // Set XYZI point data
           if(tokens.size() > 4){
               hasColor = true;
               cloud.points[idx].r = r;
               cloud.points[idx].g = g;
               cloud.points[idx].b = b;
           }
           idx++;
       }

       // Update cloud size
       cloud.width = idx;
       cloud.points.resize(idx);

       // Read the width and height of the next scan
       ptxFile >> width >> height;
   }


   ptxFile.close();
   return cloud;
}




pcl::PointCloud<PointXYZIRGB> input_data::ptx2pcd(string ptxPath,Matrix4f &transformation){
   ifstream ptxFile;

   PointCloud<PointXYZIRGB> cloud;

   int width;
   int height;

   string line;
   Vector3f loc;
   Vector3f rot;
   Matrix3f mx3;
   Matrix4f mx4;

   Vector3f pt;
   Vector3f center;
   float i;
   int r, g, b;
   int count = 0;
   int linecount = 0;

   ptxFile.open(ptxPath.c_str());

   // Read width and height
   ptxFile >> width >> height;

   // Iterate over each scan in file
   while(ptxFile.good()){

       // Reset cloud data
       cloud.width = width*height;
       cloud.height = 1;
       cloud.is_dense = false;
       cloud.points.resize(cloud.width * cloud.height);

       // Read scanner location
       ptxFile >> loc.x() >> loc.y() >> loc.z();

       // Info
       //cout << "Cloud #: " << count << endl;
       cout << "Size: " << width*height << " [" << width << "x" << height << "]" << endl;
       cout << "Position: " << loc.x() << ", " << loc.y() << ", " << loc.z() << endl;

       // Read 3x3 rotation matrix
       ptxFile >> mx3(0, 0) >> mx3(0, 1) >> mx3(0, 2);
       ptxFile >> mx3(1, 0) >> mx3(1, 1) >> mx3(1, 2);
       ptxFile >> mx3(2, 0) >> mx3(2, 1) >> mx3(2, 2);

       // Read 4x5 rotation matrix
       ptxFile >> mx4(0, 0) >> mx4(0, 1) >> mx4(0, 2) >> mx4(0, 3);
       ptxFile >> mx4(1, 0) >> mx4(1, 1) >> mx4(1, 2) >> mx4(1, 3);
       ptxFile >> mx4(2, 0) >> mx4(2, 1) >> mx4(2, 2) >> mx4(2, 3);
       ptxFile >> mx4(3, 0) >> mx4(3, 1) >> mx4(3, 2) >> mx4(3, 3);

       // Read rest of last line
       getline(ptxFile, line);

       // Create transformation matrix
       mx4.transposeInPlace();
       transformation = mx4;
       Affine3f transform(mx4);

       // Info
       center = transform * Vector3f(0, 0, 0);
       cout << "Center: " << center.x() << ", " << center.y() << ", " << center.z() << endl;

       vector<string> tokens;
       size_t idx = 0;

       // Whether or not the scan has RGB data
       bool hasColor = true;

       // Iterate over all points
       for (size_t n = 0; n < width*height; ++n){
           getline(ptxFile, line);
           if(!ptxFile.good()){
               cerr << "ERROR: Reading File " << linecount << endl;
               return cloud;
           }

           linecount++;

           // Tokenize line
           tokens.clear();
           istringstream iss(line);
           copy(istream_iterator<string>(iss),
                istream_iterator<string>(),
                back_inserter<vector<string> >(tokens));

           // Get XYZI point data
           pt.x() = atof(tokens[0].c_str());
           pt.y() = atof(tokens[1].c_str());
           pt.z() = atof(tokens[2].c_str());
           i = atof(tokens[3].c_str());

           // Get RGB data if it exists
           if(tokens.size() > 4){
               r = atoi(tokens[4].c_str());
               g = atoi(tokens[5].c_str());
               b = atoi(tokens[6].c_str());
           }

           // If point is NaN skip
           if (pt.x() == 0 && pt.y() == 0 && pt.z() == 0 && i == 0.5)
               continue;

           // Transform point
           pt = transform * pt;

           // Set cloud XYZI point data
           cloud.points[idx].x = pt.x();
           cloud.points[idx].y = pt.y();
           cloud.points[idx].z = pt.z();
           cloud.points[idx].intensity = i;

           // Set XYZI point data
           if(tokens.size() > 4){
               hasColor = true;
               cloud.points[idx].r = r;
               cloud.points[idx].g = g;
               cloud.points[idx].b = b;
           }
           idx++;
       }

       // Update cloud size
       cloud.width = idx;
       cloud.points.resize(idx);

       // Read the width and height of the next scan
       ptxFile >> width >> height;
   }


   ptxFile.close();
   return cloud;
}

bool input_data::read_quaternion(int scan_number, std::vector<double> &quat){

    stringstream ss;
    ss << scan_number;
    string number = ss.str();
    string quat_directory = myparams_->quaternion_directory_ + number + ".txt";

    cout << " Quat directory: " << quat_directory <<endl;

    string line;
    ifstream myfile (quat_directory.c_str());
    if (myfile.is_open())
    {
        while ( getline (myfile,line) )
        {
            std::string token;
            std::istringstream iss(line);

            while ( getline(iss, token, ' ') )
            {
                std::istringstream stm;
                stm.str(token);
                double d;
                stm >>d;
                quat.push_back(d);
                //dirichlet_params
            }

        }
        myfile.close();

    }

    else cout << "Unable to open file";

}


boost::shared_ptr<pcl::visualization::PCLVisualizer> input_data::rgbVis (pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud)
{
  // --------------------------------------------
  // -----Open 3D viewer and add point cloud-----
  // --------------------------------------------
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
  viewer->setBackgroundColor (0, 0, 0);
  pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud);
  viewer->addPointCloud<pcl::PointXYZRGB> (cloud, rgb, "sample cloud");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
  viewer->initCameraParameters ();
  return (viewer);
}

/*
point_cloud_irgblabel input_data::e57_2_cloud(string path, cv::Mat &image){

    point_cloud_irgblabel cloud_irgblabel;
    // Read file from disk
    ImageFile imf(path, "r");
    StructureNode root = imf.root();

    // Make sure vector of scans is defined and of expected type.
    // If "/data3D" wasn't defined, the call to root.get below would raise an exception.
    if (!root.isDefined("/data3D")) {
        cout << "File doesn't contain 3D images" << endl;
        return cloud_irgblabel;
    }
    Node n = root.get("/data3D");
    if (n.type() != E57_VECTOR) {
        cout << "bad file" << endl;
        return cloud_irgblabel;
    }

    // The node is a vector so we can safely get a VectorNode handle to it.
    // If n was not a VectorNode, this would raise an exception.
    VectorNode data3D(n);

    // Print number of children of data3D.  This is the number of scans in file.
    int64_t scanCount = data3D.childCount();

    // For each scan, print out first 4 points in either Cartesian or Spherical coordinates.
    for (int scanIndex = 0; scanIndex < scanCount; scanIndex++) {
        // Get scan from "/data3D", assume its a Structure (else get exception)
        StructureNode scan(data3D.get(scanIndex));

        // Get "points" field in scan.  Should be a CompressedVectorNode.
        CompressedVectorNode points(scan.get("points"));

        const int total_points = points.childCount();
        cout << " Total number of points in point cloud " <<  total_points << endl;

        //////////// Read data
        double *xData = new double[total_points];
        double *yData = new double[total_points];
        double *zData = new double[total_points];
        double *intensity = new double[total_points];
        uint16_t *r = new uint16_t[total_points];
        uint16_t *g = new uint16_t[total_points];
        uint16_t *b = new uint16_t[total_points];

        vector<SourceDestBuffer> destBuffers;
        destBuffers.push_back(SourceDestBuffer(imf, "cartesianX", xData, total_points, true));
        destBuffers.push_back(SourceDestBuffer(imf, "cartesianY", yData, total_points, true));
        destBuffers.push_back(SourceDestBuffer(imf, "cartesianZ", zData, total_points, true));
        destBuffers.push_back(SourceDestBuffer(imf, "intensity", intensity, total_points, true));
        destBuffers.push_back(SourceDestBuffer(imf, "colorRed", r, total_points, true));
        destBuffers.push_back(SourceDestBuffer(imf, "colorGreen", g, total_points, true));
        destBuffers.push_back(SourceDestBuffer(imf, "colorBlue", b, total_points, true));

        // read the data
        CompressedVectorReader reader = points.reader(destBuffers);
        unsigned gotCount = reader.read();

        // copy data to our type
        for(unsigned int i=0;i< total_points;i++){

            // If point is NaN skip
            if (xData[i] == 0 && yData[i] == 0 && zData[i] == 0 && intensity[i] == 0.5)
                continue;

            // data for labeled scan
            div_t divresult;
            divresult = div(i,image.cols);
            //cout << " " << labeled_image.cols << " " << i << " " << j << " " << divresult.quot << " " << divresult.rem <<endl;
            unsigned char red = image.at<cv::Vec3b>(divresult.quot,divresult.rem)[2];
            unsigned char green = image.at<cv::Vec3b>(divresult.quot,divresult.rem)[1];
            unsigned char blue = image.at<cv::Vec3b>(divresult.quot,divresult.rem)[0];
            unsigned int r_label = (unsigned int) red;
            unsigned int g_label = (unsigned int) green;
            unsigned int b_label = (unsigned int) blue;

            // decide point id here...!!!
            point_xyzirgblabel temp_point(xData[i],yData[i],zData[i], intensity[i],r[i],g[i],b[i],r_label,g_label,b_label);
            cloud_irgblabel.push_back(temp_point);

        }

        reader.close();

    }

    return cloud_irgblabel;

}
*/

int input_data::get_label(unsigned int r, unsigned int g, unsigned int b){

    int label = 5;
    if(b == 0 && g == 0 && r == 255){ // car label
        label = 0;
    }else if(b == 0 && g == 255 && r == 0){ // house label
        label = 1;
    }else if(b == 255 && g == 0 && r == 0){ // street label
        label = 2;
    }else if(b == 0 && g == 0 && r == 128){ // sidewalk label
        label = 3;
    }else if(b == 184 && g == 134 && r == 11){ // bicycle label
        label = 4;
    }else if(b == 128 && g == 128 && r == 128){ // other label
        label = 5;
    }else if(b == 69 && g == 139 && r == 0){ // tree label
        label = 6;
    }else if(b == 0 && g == 139 && r == 139){ // pole label
        label = 7;
    }else if(b == 128 && g == 128 && r == 128){ // sky label
        label = 8;
    }else if(b == 34 && g == 139 && r == 34){ // grass label
        label = 9;
    }else{
        label = 5;
    }

    return label;
}

int input_data::get_label(char c){

    stringstream ss;
    string val;

    ss << c;
    ss >> val;

    if(val == "c"){
        return 0;
    }else if(val == "h"){
        return 1;
    }else if(val == "s"){
        return 2;
    }else if(val == "w"){
        return 3;
    }else if(val == "b"){
        return 4;
    }else if(val == "o"){
        return 5;
    }else if(val == "t"){
        return 6;
    }else if(val == "p"){
        return 7;
    }else if(val == "u"){
        return 8;
    }else if(val == "g"){
        return 9;
    }else if(val == "q"){
        return 100;
    }else if(val == "e"){
        return -1;
    }else{
        return 10;
    }
}

bool input_data::get_pcl_pointcloud(int scan_number, pcl::PointCloud<PointXYZIRGB> &scan){

    stringstream ss;
    ss << scan_number;
    string number = ss.str();

    string directory = myparams_->pointcloud_directory;
    string pointcloud = directory + myparams_->image_start_label + number + myparams_->pointcloud_extension;
    //string pointcloud = directory + "test_scan" + myparams_->pointcloud_extension;

    cout << pointcloud << endl;
    timeval start;
    timeval stop;

    gettimeofday(&start, NULL);  // start timer

    scan = ptx2pcd(pointcloud);

    gettimeofday(&stop, NULL);  // stop timer
    double time_to_insert = (stop.tv_sec - start.tv_sec) + 1.0e-6 *(stop.tv_usec - start.tv_usec);

    cout << time_to_insert << endl;

    return true;

}

bool input_data::get_pcd_pointcloud(int scan_number, scansirgblabel_with_pose &labeled_scan, bool get_all_points=false){

    if(scan_number < myparams_->min_image_index_ || scan_number > myparams_->max_image_index_){
        cout << " Scan number does not adhere to limits " <<endl;
        return false;
    }
    stringstream ss;
    ss << scan_number;
    string number = ss.str();

    string directory = myparams_->image_directory;
    string image = directory + "final_labeled_images/" + myparams_->image_start_label + number + myparams_->segmented_image_label + myparams_->image_extension;
    string object = directory + "final_labeled_images/" + myparams_->image_start_label + number + myparams_->object_image_label + myparams_->image_extension;

    cout << " Labeled image directory " << image << endl;
    cout << " Object image directory " << object << endl;


    // read color image
    Mat labeled_image = imread(image);
    Mat object_image = imread(object);

    // check if image exists
    if(!labeled_image.data || !object_image.data){
        cout << " Image or Object image does not exist...check the directory " <<endl;
        return false;
    }


    //string pointcloud = directory + myparams_->image_start_label + number + "_mod" + myparams_->pointcloud_extension;
    string pointcloud_filename = myparams_->pointcloud_directory + myparams_->image_start_label + number + myparams_->pointcloud_extension;

    cout << " Trying to read pointcloud from " << pointcloud_filename << endl;
    timeval start;
    timeval stop;

    gettimeofday(&start, NULL);  // start timer

    pcl::PointCloud<PointXYZIRGB> cloud;
    pcl::io::loadPCDFile<PointXYZIRGB> (pointcloud_filename, cloud);

    gettimeofday(&stop, NULL);  // stop timer
    double time_to_insert = (stop.tv_sec - start.tv_sec) + 1.0e-6 *(stop.tv_usec - start.tv_usec);

    cout << " Time to read pointcloud: " << time_to_insert << endl;
    cout << " Read the file " <<endl;
    cout << " Point cloud size " << cloud.size() <<endl;

    point_cloud_irgblabel cloud_irgblabel;
    pose robot_pose(0.0,0.0,0.0,0.0,0.0,0.0);

    std::vector<unsigned int> ids;
    for(unsigned int j=0;j< cloud.size();j++){

        // data for labeled scan
        div_t divresult;
        divresult = div(j,labeled_image.cols);
        //cout << " " << labeled_image.cols << " " << i << " " << j << " " << divresult.quot << " " << divresult.rem <<endl;
        unsigned char red = labeled_image.at<cv::Vec3b>(divresult.quot,divresult.rem)[2];
        unsigned char green = labeled_image.at<cv::Vec3b>(divresult.quot,divresult.rem)[1];
        unsigned char blue = labeled_image.at<cv::Vec3b>(divresult.quot,divresult.rem)[0];
        unsigned int r_label = (unsigned int) red;
        unsigned int g_label = (unsigned int) green;
        unsigned int b_label = (unsigned int) blue;

        // decide point label here...!!!
        int label = get_label(r_label, g_label, b_label);

        if (!get_all_points && (label == 8 || label == 5)) // sky-other points or might be a point with no info...so skip it...
           continue;

        // also decide object index
        unsigned char val = object_image.at<cv::Vec3b>(divresult.quot,divresult.rem)[2];
        unsigned int obj_id = (unsigned int) val;

        // unpack rgb into r/g/b
        uint32_t rgb = *reinterpret_cast<int*>(&cloud[j].rgb);
        uint8_t r = (rgb >> 16) & 0x0000ff;
        uint8_t g = (rgb >> 8) & 0x0000ff;
        uint8_t b = (rgb) & 0x0000ff;

        point_xyzirgblabel temp_point(cloud[j].x,cloud[j].y,cloud[j].z, cloud[j].intensity,r,g,b,r_label,g_label,b_label,label,obj_id);
        cloud_irgblabel.push_back(temp_point);

    }

    pointcloudirgblabel_with_pose labeled_cloud = make_pair(robot_pose,cloud_irgblabel);
    labeled_scan.push_back(labeled_cloud);


    return true;

}

bool input_data::get_pointcloud(int scan_number, scansirgblabel_with_pose &labeled_scan){


    if(scan_number < myparams_->min_image_index_ || scan_number > myparams_->max_image_index_){
        return false;
    }
    stringstream ss;
    ss << scan_number;
    string number = ss.str();

    string directory = myparams_->image_directory;
    string image = directory + "final_labeled_images/" + myparams_->image_start_label + number + myparams_->segmented_image_label + myparams_->image_extension;
    string object = directory + "final_labeled_images/" + myparams_->image_start_label + number + myparams_->object_image_label + myparams_->image_extension;

    cout << " Labeled image directory " << image <<endl;
    cout << " Object image directory " << object <<endl;


    // read color image
    Mat labeled_image = imread(image);
    Mat object_image = imread(object);

    // check if image exists
    if(!labeled_image.data || !object_image.data){
        cout << " Image or Object image does not exist...check the directory " <<endl;
        return false;
    }

    string pointcloud_filename = myparams_->pointcloud_directory + myparams_->image_start_label + number + myparams_->pointcloud_extension;

    cout << pointcloud_filename <<endl;
    timeval start;
    timeval stop;

    gettimeofday(&start, NULL);  // start timer

    pcl::PointCloud<PointXYZIRGB> cloud = ptx2pcd(pointcloud_filename);

    gettimeofday(&stop, NULL);  // stop timer
    double time_to_insert = (stop.tv_sec - start.tv_sec) + 1.0e-6 *(stop.tv_usec - start.tv_usec);

    cout << time_to_insert << endl;

    cout << " Read the file " <<endl;

    point_cloud_irgblabel cloud_irgblabel;
    pose robot_pose(0.0,0.0,0.0,0.0,0.0,0.0);

    std::vector<unsigned int> ids;
    for(unsigned int j=0;j< cloud.size();j++){

        // data for labeled scan
        div_t divresult;
        divresult = div(j,labeled_image.cols);
        //cout << " " << labeled_image.cols << " " << i << " " << j << " " << divresult.quot << " " << divresult.rem <<endl;
        unsigned char red = labeled_image.at<cv::Vec3b>(divresult.quot,divresult.rem)[2];
        unsigned char green = labeled_image.at<cv::Vec3b>(divresult.quot,divresult.rem)[1];
        unsigned char blue = labeled_image.at<cv::Vec3b>(divresult.quot,divresult.rem)[0];
        unsigned int r_label = (unsigned int) red;
        unsigned int g_label = (unsigned int) green;
        unsigned int b_label = (unsigned int) blue;

        // decide point label here...!!!
        int label = get_label(r_label, g_label, b_label);

        if(label == 8 || label == 5) // sky-other points or might be a point with no info...so skip it...
            continue;

        // also decide object index
        unsigned char val = object_image.at<cv::Vec3b>(divresult.quot,divresult.rem)[2];
        unsigned int obj_id = (unsigned int) val;

        // unpack rgb into r/g/b
        uint32_t rgb = *reinterpret_cast<int*>(&cloud[j].rgb);
        uint8_t r = (rgb >> 16) & 0x0000ff;
        uint8_t g = (rgb >> 8) & 0x0000ff;
        uint8_t b = (rgb) & 0x0000ff;

        point_xyzirgblabel temp_point(cloud[j].x,cloud[j].y,cloud[j].z, cloud[j].intensity,r,g,b,r_label,g_label,b_label,label,obj_id);
        cloud_irgblabel.push_back(temp_point);

    }

    pointcloudirgblabel_with_pose labeled_cloud = make_pair(robot_pose,cloud_irgblabel);
    labeled_scan.push_back(labeled_cloud);


    return true;

}

/*
// method to read octomap files into the occupancy grid...
scans_with_pose input_data::read_octomap_file(string graphFilename, int max_scan_no){
    // the directory for the octomap file should be absolute...

    cout << "\nReading Graph file\n===========================\n";
    ScanGraph* graph = new ScanGraph();
    if (!graph->readBinary(graphFilename))
      exit(2);

    unsigned int num_points_in_graph = 0;
    if (max_scan_no > 0) {
      num_points_in_graph = graph->getNumPoints(max_scan_no-1);
      //cout << "\n Data points in graph up to scan " << max_scan_no << ": " << num_points_in_graph << endl;
    }
    else {
      num_points_in_graph = graph->getNumPoints();
      max_scan_no = graph->end() - graph->begin();
      //cout << "\n Data points in graph: " << num_points_in_graph << endl;
    }

    scans_with_pose scans;

    // transform pointclouds first, so we can directly operate on them later
    for (ScanGraph::iterator scan_it = graph->begin(); scan_it != (graph->begin() + max_scan_no); scan_it++) {

      pose6d frame_origin = (*scan_it)->pose;
      point3d sensor_origin = frame_origin.inv().transform((*scan_it)->pose.trans());

      (*scan_it)->scan->transform(frame_origin);
      point3d transformed_sensor_origin = frame_origin.transform(sensor_origin);
      (*scan_it)->pose = pose6d(transformed_sensor_origin, octomath::Quaternion());

      // transfer to the required format
      pose mypose((*scan_it)->pose.x(), (*scan_it)->pose.y(), (*scan_it)->pose.z()
                  ,(*scan_it)->pose.roll(),(*scan_it)->pose.pitch(), (*scan_it)->pose.yaw() );

      point_cloud mycloud;

      ScanNode node = **scan_it;
      Pointcloud& cloud = *(node.scan);
      for (Pointcloud::const_iterator point_it = cloud.begin(); point_it != cloud.end(); point_it++) {
          const point3d& p = *point_it;
          point_xyz mypoint(p.x(),p.y(),p.z());
          mycloud.push_back(mypoint);
      }

      pointcloud_with_pose scan = make_pair(mypose,mycloud);
      scans.push_back(scan);

    }

    return scans;

}

*/
/*
    //point_xyzrgb labeled_point(cloud[j].x,cloud[j].y,cloud[j].z,r_,g_,b_);
    //cloud_rgb.push_back(labeled_point);

    //point_xyzirgb temp_point(cloud[j].x,cloud[j].y,cloud[j].z, cloud[j].intensity,r,g,b);
    //cloud_irgb.push_back(temp_point);


//pointcloudirgb_with_pose temp_cloud = make_pair(robot_pose,cloud_irgb);
//pointcloudrgb_with_pose labeled_cloud = make_pair(robot_pose,cloud_rgb);
//labeled_scan.push_back(labeled_cloud);
//scanirgb.push_back(temp_cloud);


pcl::PointCloud<PointXYZRGB> all_cloud;


for(unsigned int j=0;j< cloud.size();j++){

   pcl::PointXYZRGB temp_point;
   temp_point.x = cloud[j].x;
   temp_point.y = cloud[j].y;
   temp_point.z = cloud[j].z;
   //temp_point.rgb = cloud[j].rgb;
   all_cloud.push_back(temp_point);

}

unsigned int val = 0;
for(unsigned int i=0;i<labeled_image.rows;i++){
    for(unsigned int j=0;j<labeled_image.cols;j++){
        val = i*labeled_image.cols + j;
        div_t divresult;
        divresult = div(val,labeled_image.cols);
        cout << " " << labeled_image.cols << " " << i << " " << j << " " << divresult.quot << " " << divresult.rem <<endl;
        unsigned char r = labeled_image.at<cv::Vec3b>(i,j)[2];
        unsigned char g = labeled_image.at<cv::Vec3b>(i,j)[1];
        unsigned char b = labeled_image.at<cv::Vec3b>(i,j)[0];
        unsigned int r_ = (unsigned int) r;
        unsigned int g_ = (unsigned int) g;
        unsigned int b_ = (unsigned int) b;
        cloud_rgb[val].r_ = r_;
        cloud_rgb[val].g_ = g_;
        cloud_rgb[val].b_ = b_;
     }
}

unsigned int val = 0;

for(unsigned int i=0;i<col_image.rows;i++){
    for(unsigned int j=0;j<col_image.cols;j++){
        val = i*col_image.cols + j;
        unsigned char r = col_image.at<cv::Vec3b>(i,j)[2];
        unsigned char g = col_image.at<cv::Vec3b>(i,j)[1];
        unsigned char b = col_image.at<cv::Vec3b>(i,j)[0];
        unsigned int r_ = (unsigned int) r;
        unsigned int g_ = (unsigned int) g;
        unsigned int b_ = (unsigned int) b;
        uint32_t rgb = ((uint32_t)r_ << 16) | ((uint32_t)g_ << 8) | (uint32_t)b_;
        all_cloud[val].rgb = *reinterpret_cast<float*>(&rgb);

    }
}

cout << " Copying data done " <<endl;

pcl::PointCloud<PointXYZRGB>::Ptr mycloud;
mycloud.reset(new pcl::PointCloud<pcl::PointXYZRGB> (all_cloud));

boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
viewer = rgbVis(mycloud);

std::cout << mycloud->size() <<std::endl;

 //--------------------
// -----Main loop-----
//--------------------
while (!viewer->wasStopped ())
{
  viewer->spinOnce (100);
  boost::this_thread::sleep (boost::posix_time::microseconds (100000));
}
*/

#endif
