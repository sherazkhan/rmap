#ifndef GRID_FNS
#define GRID_FNS

///////////
// helper functions to operate on the occupancy grid...
// such as point mapping to grid and ray tracing through the grid!!!
////////////

#include <iostream>
#include "params.h"
#include <math.h>
#include <numeric>      // std::accumulate
#include "statistics.hpp"  // provides grid_cell

/*
namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;
namespace bdru = bgi::detail::rtree::utilities;

using namespace std;

typedef bg::model::point<int, 3, bg::cs::cartesian> point;
typedef std::vector <point> points;
typedef bg::model::box<point> rect_cuboid;
typedef pair<rect_cuboid, grid_cell*> value;
typedef std::vector<rect_cuboid> rect_cuboids;
typedef std::vector<value> grid_cells;
*/

class grid_functions{

public:
    grid_functions();
    grid_functions(params *params);
    rect_cuboid map_to_grid(point_xyz mypoint);
    rect_cuboid map_to_grid(point_xyzlabel mypoint);
    rect_cuboid get_cuboid(point_xyz min_point,point_xyz max_point);
    rect_cuboid get_cuboid_from_min_pt(point min_point_rc);
    void get_centroid(rect_cuboid cell, double &centroid_x, double &centroid_y, double &centroid_z);
    bool get_search_information(rect_cuboid cell, cuboid_corners &cub_corners, search_mat &search_cell,
                                search_mat &search_dir,std::vector<unsigned int> &search_order);
    bool check_coverage_and_expansion(grid_cells &returned_cells, cuboid_corners shifted_cuboid, cuboid_corners &expanded_cuboid, unsigned int index_search);
    bool check_coverage_and_expansion_generalized(grid_cells &returned_cells, cuboid_corners shifted_cuboid, cuboid_corners &expanded_cuboid, unsigned int index_search);
    rect_cuboids trace_ray(point_xyz in_point, point_xyz out_point);
    int assign_label(value cell, dirichlet_params params);
    double evaluate_dirichlet(std::vector<double> prob, concentration_params params);
    params *myparams_;

};

grid_functions::grid_functions(params *params):myparams_(params){

}

void grid_functions::get_centroid(rect_cuboid cell, double &centroid_x, double &centroid_y, double &centroid_z){

    // the min corner of the rect cuboid
    int min_x = bg::get<bg::min_corner, 0>(cell);
    int min_y = bg::get<bg::min_corner, 1>(cell);
    int min_z = bg::get<bg::min_corner, 2>(cell);

    // the max corner of the rect cuboid
    int max_x = bg::get<bg::max_corner, 0>(cell);
    int max_y = bg::get<bg::max_corner, 1>(cell);
    int max_z = bg::get<bg::max_corner, 2>(cell);

    centroid_x = ( (double) min_x + (double) max_x )/2;
    centroid_y = ( (double) min_y + (double) max_y )/2;
    centroid_z = ( (double) min_z + (double) max_z )/2;

}

bool grid_functions::get_search_information(rect_cuboid cell, cuboid_corners &cub_corners, search_mat &search_cell,
                                            search_mat &search_direction, std::vector<unsigned int> &search_order){

    cub_corners(0,0) = bg::get<bg::min_corner, 0>(cell);
    cub_corners(1,0) = bg::get<bg::min_corner, 1>(cell);
    cub_corners(2,0) = bg::get<bg::min_corner, 2>(cell);

    // the max corner of the rect cuboid
    cub_corners(3,0) = bg::get<bg::max_corner, 0>(cell);
    cub_corners(4,0) = bg::get<bg::max_corner, 1>(cell);
    cub_corners(5,0) = bg::get<bg::max_corner, 2>(cell);

    int width_x = cub_corners(3,0) - cub_corners(0,0);
    int width_y = cub_corners(4,0) - cub_corners(1,0);
    int width_z = cub_corners(5,0) - cub_corners(2,0);


    /*
    // check
    search_order.push_back(0); // positive x
    search_order.push_back(3); //
    search_order.push_back(1);
    search_order.push_back(4);
    search_order.push_back(2);
    search_order.push_back(5);

    search_cell <<      width_x,0,0,width_x,0,0, // first along x, then y ...etc
                        -width_x,0,0,-width_x,0,0,
                        0,width_y,0,0,width_y,0,
                        0,-width_y,0,0,-width_y,0,
                        0,0,width_z,0,0,width_z,
                        0,0,-width_z,0,0,-width_z;

    search_direction << 0,0,0,width_x,0,0, // first along x, then y ...etc
                        -width_x,0,0,0,0,0,
                        0,0,0,0,width_y,0,
                        0,-width_y,0,0,0,0,
                        0,0,0,0,0,width_z,
                        0,0,-width_z,0,0,0;
    return true;
     */


    // rewrite this part...such that the search direction is based on the covariance matrix or
    // the biased based on the grid cell size....

    if( (width_x >= width_y) && (width_x >= width_z) ){

    ///// ORGINAL CODE!!!!

        search_order.push_back(0);
        search_order.push_back(3);
        search_order.push_back(1);
        search_order.push_back(2);
        search_order.push_back(4);
        search_order.push_back(5);

        search_cell <<      width_x,0,0,width_x,0,0, // first along x, then y ...etc
                            -width_x,0,0,-width_x,0,0,
                            0,width_y,0,0,width_y,0,
                            0,0,width_z,0,0,width_z,
                            0,-width_y,0,0,-width_y,0,
                            0,0,-width_z,0,0,-width_z;

        search_direction << 0,0,0,width_x,0,0, // first along x, then y ...etc
                            -width_x,0,0,0,0,0,
                            0,0,0,0,width_y,0,
                            0,0,0,0,0,width_z,
                            0,-width_y,0,0,0,0,
                            0,0,-width_z,0,0,0;
        return true;


    }else if( (width_y >= width_x) && (width_y >= width_z) ){

        search_order.push_back(1);
        search_order.push_back(4);
        search_order.push_back(0);
        search_order.push_back(2);
        search_order.push_back(3);
        search_order.push_back(5);

        search_cell <<      0,width_y,0,0,width_y,0,
                            0,-width_y,0,0,-width_y,0,
                            width_x,0,0,width_x,0,0, // first along x, then y ...etc
                            0,0,width_z,0,0,width_z,
                            -width_x,0,0,-width_x,0,0,
                            0,0,-width_z,0,0,-width_z;

        search_direction << 0,0,0,0,width_y,0,
                            0,-width_y,0,0,0,0,
                            0,0,0,width_x,0,0, // first along x, then y ...etc
                            0,0,0,0,0,width_z,
                            -width_x,0,0,0,0,0,
                            0,0,-width_z,0,0,0;

        return true;


    }else if( (width_z >= width_x) && (width_z >= width_y) ){

        search_order.push_back(2);
        search_order.push_back(5);
        search_order.push_back(1);
        search_order.push_back(0);
        search_order.push_back(4);
        search_order.push_back(3);

        search_cell <<      0,0,width_z,0,0,width_z,
                            0,0,-width_z,0,0,-width_z,
                            0,width_y,0,0,width_y,0,
                            width_x,0,0,width_x,0,0, // first along x, then y ...etc
                            0,-width_y,0,0,-width_y,0,
                            -width_x,0,0,-width_x,0,0;


        search_direction << 0,0,0,0,0,width_z,
                            0,0,-width_z,0,0,0,
                            0,0,0,0,width_y,0,
                            0,0,0,width_x,0,0, // first along x, then y ...etc
                            0,-width_y,0,0,0,0,
                            -width_x,0,0,0,0,0;

        return true;

    }else{
        cout << " Missed some case " << endl;
        return false;
    }




}

bool grid_functions::check_coverage_and_expansion(grid_cells &returned_cells, cuboid_corners shifted_cuboid, cuboid_corners &expanded_cuboid, unsigned int search_index){

    int width_shifted_cuboid_x = shifted_cuboid(3,0) - shifted_cuboid(0,0);
    int width_shifted_cuboid_y = shifted_cuboid(4,0) - shifted_cuboid(1,0);
    int width_shifted_cuboid_z = shifted_cuboid(5,0) - shifted_cuboid(2,0);

    int min_x = shifted_cuboid(0,0);
    int min_y = shifted_cuboid(1,0);
    int min_z = shifted_cuboid(2,0);
    int max_x = shifted_cuboid(3,0);
    int max_y = shifted_cuboid(4,0);
    int max_z = shifted_cuboid(5,0);

    int total_volume = width_shifted_cuboid_x*width_shifted_cuboid_y*width_shifted_cuboid_z;
    bool coverage[total_volume];

    // coverage along width as false...
    std::fill_n(coverage,total_volume,false);

    int min_bound_x, min_bound_y, min_bound_z, max_bound_x, max_bound_y, max_bound_z;

    // check coverage and if any cuboid crosses the boundary. Additionally find min and max bound along all dimensions
    for(unsigned int k=0;k< returned_cells.size();k++){

        int temp_cuboid_min_x = bg::get<bg::min_corner, 0>(returned_cells[k].first);
        int temp_cuboid_min_y = bg::get<bg::min_corner, 1>(returned_cells[k].first);
        int temp_cuboid_min_z = bg::get<bg::min_corner, 2>(returned_cells[k].first);

        // the max corner of the rect cuboid
        int temp_cuboid_max_x = bg::get<bg::max_corner, 0>(returned_cells[k].first);
        int temp_cuboid_max_y = bg::get<bg::max_corner, 1>(returned_cells[k].first);
        int temp_cuboid_max_z = bg::get<bg::max_corner, 2>(returned_cells[k].first);

        if( ( search_index == 0 || search_index == 3 )  ){

            // check y and z limit
            if( (temp_cuboid_min_y < min_y || temp_cuboid_min_z < min_z || temp_cuboid_max_y > max_y) || temp_cuboid_max_z > max_z ){
                //cout << " Cuboid exceeds y and z dimension during expansion along x " <<endl;
                return false;
            }


        }else if( ( search_index == 1 || search_index == 4 )  ){

            if( (temp_cuboid_min_x < min_x || temp_cuboid_min_z < min_z || temp_cuboid_max_x > max_x) || temp_cuboid_max_z > max_z ){
                //cout << " Cuboid exceeds x and z dimension during expansion along y " <<endl;
                return false;
            }

        }else if( ( search_index == 2 || search_index == 5 )  ){

            if( (temp_cuboid_min_x < min_x || temp_cuboid_min_y < min_y || temp_cuboid_max_x > max_x) || temp_cuboid_max_y > max_y ){
                //cout << " Cuboid exceeds x and y dimension during expansion along z " <<endl;
                return false;
            }

        }

        int min_band_x, min_band_y, min_band_z, max_band_x, max_band_y, max_band_z;

        // the band to be held true in case of x ....
        if(temp_cuboid_min_x <= min_x){
            // confirm if this is correct...
            min_band_x = 0;
        }else{
            min_band_x = temp_cuboid_min_x - min_x;
        }

        if(temp_cuboid_max_x >= max_x){
            max_band_x = max_x - min_x - 1;
        }else{
            max_band_x = temp_cuboid_max_x - min_x - 1;
        }

        // the band to be held true in case of y...
        if(temp_cuboid_min_y <= min_y){
            min_band_y = 0;
        }else{
            min_band_y = temp_cuboid_min_y - min_y;
        }

        if(temp_cuboid_max_y >= max_y){
            max_band_y = max_y - min_y - 1;
        }else{
            max_band_y = temp_cuboid_max_y - min_y - 1;
        }

        // the band to be held true along z...
        if(temp_cuboid_min_z <= min_z){
            min_band_z = 0;
        }else{
            min_band_z = temp_cuboid_min_z - min_z;
        }

        if(temp_cuboid_max_z >= max_z){
            max_band_z = max_z - min_z - 1;
        }else{
            max_band_z = temp_cuboid_max_z - min_z - 1;
        }

        /*   ////PREVIOUS CODE TO GENERATE INDEXES.....was incorrect...!!
        // convert to a one dimensional index
        int min_band = min_band_x + min_band_y*(width_shifted_cuboid_x) + min_band_z*(width_shifted_cuboid_x*width_shifted_cuboid_y);
        int max_band = max_band_x + max_band_y*(width_shifted_cuboid_x) + max_band_z*(width_shifted_cuboid_x*width_shifted_cuboid_y);

        //for(int l = min_band; l<= max_band;l++){
        //    coverage[l] = true;
        //}
        */

        std::vector<int> indexes_coverage;

        for(int index_x=min_band_x;index_x <= max_band_x;index_x++){
            for(int index_y=min_band_y; index_y <= max_band_y;index_y++){
                for(int index_z=min_band_z; index_z <= max_band_z;index_z++){

                    // convert to a one dimensional index
                    int index = index_x + index_y*(width_shifted_cuboid_x) + index_z*(width_shifted_cuboid_x*width_shifted_cuboid_y);

                    //push back index...
                    indexes_coverage.push_back(index);
                }
            }
        }

        for(int l = 0; l< indexes_coverage.size();l++){
            coverage[indexes_coverage[l]] = true;
        }

        if( k==0 ){
            min_bound_x = temp_cuboid_min_x;
            max_bound_x = temp_cuboid_max_x;
            min_bound_y = temp_cuboid_min_y;
            max_bound_y = temp_cuboid_max_y;
            min_bound_z = temp_cuboid_min_z;
            max_bound_z = temp_cuboid_max_z;
        }

        // find min bound along x....
        if( min_bound_x > temp_cuboid_min_x ){
            min_bound_x = temp_cuboid_min_x;
        }

        // find min bound along y....
        if( min_bound_y > temp_cuboid_min_y ){
            min_bound_y = temp_cuboid_min_y;
        }

        // find min bound along z....
        if( min_bound_z > temp_cuboid_min_z ){
            min_bound_z = temp_cuboid_min_z;
        }

        // find max bound along x....
        if( max_bound_x < temp_cuboid_max_x ){
            max_bound_x = temp_cuboid_max_x;
        }

        // find max bound along y....
        if( max_bound_y < temp_cuboid_max_y ){
            max_bound_y = temp_cuboid_max_y;
        }

        // find max bound along z....
        if( max_bound_z < temp_cuboid_max_z ){
            max_bound_z = temp_cuboid_max_z;
        }

    }

    // check total coverage
    bool total_coverage = true;
    for(unsigned int m = 0;m < total_volume;m++){
        total_coverage = (total_coverage && coverage[m]);
    }

    //cout << " Total coverage " << total_coverage <<endl;

    if(!total_coverage){
        return false;
    }else if( ( search_index == 0 || search_index == 3 ) && (total_coverage) && (min_bound_x == min_x) && (max_bound_x == max_x) ){
        return true;
    }else if( ( search_index == 1 || search_index == 4 ) && (total_coverage) && (min_bound_y == min_y) && (max_bound_y == max_y) ){
        return true;
    }else if( ( search_index == 2 || search_index == 5 ) && (total_coverage) && (min_bound_z == min_z) && (max_bound_z == max_z) ){
        return true;
    }else if( ( search_index == 0 ) && (total_coverage) && (min_bound_x == min_x) && (max_bound_x > max_x)
              && (width_shifted_cuboid_y == 1) && (width_shifted_cuboid_z == 1)){
        expanded_cuboid(3,0) = max_bound_x;
        return true;
    }else if( ( search_index == 1 ) && (total_coverage) && (min_bound_y == min_y) && (max_bound_y > max_y)
              && (width_shifted_cuboid_x == 1) && (width_shifted_cuboid_z == 1)){
        expanded_cuboid(4,0) = max_bound_y;
        return true;
    }else if( ( search_index == 2 ) && (total_coverage) && (min_bound_z == min_z) && (max_bound_z > max_z)
              && (width_shifted_cuboid_x == 1) && (width_shifted_cuboid_y == 1)){
        expanded_cuboid(5,0) = max_bound_z;
        return true;
    }else if( ( search_index == 3 ) && (total_coverage) && (min_bound_x < min_x) && (max_bound_x == max_x)
              && (width_shifted_cuboid_y == 1) && (width_shifted_cuboid_z == 1)){
        expanded_cuboid(0,0) = min_bound_x;
        return true;
    }else if( ( search_index == 4 ) && (total_coverage) && (min_bound_y < min_y) && (max_bound_y == max_y)
              && (width_shifted_cuboid_x == 1) && (width_shifted_cuboid_z == 1)){
        expanded_cuboid(1,0) = min_bound_y;
        return true;
    }else if( ( search_index == 5 ) && (total_coverage) && (min_bound_z < min_z) && (max_bound_z == max_z)
              && (width_shifted_cuboid_x == 1) && (width_shifted_cuboid_y == 1)){
        expanded_cuboid(2,0) = min_bound_z;
        return true;
    }else{
        return false;
    }

}

bool grid_functions::check_coverage_and_expansion_generalized(grid_cells &returned_cells, cuboid_corners shifted_cuboid, cuboid_corners &expanded_cuboid, unsigned int search_index){

    int width_shifted_cuboid_x = shifted_cuboid(3,0) - shifted_cuboid(0,0);
    int width_shifted_cuboid_y = shifted_cuboid(4,0) - shifted_cuboid(1,0);
    int width_shifted_cuboid_z = shifted_cuboid(5,0) - shifted_cuboid(2,0);

    int min_x = shifted_cuboid(0,0);
    int min_y = shifted_cuboid(1,0);
    int min_z = shifted_cuboid(2,0);
    int max_x = shifted_cuboid(3,0);
    int max_y = shifted_cuboid(4,0);
    int max_z = shifted_cuboid(5,0);

    int total_volume = width_shifted_cuboid_x*width_shifted_cuboid_y*width_shifted_cuboid_z;
    bool coverage[total_volume];

    // coverage along width as false...
    std::fill_n(coverage,total_volume,false);

    std::vector<unsigned int> indexes_cuboids;

    int min_bound_x, min_bound_y, min_bound_z, max_bound_x, max_bound_y, max_bound_z;

    // check coverage and if any cuboid crosses the boundary. Additionally find min and max bound along all dimensions
    for(unsigned int k=0;k< returned_cells.size();k++){

        int temp_cuboid_min_x = bg::get<bg::min_corner, 0>(returned_cells[k].first);
        int temp_cuboid_min_y = bg::get<bg::min_corner, 1>(returned_cells[k].first);
        int temp_cuboid_min_z = bg::get<bg::min_corner, 2>(returned_cells[k].first);

        // the max corner of the rect cuboid
        int temp_cuboid_max_x = bg::get<bg::max_corner, 0>(returned_cells[k].first);
        int temp_cuboid_max_y = bg::get<bg::max_corner, 1>(returned_cells[k].first);
        int temp_cuboid_max_z = bg::get<bg::max_corner, 2>(returned_cells[k].first);

        if( ( search_index == 0 || search_index == 3 )  ){

            // check y and z limit
            if( (temp_cuboid_min_y < min_y || temp_cuboid_min_z < min_z || temp_cuboid_max_y > max_y) || temp_cuboid_max_z > max_z ){
                //cout << " Cuboid exceeds y and z dimension during expansion along x " <<endl;

                if( (search_index == 0) && (temp_cuboid_min_x == min_x) ){
                    return false;
                }

                if( (search_index == 3) && (temp_cuboid_max_x == max_x) ){
                    return false;
                }

            }


        }else if( ( search_index == 1 || search_index == 4 )  ){

            if( (temp_cuboid_min_x < min_x || temp_cuboid_min_z < min_z || temp_cuboid_max_x > max_x) || temp_cuboid_max_z > max_z ){
                //cout << " Cuboid exceeds x and z dimension during expansion along y " <<endl;

                if( (search_index == 1) && (temp_cuboid_min_y == min_y) ){
                    return false;
                }

                if( (search_index == 4) && (temp_cuboid_max_y == max_y) ){
                    return false;
                }

            }

        }else if( ( search_index == 2 || search_index == 5 )  ){

            if( (temp_cuboid_min_x < min_x || temp_cuboid_min_y < min_y || temp_cuboid_max_x > max_x) || temp_cuboid_max_y > max_y ){
                //cout << " Cuboid exceeds x and y dimension during expansion along z " <<endl;

                if( (search_index == 2) && (temp_cuboid_min_z == min_z) ){
                    return false;
                }

                if( (search_index == 5) && (temp_cuboid_max_z == max_z) ){
                    return false;
                }

            }

        }

        int min_band_x, min_band_y, min_band_z, max_band_x, max_band_y, max_band_z;

        // the band to be held true in case of x ....
        if(temp_cuboid_min_x <= min_x){
            // confirm if this is correct...
            min_band_x = 0;
        }else{
            min_band_x = temp_cuboid_min_x - min_x;
        }

        if(temp_cuboid_max_x >= max_x){
            max_band_x = max_x - min_x - 1;
        }else{
            max_band_x = temp_cuboid_max_x - min_x - 1;
        }

        // the band to be held true in case of y...
        if(temp_cuboid_min_y <= min_y){
            min_band_y = 0;
        }else{
            min_band_y = temp_cuboid_min_y - min_y;
        }

        if(temp_cuboid_max_y >= max_y){
            max_band_y = max_y - min_y - 1;
        }else{
            max_band_y = temp_cuboid_max_y - min_y - 1;
        }

        // the band to be held true along z...
        if(temp_cuboid_min_z <= min_z){
            min_band_z = 0;
        }else{
            min_band_z = temp_cuboid_min_z - min_z;
        }

        if(temp_cuboid_max_z >= max_z){
            max_band_z = max_z - min_z - 1;
        }else{
            max_band_z = temp_cuboid_max_z - min_z - 1;
        }

        std::vector<int> indexes_coverage;

        for(int index_x=min_band_x;index_x <= max_band_x;index_x++){
            for(int index_y=min_band_y; index_y <= max_band_y;index_y++){
                for(int index_z=min_band_z; index_z <= max_band_z;index_z++){

                    // convert to a one dimensional index
                    int index = index_x + index_y*(width_shifted_cuboid_x) + index_z*(width_shifted_cuboid_x*width_shifted_cuboid_y);

                    //push back index...
                    indexes_coverage.push_back(index);
                }
            }
        }

        for(int l = 0; l< indexes_coverage.size();l++){
            coverage[indexes_coverage[l]] = true;
        }

        if(indexes_coverage.size() > 0){
            indexes_cuboids.push_back(k);
        }

        if( k==0 ){
            min_bound_x = temp_cuboid_min_x;
            max_bound_x = temp_cuboid_max_x;
            min_bound_y = temp_cuboid_min_y;
            max_bound_y = temp_cuboid_max_y;
            min_bound_z = temp_cuboid_min_z;
            max_bound_z = temp_cuboid_max_z;
        }

        // find min bound along x....
        if( min_bound_x > temp_cuboid_min_x ){
            min_bound_x = temp_cuboid_min_x;
        }

        // find min bound along y....
        if( min_bound_y > temp_cuboid_min_y ){
            min_bound_y = temp_cuboid_min_y;
        }

        // find min bound along z....
        if( min_bound_z > temp_cuboid_min_z ){
            min_bound_z = temp_cuboid_min_z;
        }

        // find max bound along x....
        if( max_bound_x < temp_cuboid_max_x ){
            max_bound_x = temp_cuboid_max_x;
        }

        // find max bound along y....
        if( max_bound_y < temp_cuboid_max_y ){
            max_bound_y = temp_cuboid_max_y;
        }

        // find max bound along z....
        if( max_bound_z < temp_cuboid_max_z ){
            max_bound_z = temp_cuboid_max_z;
        }

    }

    // check total coverage
    bool total_coverage = true;
    for(unsigned int m = 0;m < total_volume;m++){
        total_coverage = (total_coverage && coverage[m]);
    }


    if(indexes_cuboids.size() == 0){
        // fusion not possible ...
        return false;
    }else{

        sort( indexes_cuboids.begin(), indexes_cuboids.end() );
        indexes_cuboids.erase( unique( indexes_cuboids.begin(), indexes_cuboids.end() ), indexes_cuboids.end() );

        int conn_min_bound_x, conn_min_bound_y, conn_min_bound_z, conn_max_bound_x, conn_max_bound_y, conn_max_bound_z;

        //cout << indexes_cuboids.size() <<endl;
        for(unsigned int p=0;p < indexes_cuboids.size();p++){

             // the max corner of the rect cuboid
            int temp_cuboid_min_x = bg::get<bg::min_corner, 0>(returned_cells[indexes_cuboids[p]].first);
            int temp_cuboid_min_y = bg::get<bg::min_corner, 1>(returned_cells[indexes_cuboids[p]].first);
            int temp_cuboid_min_z = bg::get<bg::min_corner, 2>(returned_cells[indexes_cuboids[p]].first);

             // the max corner of the rect cuboid
            int temp_cuboid_max_x = bg::get<bg::max_corner, 0>(returned_cells[indexes_cuboids[p]].first);
            int temp_cuboid_max_y = bg::get<bg::max_corner, 1>(returned_cells[indexes_cuboids[p]].first);
            int temp_cuboid_max_z = bg::get<bg::max_corner, 2>(returned_cells[indexes_cuboids[p]].first);

            if(p == 0){
                conn_min_bound_x = temp_cuboid_min_x;
                conn_min_bound_y = temp_cuboid_min_y;
                conn_min_bound_z = temp_cuboid_min_z;

                conn_max_bound_x = temp_cuboid_max_x;
                conn_max_bound_y = temp_cuboid_max_y;
                conn_max_bound_z = temp_cuboid_max_z;

            }

            if(temp_cuboid_min_x < conn_min_bound_x){
                conn_min_bound_x = temp_cuboid_min_x;
            }

            if(temp_cuboid_min_y < conn_min_bound_y){
                conn_min_bound_y = temp_cuboid_min_y;
            }

            if(temp_cuboid_min_z < conn_min_bound_z){
                conn_min_bound_z = temp_cuboid_min_z;
            }

            if(temp_cuboid_max_x > conn_max_bound_x){
                conn_max_bound_x = temp_cuboid_max_x;
            }

            if(temp_cuboid_max_y > conn_max_bound_y){
                conn_max_bound_y = temp_cuboid_max_y;
            }

            if(temp_cuboid_max_z > conn_max_bound_z){
                conn_max_bound_z = temp_cuboid_max_z;
            }

        }

        int width_connected_cuboid_x = conn_max_bound_x - conn_min_bound_x;
        int width_connected_cuboid_y = conn_max_bound_y - conn_min_bound_y;
        int width_connected_cuboid_z = conn_max_bound_z - conn_min_bound_z;


        int total_volume = width_connected_cuboid_x*width_connected_cuboid_y*width_connected_cuboid_z;
        bool connected_coverage[total_volume];

        // coverage along width as false...
        std::fill_n(connected_coverage,total_volume,false);

        int min_band_x, min_band_y, min_band_z, max_band_x, max_band_y, max_band_z;

        for(unsigned int p=0;p < indexes_cuboids.size();p++){

            // the max corner of the rect cuboid
           int temp_cuboid_min_x = bg::get<bg::min_corner, 0>(returned_cells[indexes_cuboids[p]].first);
           int temp_cuboid_min_y = bg::get<bg::min_corner, 1>(returned_cells[indexes_cuboids[p]].first);
           int temp_cuboid_min_z = bg::get<bg::min_corner, 2>(returned_cells[indexes_cuboids[p]].first);

            // the max corner of the rect cuboid
           int temp_cuboid_max_x = bg::get<bg::max_corner, 0>(returned_cells[indexes_cuboids[p]].first);
           int temp_cuboid_max_y = bg::get<bg::max_corner, 1>(returned_cells[indexes_cuboids[p]].first);
           int temp_cuboid_max_z = bg::get<bg::max_corner, 2>(returned_cells[indexes_cuboids[p]].first);

           // the band to be held true in case of x ....
            if(temp_cuboid_min_x <= conn_min_bound_x){
                // confirm if this is correct...
                min_band_x = 0;
            }else{
                min_band_x = temp_cuboid_min_x - conn_min_bound_x;
            }

            if(temp_cuboid_max_x >= conn_max_bound_x){
                max_band_x = conn_max_bound_x - conn_min_bound_x - 1;
            }else{
                max_band_x = temp_cuboid_max_x - conn_min_bound_x - 1;
            }

            // the band to be held true in case of y...
            if(temp_cuboid_min_y <= conn_min_bound_y){
                min_band_y = 0;
            }else{
                min_band_y = temp_cuboid_min_y - conn_min_bound_y;
            }

            if(temp_cuboid_max_y >= conn_max_bound_y){
                max_band_y = conn_max_bound_y - conn_min_bound_y - 1;
            }else{
                max_band_y = temp_cuboid_max_y - conn_min_bound_y - 1;
            }

            // the band to be held true along z...
            if(temp_cuboid_min_z <= conn_min_bound_z){
                min_band_z = 0;
            }else{
                min_band_z = temp_cuboid_min_z - conn_min_bound_z;
            }

            if(temp_cuboid_max_z >= conn_max_bound_z){
                max_band_z = conn_max_bound_z - conn_min_bound_z - 1;
            }else{
                max_band_z = temp_cuboid_max_z - conn_min_bound_z - 1;
            }

            std::vector<int> indexes_coverage;

            for(int index_x=min_band_x;index_x <= max_band_x;index_x++){
                for(int index_y=min_band_y; index_y <= max_band_y;index_y++){
                    for(int index_z=min_band_z; index_z <= max_band_z;index_z++){

                        // convert to a one dimensional index
                        int index = index_x + index_y*(width_connected_cuboid_x) + index_z*(width_connected_cuboid_x*width_connected_cuboid_y);

                        //push back index...
                        indexes_coverage.push_back(index);
                    }
                }
            }

            for(int l = 0; l< indexes_coverage.size();l++){
                connected_coverage[indexes_coverage[l]] = true;
            }


        }

        // check total coverage
        bool total_connected_coverage = true;
        for(unsigned int m = 0;m < total_volume;m++){
            total_connected_coverage = (total_connected_coverage && connected_coverage[m]);
        }

        if( (search_index == 0) || (search_index == 1) || (search_index == 2) ){

            if( (search_index == 0) && (width_connected_cuboid_y == width_shifted_cuboid_y) && (width_connected_cuboid_z == width_shifted_cuboid_z)){

                if( conn_min_bound_x == min_x && conn_max_bound_x == max_x && total_coverage && total_connected_coverage){ // bounds are the same...
                    return true;
                }else if( conn_min_bound_x == min_x && conn_max_bound_x > max_x && total_coverage && total_connected_coverage){ // bound is greater...
                    expanded_cuboid(3,0) = conn_max_bound_x;
                    return true;
                }else if(conn_min_bound_x == min_x && conn_max_bound_x < max_x && total_connected_coverage){
                    // remove cells which are not contained and then return true;
                    // temporarily

                    grid_cells temp_cells;
                    for(unsigned int p=0;p < indexes_cuboids.size();p++){
                       // the max corner of the rect cuboid
                       temp_cells.push_back(returned_cells[indexes_cuboids[p]]);
                    }

                    if(temp_cells.size() == returned_cells.size()){
                        expanded_cuboid(3,0) = conn_max_bound_x;
                        return true;
                    }else{
                        cout << " Scenario with conn_max_bound_x < max_x and search index = 0 " <<endl;
                        return false;
                    }

                }else{
                    /*

                    cout << " some scenario not accounted for " << endl;

                    cout << total_coverage << " " << total_connected_coverage <<endl;
                    cout << expanded_cuboid(0,0) << " " << expanded_cuboid(1,0) << " " << expanded_cuboid(2,0)
                         << " " << expanded_cuboid(3,0) << " " << expanded_cuboid(4,0) << " " << expanded_cuboid(5,0) << endl;

                    cout << shifted_cuboid(0,0) << " " << shifted_cuboid(1,0) << " " << shifted_cuboid(2,0)
                         << " " << shifted_cuboid(3,0) << " " << shifted_cuboid(4,0) << " " << shifted_cuboid(5,0) << endl;

                    cout << conn_min_bound_x << " " << conn_min_bound_y << " " << conn_min_bound_z
                         << " " << conn_max_bound_x << " " << conn_max_bound_y << " " << conn_max_bound_z << endl;


                    cout << temp_min_bound_x << " " << temp_min_bound_y << " " << temp_min_bound_z
                         << " " << temp_max_bound_x << " " << temp_max_bound_y << " " << temp_max_bound_z << endl;


                    cout << " The cuboids " <<endl;

                    //cout << indexes_cuboids.size() <<endl;
                    for(unsigned int p=0;p < indexes_cuboids.size();p++){

                         // the max corner of the rect cuboid
                        int temp_cuboid_min_x = bg::get<bg::min_corner, 0>(returned_cells[indexes_cuboids[p]].first);
                        int temp_cuboid_min_y = bg::get<bg::min_corner, 1>(returned_cells[indexes_cuboids[p]].first);
                        int temp_cuboid_min_z = bg::get<bg::min_corner, 2>(returned_cells[indexes_cuboids[p]].first);

                         // the max corner of the rect cuboid
                        int temp_cuboid_max_x = bg::get<bg::max_corner, 0>(returned_cells[indexes_cuboids[p]].first);
                        int temp_cuboid_max_y = bg::get<bg::max_corner, 1>(returned_cells[indexes_cuboids[p]].first);
                        int temp_cuboid_max_z = bg::get<bg::max_corner, 2>(returned_cells[indexes_cuboids[p]].first);


                        cout << temp_cuboid_min_x << " " << temp_cuboid_min_y << " " << temp_cuboid_min_z
                             << " " << temp_cuboid_max_x << " " << temp_cuboid_max_y << " " << temp_cuboid_max_z << endl;

                    }
                    */

                    return false;
                }

            }else if( (search_index == 1) && (width_connected_cuboid_x == width_shifted_cuboid_x) && (width_connected_cuboid_z == width_shifted_cuboid_z)){

                if( conn_min_bound_y == min_y && conn_max_bound_y == max_y && total_coverage && total_connected_coverage){ // bounds are the same...
                    return true;
                }else if( conn_min_bound_y == min_y && conn_max_bound_y > max_y && total_coverage && total_connected_coverage){ // bound is greater...
                    expanded_cuboid(4,0) = conn_max_bound_y;
                    return true;
                }else if(conn_min_bound_y == min_y && conn_max_bound_y < max_y && total_connected_coverage){
                    // remove cells which are not contained and then return true;
                    // temporarily

                    grid_cells temp_cells;
                    for(unsigned int p=0;p < indexes_cuboids.size();p++){
                       // the max corner of the rect cuboid
                       temp_cells.push_back(returned_cells[indexes_cuboids[p]]);
                    }

                    if(temp_cells.size() == returned_cells.size()){
                        expanded_cuboid(4,0) = conn_max_bound_y;
                        return true;
                    }else{
                        cout << " Scenario with conn_max_bound_y < max_y and search index = 1 " <<endl;
                        return false;
                    }

                    return false;
                }else{
                    return false;
                }

            }else if( (search_index == 2) && (width_connected_cuboid_x == width_shifted_cuboid_x) && (width_connected_cuboid_y == width_shifted_cuboid_y)){

                if( conn_min_bound_z == min_z && conn_max_bound_z == max_z && total_coverage && total_connected_coverage){ // bounds are the same...
                    return true;
                }else if( conn_min_bound_z == min_z && conn_max_bound_z > max_z && total_coverage && total_connected_coverage){ // bound is greater...
                    expanded_cuboid(5,0) = conn_max_bound_z;
                    return true;
                    //return false;
                }else if(conn_min_bound_z == min_z && conn_max_bound_z < max_z && total_connected_coverage){
                    // remove cells which are not contained and then return true;
                    // temporarily

                    grid_cells temp_cells;
                    for(unsigned int p=0;p < indexes_cuboids.size();p++){
                       // the max corner of the rect cuboid
                       temp_cells.push_back(returned_cells[indexes_cuboids[p]]);
                    }

                    if(temp_cells.size() == returned_cells.size()){
                        expanded_cuboid(5,0) = conn_max_bound_z;
                        return true;
                    }else{
                        cout << " Scenario with conn_max_bound_z < max_z and search index = 2 " <<endl;
                        return false;
                    }

                }else{
                    return false;
                }

            }else{
                // width is not the same....
                return false;
            }

        }else if( (search_index == 3) || (search_index == 4) || (search_index == 5) ){

            if( (search_index == 3) && (width_connected_cuboid_y == width_shifted_cuboid_y) && (width_connected_cuboid_z == width_shifted_cuboid_z)){

                if( conn_min_bound_x == min_x && conn_max_bound_x == max_x && total_coverage && total_connected_coverage){ // bounds are the same...
                    return true;
                }else if( conn_min_bound_x < min_x && conn_max_bound_x == max_x && total_coverage && total_connected_coverage){ // bound is greater...
                    expanded_cuboid(0,0) = conn_min_bound_x;
                    return true;
                }else if(conn_min_bound_x > min_x && conn_max_bound_x == max_x && total_connected_coverage){
                    // remove cells which are not contained and then return true;
                    // temporarily

                    grid_cells temp_cells;
                    for(unsigned int p=0;p < indexes_cuboids.size();p++){
                       // the max corner of the rect cuboid
                       temp_cells.push_back(returned_cells[indexes_cuboids[p]]);
                    }

                    if(temp_cells.size() == returned_cells.size()){
                        expanded_cuboid(0,0) = conn_min_bound_x;
                        return true;
                    }else{
                        cout << " Scenario with conn_min_bound_x > min_x and search index = 3 " <<endl;
                        return false;
                    }

                }else{
                    return false;
                }

            }else if( (search_index == 4) && (width_connected_cuboid_x == width_shifted_cuboid_x) && (width_connected_cuboid_z == width_shifted_cuboid_z)){

                if( conn_min_bound_y == min_y && conn_max_bound_y == max_y && total_coverage && total_connected_coverage){ // bounds are the same...
                    return true;
                }else if( conn_min_bound_y < min_y && conn_max_bound_y == max_y && total_coverage && total_connected_coverage ){ // bound is greater...
                    expanded_cuboid(1,0) = conn_min_bound_y;
                    return true;
                }else if( conn_min_bound_y > min_y && conn_max_bound_y == max_y && total_connected_coverage ){
                    // remove cells which are not contained and then return true;
                    // temporarily

                    grid_cells temp_cells;
                    for(unsigned int p=0;p < indexes_cuboids.size();p++){
                       // the max corner of the rect cuboid
                       temp_cells.push_back(returned_cells[indexes_cuboids[p]]);
                    }

                    if(temp_cells.size() == returned_cells.size()){
                        expanded_cuboid(1,0) = conn_min_bound_y;
                        return true;
                    }else{
                        cout << " conn_min_bound_y > min_y and search index = 4 " <<endl;
                        return false;
                    }

                }else{
                    return false;
                }

            }else if( (search_index == 5) && (width_connected_cuboid_x == width_shifted_cuboid_x) && (width_connected_cuboid_y == width_shifted_cuboid_y)){

                if( conn_min_bound_z == min_z && conn_max_bound_z == max_z && total_coverage && total_connected_coverage){ // bounds are the same...
                    return true;
                }else if( conn_min_bound_z < min_z && conn_max_bound_z == max_z && total_coverage && total_connected_coverage ){ // bound is greater...
                    expanded_cuboid(2,0) = conn_min_bound_z;
                    return true;
                    //return false;
                }else if( conn_min_bound_z > min_z && conn_max_bound_z == max_z && total_connected_coverage ){
                    // remove cells which are not contained and then return true;
                    // temporarily

                    grid_cells temp_cells;
                    for(unsigned int p=0;p < indexes_cuboids.size();p++){
                       // the max corner of the rect cuboid
                       temp_cells.push_back(returned_cells[indexes_cuboids[p]]);
                    }

                    if(temp_cells.size() == returned_cells.size()){
                        expanded_cuboid(2,0) = conn_min_bound_z;
                        return true;
                    }else{
                        cout << " conn_min_bound_z > min_z and search index = 5 " <<endl;
                        return false;
                    }

                }else{
                    return false;
                }

            }else{
                // width is not the same....
                return false;
            }


        }else{
            return false;
        }

        return false;


    }


}

rect_cuboid grid_functions::get_cuboid(point_xyz min_point, point_xyz max_point){

    double x = min_point.x_;
    double y = min_point.y_;
    double z = min_point.z_;

    // map to minimal corner of grid cell
    int x_grid_min = floor((x/myparams_->get_resolution()));
    int y_grid_min = floor((y/myparams_->get_resolution()));
    int z_grid_min = floor((z/myparams_->get_resolution()));

    //cout << x_grid_min << " " << y_grid_min << " " << z_grid_min <<endl;

    point min_point_rc(x_grid_min,y_grid_min,z_grid_min);

    // maximal corner of grid cell
    x = max_point.x_;
    y = max_point.y_;
    z = max_point.z_;

    // map to minimal corner of grid cell
    int x_grid_max = floor((x/myparams_->get_resolution()));
    int y_grid_max = floor((y/myparams_->get_resolution()));
    int z_grid_max = floor((z/myparams_->get_resolution()));

    //cout << x_grid_max << " " << y_grid_max << " " << z_grid_max <<endl;

    point max_point_rc(x_grid_max,y_grid_max,z_grid_max);

    // get the cuboid
    rect_cuboid cuboid(min_point_rc,max_point_rc);
    return cuboid;

}

// map the point to the grid and return the rectangular cuboid!!!
rect_cuboid grid_functions::map_to_grid(point_xyz mypoint){

    // get the point coordinates
    double x = mypoint.x_;
    double y = mypoint.y_;
    double z = mypoint.z_;

    //cout << x << " " << y << " " << z <<endl;

    // map to minimal corner of grid cell
    int x_grid_min = floor((x/myparams_->get_resolution()));
    int y_grid_min = floor((y/myparams_->get_resolution()));
    int z_grid_min = floor((z/myparams_->get_resolution()));

    //cout << x_grid_min << " " << y_grid_min << " " << z_grid_min <<endl;

    point min_point_rc(x_grid_min,y_grid_min,z_grid_min);

    // find maximal corner of grid cell
    int x_grid_max = x_grid_min + 1;
    int y_grid_max = y_grid_min + 1;
    int z_grid_max = z_grid_min + 1;

    point max_point_rc(x_grid_max,y_grid_max,z_grid_max);

    //cout << x_grid_max << " " << y_grid_max << " " << z_grid_max <<endl;

    // get the cuboid
    rect_cuboid cuboid(min_point_rc,max_point_rc);
    return cuboid;
}

// -- uses boost point type which is int -- map the point to the grid and return the rectangular cuboid!!!
rect_cuboid grid_functions::get_cuboid_from_min_pt(point min_point_rc){

    // find maximal corner of grid cell
    int x_grid_max = min_point_rc.get<0>() + 1;
    int y_grid_max = min_point_rc.get<1>() + 1;
    int z_grid_max = min_point_rc.get<2>() + 1;

    point max_point_rc(x_grid_max,y_grid_max,z_grid_max);

    //cout << min_point_rc.get<0>() << " " << min_point_rc.get<1>() << " " << min_point_rc.get<2>() <<endl;
    //cout << x_grid_max << " " << y_grid_max << " " << z_grid_max <<endl;

    // get the cuboid
    rect_cuboid cuboid(min_point_rc,max_point_rc);
    return cuboid;
}

rect_cuboids grid_functions::trace_ray(point_xyz in_point, point_xyz out_point){


    rect_cuboids traced_cuboids; // cuboids found during ray tracing

    ////////// Bresenham algorithm with integer arthmetic...
    // in other words its just a freaking line through a grid but super duper fast!!!....

   rect_cuboid in_cuboid = map_to_grid(in_point);
   rect_cuboid out_cuboid = map_to_grid(out_point);

   traced_cuboids.push_back(in_cuboid);

   int in_x = bg::get<bg::min_corner, 0>(in_cuboid);
   int in_y = bg::get<bg::min_corner, 1>(in_cuboid);
   int in_z = bg::get<bg::min_corner, 2>(in_cuboid);

   int out_x = bg::get<bg::min_corner, 0>(out_cuboid);
   int out_y = bg::get<bg::min_corner, 1>(out_cuboid);
   int out_z = bg::get<bg::min_corner, 2>(out_cuboid);

   if( (in_x == out_x) && (in_y == out_y) && (in_z == out_z) ){
       return traced_cuboids;
   }

   int d_x = out_x - in_x;
   int d_y = out_y - in_y;
   int d_z = out_z - in_z;

   int s_x, s_y, s_z;

   if ( d_x < 0 )
     s_x = -1;
   else
     s_x =  1;

   if ( d_y < 0 )
     s_y = -1;
   else
     s_y =  1;

   if ( d_z < 0 )
     s_z = -1;
   else
     s_z =  1;


   int abs_dx = abs(d_x);
   int abs_dy = abs(d_y);
   int abs_dz = abs(d_z);

   int abs_dx2 = abs_dx*2;
   int abs_dy2 = abs_dy*2;
   int abs_dz2 = abs_dz*2;

   // dominant along x...with a conditional incremental along y and z
   if ((abs_dx >= abs_dy) && ( abs_dx >= abs_dz) ){

      int err_1 = abs_dy2 - abs_dx;
      int err_2 = abs_dz2 - abs_dx;

      for(unsigned int i=0;i < abs_dx -1 ;i++){

         if ( err_1 > 0 ){
             in_y += s_y;
             err_1 -= abs_dx2;
         }

         if ( err_2 > 0 ){
             in_z += s_z;
             err_2 -= abs_dx2;
         }

         err_1 += abs_dy2;
         err_2 += abs_dz2;
         in_x += s_x;

         point temp_point(in_x,in_y,in_z);
         rect_cuboid temp_cuboid = get_cuboid_from_min_pt(temp_point);
         traced_cuboids.push_back(temp_cuboid);

      }

  }else if ( (abs_dy > abs_dx) && ( abs_dy >= abs_dz ) ){     // dominant along y...with a conditional incremental along x and z

     int err_1 = abs_dx2 - abs_dy;
     int err_2 = abs_dz2 - abs_dy;

     for(unsigned int i=0;i < abs_dy -1 ;i++){

       if( err_1 > 0 ){
            in_x += s_x;
            err_1 -= abs_dy2;
       }

       if ( err_2 > 0 ){
            in_z += s_z;
            err_2 -= abs_dy2;
       }

       err_1 += abs_dx2;
       err_2 += abs_dz2;
       in_y += s_y;

       point temp_point(in_x,in_y,in_z);
       rect_cuboid temp_cuboid = get_cuboid_from_min_pt(temp_point);
       traced_cuboids.push_back(temp_cuboid);
     }

   }else if ( ( abs_dz > abs_dx ) && ( abs_dz > abs_dy ) ){   // dominant along z...with a conditional incremental along x and y

     int err_1 = abs_dy2 - abs_dz;
     int err_2 = abs_dx2 - abs_dz;

     for(unsigned int i=0; i < abs_dz -1 ;i++){

         if ( err_1 > 0 ){
             in_y += s_y;
             err_1 -= abs_dz2;
         }

         if ( err_2 > 0 ){
             in_x += s_x;
             err_2 -= abs_dz2;
         }

         err_1 += abs_dy2;
         err_2 += abs_dx2;
         in_z += s_z;

         point temp_point(in_x,in_y,in_z);
         rect_cuboid temp_cuboid = get_cuboid_from_min_pt(temp_point);
         traced_cuboids.push_back(temp_cuboid);
      }

   }

   return traced_cuboids;

}

double grid_functions::evaluate_dirichlet(std::vector<double> prob, concentration_params params){

    //for(unsigned int j = 0;j < params.second.size();j++){
    //    cout << params.second[j] << " ";
   // }
   // cout << endl;

    int sum_alpha = 0;
    double sum_gamma = 0.0;
    std::vector<unsigned int> alpha;

    for(unsigned int j = 0;j < params.second.size();j++){
        alpha.push_back(params.second[j]);
        sum_alpha += params.second[j];
        int fault;
        double value = lngamma((double) params.second[j], &fault);
        sum_gamma += value;
    }

    //cout << sum_alpha << endl;
    //cout << sum_gamma <<endl;

    int fault;
    double mygamma = lngamma((double) sum_alpha, &fault);

    //cout << mygamma <<endl;

    double pdf_value = 1.0;
    for(unsigned int i=0;i < alpha.size();i++){
        pdf_value = pdf_value * pow(prob[i],alpha[i]-1);
    }

    //cout << " Normalization " << exp(mygamma - sum_gamma) << " product " << pdf_value <<endl;

    pdf_value = pdf_value*exp(mygamma - sum_gamma);
    //cout << " pdf value " << pdf_value <<endl;
    return pdf_value;
}

int grid_functions::assign_label(value cell, dirichlet_params params){

    std::vector<unsigned int> label_counts = cell.second->label_count_;
    std::vector<double> probability;
    double total_hits = (double) cell.second->num_hits_;

    for(unsigned int i=0;i< label_counts.size();i++){
        if(i == 5 || i == 8 || i == 10)
            continue;
        //cout << label_counts[i] << " ";
        double prob;
        prob = ( (double) label_counts[i])/total_hits;
        prob += 0.01;
        probability.push_back(prob);
    }

    //cout << endl;
    double sum = 0.0;
    sum = std::accumulate(probability.begin(),probability.end(),0.0);

    for(unsigned int i=0;i< probability.size();i++){
        probability[i] = probability[i]/sum;
        //cout << " " << probability[i] <<endl;
    }

    double max_pdf = 0.0;
    int label_index = -1;
    // iterate of all classes and then find the appriopriate assignment
    for(unsigned int i=0;i< params.size();i++){
        double temp = evaluate_dirichlet(probability,params[i]);

        if(isnan(temp)){
            continue;
        }else if(temp > max_pdf){
            max_pdf = temp;
            label_index = i;
        }
    }

    return label_index;

}

#endif

