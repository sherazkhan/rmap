// Boost.Geometry Index
//
// R-tree visitor collecting basic statistics
//
// Copyright (c) 2011-2013 Adam Wulkiewicz, Lodz, Poland.
// Copyright (c) 2013 Mateusz Loskot, London, UK.
//
// Use, modification and distribution is subject to the Boost Software License,
// Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef RMAP_STATISTICS
#define RMAP_STATISTICS

#include <algorithm>
#include <vector>

//#include <tuple>

struct rtree_stats{
    int num_levels;
    int num_nodes;
    int num_leaves;
    int num_values;
    int values_min;
    int values_max;
};

struct grid_cell{

    grid_cell(double log_odds, int r,int g, int b):log_odds_(log_odds),r_(r),g_(g),b_(b){
        num_hits_ = 0;
        num_misses_ = 0;
    }

    grid_cell(double log_odds, int r,int g, int b, unsigned int obj_id):log_odds_(log_odds),r_(r),g_(g),b_(b),object_id_(obj_id){
        num_hits_ = 0;
        num_misses_ = 0;
    }

    grid_cell(double log_odds, int r,int g, int b, unsigned int max_labels ,unsigned int obj_id):log_odds_(log_odds),r_(r),g_(g),b_(b),object_id_(obj_id){
        label_count_.assign(max_labels,0);
        num_hits_ = 0;
        num_misses_ = 0;
    }

    grid_cell():log_odds_(0.0),r_(0),g_(0),b_(0){
        r_ = 0;
        g_ = 0;
        b_ = 0;
        object_id_ = 0;
        num_hits_ = 0;
        num_misses_ = 0;
    }

    grid_cell(unsigned int max_labels):log_odds_(0.0),r_(0),g_(0),b_(0){
        label_count_.assign(max_labels,0);
        r_ = 0;
        g_ = 0;
        b_ = 0;
        object_id_ = 0;
        num_hits_ = 0;
        num_misses_ = 0;
    }

    double log_odds_;
    int r_;
    int g_;
    int b_;
    float intensity_;
    std::vector<unsigned int> label_count_;
    int label_;
    unsigned int object_id_;
    int num_hits_;
    int num_misses_;
};


namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;
namespace bdru = bgi::detail::rtree::utilities;

using namespace std;

typedef bg::model::point<double, 3, bg::cs::cartesian> float_point;
typedef bg::model::point<int, 3, bg::cs::cartesian> point;
typedef std::vector <point> points;
typedef bg::model::box<point> rect_cuboid;
typedef std::pair<rect_cuboid, grid_cell*> value;
typedef std::vector<rect_cuboid> rect_cuboids;
typedef std::vector<value> grid_cells;


namespace boost { namespace geometry { namespace index { namespace detail { namespace rtree { namespace utilities {

namespace visitors {

template <typename Value, typename Options, typename Box, typename Allocators>
struct statistics : public rtree::visitor<Value, typename Options::parameters_type, Box, Allocators, typename Options::node_tag, true>::type
{
    typedef typename rtree::internal_node<Value, typename Options::parameters_type, Box, Allocators, typename Options::node_tag>::type internal_node;
    typedef typename rtree::leaf<Value, typename Options::parameters_type, Box, Allocators, typename Options::node_tag>::type leaf;

    inline statistics()
        : level(0)
        , levels(1) // count root
        , nodes(0)
        , leaves(0)
        , values(0)
        , values_min(0)
        , values_max(0)
    {}

    inline void operator()(internal_node const& n)
    {
        typedef typename rtree::elements_type<internal_node>::type elements_type;
        elements_type const& elements = rtree::elements(n);

        ++nodes; // count node

        size_t const level_backup = level;
        ++level;

        levels += level++ > levels ? 1 : 0; // count level (root already counted)

        for (typename elements_type::const_iterator it = elements.begin();
            it != elements.end(); ++it)
        {
            rtree::apply_visitor(*this, *it->second);
        }

        level = level_backup;
    }

    inline void operator()(leaf const& n)
    {
        typedef typename rtree::elements_type<leaf>::type elements_type;
        elements_type const& elements = rtree::elements(n);

        ++leaves; // count leaves

        std::size_t const v = elements.size();
        // count values spread per node and total
        values_min = (std::min)(values_min == 0 ? v : values_min, v);
        values_max = (std::max)(values_max, v);
        values += v;

    }

    std::size_t level;
    std::size_t levels;
    std::size_t nodes;
    std::size_t leaves;
    std::size_t values;
    std::size_t values_min;
    std::size_t values_max;
};


template <typename Value, typename Options, typename Box, typename Allocators>
struct get_cells : public rtree::visitor<Value, typename Options::parameters_type, Box, Allocators, typename Options::node_tag, true>::type
{
    typedef typename rtree::internal_node<Value, typename Options::parameters_type, Box, Allocators, typename Options::node_tag>::type internal_node;
    typedef typename rtree::leaf<Value, typename Options::parameters_type, Box, Allocators, typename Options::node_tag>::type leaf;

    inline get_cells()
    {}

    inline void operator()(internal_node const& n)
    {
        typedef typename rtree::elements_type<internal_node>::type elements_type;
        elements_type const& elements = rtree::elements(n);

        for (typename elements_type::const_iterator it = elements.begin();
            it != elements.end(); ++it)
        {
            rtree::apply_visitor(*this, *it->second);
        }

    }

    inline void operator()(leaf const& n)
    {
        typedef typename rtree::elements_type<leaf>::type elements_type;
        elements_type const& elements = rtree::elements(n);

        // store all grid cells in the vector
        for (typename elements_type::const_iterator it = elements.begin();
            it != elements.end(); ++it)
        {
            occupancy_grid_cells.push_back(*(it));
        }
    }

    grid_cells occupancy_grid_cells;
};





} // namespace visitors

template <typename Rtree> inline
void
access_all_cells(Rtree const& tree, grid_cells &cells)
{

    typedef utilities::view<Rtree> RTV;

    RTV rtv(tree);

    visitors::get_cells<
        typename RTV::value_type,
        typename RTV::options_type,
        typename RTV::box_type,
        typename RTV::allocators_type
    > all_cells;

    rtv.apply_visitor(all_cells);

    cells = all_cells.occupancy_grid_cells;
}


template <typename Rtree> inline
rtree_stats
get_statistics(Rtree const& tree)
{

    typedef utilities::view<Rtree> RTV;

    RTV rtv(tree);

    rtree_stats mystats;

    visitors::statistics<
        typename RTV::value_type,
        typename RTV::options_type,
        typename RTV::box_type,
        typename RTV::allocators_type
    > stats_v;

    rtv.apply_visitor(stats_v);

    mystats.num_levels = (int)stats_v.levels;
    mystats.num_nodes = (int)stats_v.nodes;
    mystats.num_leaves = (int)stats_v.leaves;
    mystats.num_values = (int)stats_v.values;
    mystats.values_min = (int)stats_v.values_min;
    mystats.values_max = (int)stats_v.values_max;

    return mystats;

}

}}}}}} // namespace boost::geometry::index::detail::rtree::utilities



/* The original boost code !!!!
namespace boost { namespace geometry { namespace index { namespace detail { namespace rtree { namespace utilities {

namespace visitors {

template <typename Value, typename Options, typename Box, typename Allocators>
struct statistics : public rtree::visitor<Value, typename Options::parameters_type, Box, Allocators, typename Options::node_tag, true>::type
{
    typedef typename rtree::internal_node<Value, typename Options::parameters_type, Box, Allocators, typename Options::node_tag>::type internal_node;
    typedef typename rtree::leaf<Value, typename Options::parameters_type, Box, Allocators, typename Options::node_tag>::type leaf;

    inline statistics()
        : level(0)
        , levels(1) // count root
        , nodes(0)
        , leaves(0)
        , values(0)
        , values_min(0)
        , values_max(0)
    {}

    inline void operator()(internal_node const& n)
    {
        typedef typename rtree::elements_type<internal_node>::type elements_type;
        elements_type const& elements = rtree::elements(n);
        
        ++nodes; // count node

        size_t const level_backup = level;
        ++level;

        levels += level++ > levels ? 1 : 0; // count level (root already counted)
                
        for (typename elements_type::const_iterator it = elements.begin();
            it != elements.end(); ++it)
        {
            rtree::apply_visitor(*this, *it->second);
        }
        
        level = level_backup;
    }

    inline void operator()(leaf const& n)
    {   
        typedef typename rtree::elements_type<leaf>::type elements_type;
        elements_type const& elements = rtree::elements(n);

        ++leaves; // count leaves
        
        std::size_t const v = elements.size();
        // count values spread per node and total
        values_min = (std::min)(values_min == 0 ? v : values_min, v);
        values_max = (std::max)(values_max, v);
        values += v;
    }
    
    std::size_t level;
    std::size_t levels;
    std::size_t nodes;
    std::size_t leaves;
    std::size_t values;
    std::size_t values_min;
    std::size_t values_max;
};

} // namespace visitors

template <typename Rtree> inline
boost::tuple<std::size_t, std::size_t, std::size_t, std::size_t, std::size_t, std::size_t>
statistics(Rtree const& tree)
{
    typedef utilities::view<Rtree> RTV;
    RTV rtv(tree);

    visitors::statistics<
        typename RTV::value_type,
        typename RTV::options_type,
        typename RTV::box_type,
        typename RTV::allocators_type
    > stats_v;

    rtv.apply_visitor(stats_v);
    
    return boost::make_tuple(stats_v.levels, stats_v.nodes, stats_v.leaves, stats_v.values, stats_v.values_min, stats_v.values_max);
}

}}}}}} // namespace boost::geometry::index::detail::rtree::utilities

*/

#endif // RMAP_STATISTICS
