# README #

### What is this repository for? ###

This code is the implementation of the Semantic Rtree (SRtree) which augments a 3D occupancy grid with semantic labels in a probabilistic manner. The scientific details of this implementation can be found in

D. Wollherr, S. Khan, C. Landsiedel and M. Buss. The Interactive Urban Robot IURO:Towards Robot Action in Human Environments, International Symposium on Experimental Robotics (ISER), Springer, 2014.

The 3D occupancy grid is based on the Rtree data structure which allows the environment to be modeled using a hierarchy of axis aligned rectangular cuboids. The main advantage of the approach is that it is capable of modeling an adaptive resolution grid. In addition, the data structure generates the hierarchy in an incremental manner and allows the user to define the branching factor in advance which can be used to effectively reduce/increase the height and width of the tree thereby affecting insertion, access time as well as the number of bounding rectangular cuboids in the hierarchy.

Scientific details about the occupancy grid can be found in the following papers:

S. Khan, D. Wollherr, M. Buss , Adaptive rectangular cuboids for 3D mapping , IEEE International Conference on Robotics and Automation (ICRA), 2015.

and

S. Khan, C. Verginis, A. Dometios, C. Tzafestas, D. Wollherr, M. Buss , RMAP: a rectangular cuboid approximation framework for 3D environment mapping , Autonomous Robots , (2014) .


### How do I get set up? ###
The code depends on Boost (tested with version 1.54), PCL (version 1.7), OpenCV, ROS (tested with Groovy) as well as the Dirichlet implementation:

https://people.sc.fsu.edu/~jburkardt/cpp_src/asa266/asa266.html

The package used for estimating the Dirichlet parameters does not have a FindCmake, so its directory has been defined directly in the CMakeList.

Currently this code is geared towards a specific application i.e. downsampling the point clouds using the occupancy grid and assigning a semantic label to each grid cell. To get access to the dataset with which this code has been tested please email lsr@ei.tum.de. 

### Who do I talk to? ###
In case of any doubts please contact sheraz@lsr.ei.tum.de