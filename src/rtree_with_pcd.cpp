#include <stdio.h>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/index/rtree.hpp>
/* #include <boost/geometry/index/detail/rtree/utilities/statistics.hpp> */

// roservice includes
#include <rmap/cuboid.h>
#include <rmap/cuboids.h>
#include <rmap/read_scan.h>
#include <rmap/cuboid_service.h>

// opencv includes to read input images
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

//#include "E57Foundation.h"

// pcl includes incase rviz visualization is not sufficient
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/pcl_base.h>
#include <pcl/impl/pcl_base.hpp>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/point_representation.h>
#include <pcl/io/pcd_io.h>
#include <pcl/conversions.h>
#include <pcl/filters/filter.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/search/kdtree.h>
#include <pcl/search/impl/flann_search.hpp>
#include <pcl/search/impl/kdtree.hpp>
#include <pcl/features/normal_3d.h>
#include <pcl/features/feature.h>
#include <pcl/features/impl/feature.hpp>
/* #include <pcl/features/moment_of_inertia_estimation.h> */
#include <pcl/filters/voxel_grid.h>

// for dirichlet calculations
#include <asa266.cpp>

// includes for the occupancy grids
#include <allparams.h>
#include <input_data.h>
#include <occupancy_grid.h>

// ros includes for visualization!!!
#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <visualization_rviz.h>
#include <math.h>

// include for saving the full point cloud
#include "custom_point_types.h"

int scan_no = -1;
bool change_scan = false;

double timediff(const timeval &start, const timeval &stop) {
    return (stop.tv_sec - start.tv_sec) + 1.0e-6 * (stop.tv_usec - start.tv_usec);
}

boost::shared_ptr<pcl::visualization::PCLVisualizer> normalsVis(pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud,
                                                                pcl::PointCloud<pcl::Normal>::ConstPtr normals) {
    // --------------------------------------------------------
    // -----Open 3D viewer and add point cloud and normals-----
    // --------------------------------------------------------
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
    viewer->setBackgroundColor(0, 0, 0);
    // pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZ> rgb(cloud);
    viewer->addPointCloud<pcl::PointXYZ>(cloud, "sample cloud");
    viewer->addPointCloudNormals<pcl::PointXYZ, pcl::Normal>(cloud, normals, 10, 5, "normals");
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "sample cloud");
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 1.0, 0.0, 0.0, "normals");
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 5, "normals");
    viewer->addCoordinateSystem(1.0);
    viewer->initCameraParameters();
    return (viewer);
}

void get_scan_number(const rmap::read_scanConstPtr &scan) {
    cout << " Read scan number " << scan->scan_number << endl;

    if (scan_no == scan->scan_number) {
        cout << " The user entered the same scan number..so skipping " << endl;
    } else {
        scan_no = scan->scan_number;
        change_scan = true;
    }
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "rmap");
    ros::NodeHandle nh("rmap");
    ros::Rate loop_rate(2);

    timeval start;
    timeval stop;

    Color::Modifier green(Color::FG_GREEN);
    Color::Modifier red(Color::FG_RED);
    Color::Modifier def(Color::FG_DEFAULT);

    ros::Subscriber sub = nh.subscribe<rmap::read_scan>("scan_number", 10, get_scan_number);

    // initialize all classes
    params *myparams = new params();
    input_data *my_input_data = new input_data(myparams);
    grid_functions *mygrid_fns = new grid_functions(myparams);
    visualizer *myvisualizer = new visualizer(nh, myparams, my_input_data->semantic_dict, mygrid_fns);

    while (!change_scan) {
        cout << " Waiting for input scan " << endl;
        ros::spinOnce();
        loop_rate.sleep();
    }

    while (ros::ok()) {
        change_scan = false;
        // initialize occupancy grid
        rtree_occupancy_grid *myoccupancygrid = new rtree_occupancy_grid(myparams, mygrid_fns, 32);

        // ros service
        ros::ServiceServer service =
                nh.advertiseService("cuboid_service", &rtree_occupancy_grid::return_cuboids, myoccupancygrid);
        ros::Publisher pub = nh.advertise<rmap::cuboids>("Objects", 1000);

        scansirgblabel_with_pose labeled_scan;

        cout << " Reading input scan " << endl;
        bool data = my_input_data->get_pcd_pointcloud(scan_no, labeled_scan);

        if (labeled_scan.size() > 0) {
            cout << " Point cloud size " << labeled_scan[0].second.size() << endl;
        }
        vector<double> quat;
        quat.assign(7, 0.0);
        pcl::PointCloud<pcl::PointXYZ> entire_cloud, cloud_final;

        if (labeled_scan.size() > 0) {
            my_input_data->read_quaternion(scan_no, quat);
            // transform point cloud!!!
            for (unsigned int i = 0; i < labeled_scan[0].second.size(); i++) {
                pcl::PointXYZ mypoint;
                mypoint.x = labeled_scan[0].second[i].x_ - quat[0];
                mypoint.y = labeled_scan[0].second[i].y_ - quat[1];
                mypoint.z = labeled_scan[0].second[i].z_ - quat[2];

                entire_cloud.points.push_back(mypoint);
            }

            Eigen::Vector3f translation(0, 0, 0);
            Eigen::Quaternionf quaternion(quat[6], -quat[3], -quat[4], -quat[5]);

            cout << " Transforming scan " << endl;
            pcl::transformPointCloud(entire_cloud, cloud_final, translation, quaternion);
        }

        for (unsigned int i = 0; i < cloud_final.size(); i++) {
            labeled_scan[0].second[i].x_ = cloud_final[i].x;
            labeled_scan[0].second[i].y_ = cloud_final[i].y;
            labeled_scan[0].second[i].z_ = cloud_final[i].z;
        }

        cout << " Inserting observations into the Rtree grid " << endl;

        double total_time = 0.0;

        for (unsigned int i = 0; i < labeled_scan.size(); i++) {
            cout << " Robot position " << labeled_scan[i].first.x_ << " " << labeled_scan[i].first.y_ << " "
                 << labeled_scan[i].first.z_ << endl;
            int not_interesting = 0;
            for (unsigned int j = 0; j < labeled_scan[i].second.size(); j++) {
                point_xyz sensor_point(labeled_scan[i].first.x_, labeled_scan[i].first.y_, labeled_scan[i].first.z_);

                double range = pow(labeled_scan[i].second[j].x_, 2) + pow(labeled_scan[i].second[j].y_, 2) +
                               pow(labeled_scan[i].second[j].z_, 2);
                range = sqrt(range);

                if (range >= myparams->get_max_range() ||
                    range <= myparams->get_min_range()) {  // points that are outside/inside the max/min range declared
                                                           // for the grid
                    not_interesting++;
                    continue;
                }

                gettimeofday(&start, NULL);  // start timer
                myoccupancygrid->insert_observation(sensor_point, labeled_scan[i].second[j]);
                gettimeofday(&stop, NULL);  // stop timer
                double time_to_insert = (stop.tv_sec - start.tv_sec) + 1.0e-6 * (stop.tv_usec - start.tv_usec);
                total_time += time_to_insert;

                // status of point insertion
                if (j % myparams->get_status_update() == 0) {
                    cout << j << " points have been inserted into the grid " << endl;
                }
            }
            cout << " Scan number " << i + 1 << " inserted ... " << endl;
            cout << " Cases that are not interesting: " << not_interesting << endl;
        }

        cout << red << " Total time taken " << green << total_time << def << endl;
        cout << " Insertion complete... accessing all grid cells " << endl;

        rtree_stats occupancy_grid_stats = myoccupancygrid->get_rtree_stats();

        cout << " Inner nodes: " << occupancy_grid_stats.num_nodes << endl;
        cout << " Leaf nodes: " << occupancy_grid_stats.num_leaves << endl;
        cout << " Levels: " << occupancy_grid_stats.num_levels << endl;

        myvisualizer->remove_grid_cells();
        grid_cells all_cells;
        // access all cells
        gettimeofday(&start, NULL);  // start timer
        myoccupancygrid->access_all_gridcells(all_cells);
        gettimeofday(&stop, NULL);  // stop timer
        double time_to_access = (stop.tv_sec - start.tv_sec) + 1.0e-6 * (stop.tv_usec - start.tv_usec);
        cout << red << " Time taken to access all grid cells :  " << green << all_cells.size() << def << " is " << green
             << time_to_access << def << endl;

        pcl::PointCloud<pcl::PointXYZ>::Ptr total_cloud(new pcl::PointCloud<pcl::PointXYZ>);

        // filter cells which have less occupancy probabilities
        for (unsigned int i = 0; i < all_cells.size(); i++) {
            double centre_x, centre_y, centre_z;
            mygrid_fns->get_centroid(all_cells[i].first, centre_x, centre_y, centre_z);
            pcl::PointXYZ mypoint;
            mypoint.x = centre_x;
            mypoint.y = centre_y;
            mypoint.z = centre_z;

            total_cloud->points.push_back(mypoint);

            if (all_cells[i].second->log_odds_ < myparams->get_log_max_prob_occ_vis()) {
                continue;
            } else
                myoccupancygrid->all_cells_.push_back(all_cells[i]);
        }

        cout << " Number of grid cells after pruning " << myoccupancygrid->all_cells_.size() << endl;

        // build pcl xyz cloud for normal computation
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
        unsigned int max_object_id = 0;
        for (unsigned int i = 0; i < myoccupancygrid->all_cells_.size(); i++) {
            // assign labels to each grid cell
            dirichlet_params dist_params = myparams->get_dirichlet_params();
            // unsigned int label_index =
            // std::distance(cell.second->label_count_.begin(),std::max_element(cell.second->label_count_.begin(),cell.second->label_count_.end()));
            int dir_label = mygrid_fns->assign_label(myoccupancygrid->all_cells_[i], dist_params);

            // assign label here!!
            myoccupancygrid->all_cells_[i].second->label_ = dir_label;

            if (myoccupancygrid->all_cells_[i].second->object_id_ > max_object_id) {
                max_object_id = myoccupancygrid->all_cells_[i].second->object_id_;
            }

            double centre_x, centre_y, centre_z;
            mygrid_fns->get_centroid(myoccupancygrid->all_cells_[i].first, centre_x, centre_y, centre_z);
            pcl::PointXYZ mypoint;
            mypoint.x = centre_x * myparams->get_resolution();
            mypoint.y = centre_y * myparams->get_resolution();
            mypoint.z = centre_z * myparams->get_resolution();
            cloud->points.push_back(mypoint);
        }

        cout << " Max object id " << max_object_id << endl;

        myoccupancygrid->compute_normals(cloud);

        myoccupancygrid->label_based_.resize(myparams->get_max_no_labels());
        myoccupancygrid->object_based_.resize(max_object_id + 1);

        // sort cuboids by label and object id here
        for (unsigned int i = 0; i < myoccupancygrid->all_cells_.size(); i++) {
            int label = myoccupancygrid->all_cells_[i].second->label_;
            unsigned int object = myoccupancygrid->all_cells_[i].second->object_id_;

            if (label == -1) {
                continue;
            } else {
                cell_with_normal temp_cell =
                        make_pair(myoccupancygrid->all_cells_[i], myoccupancygrid->cloud_normals_[i]);
                myoccupancygrid->label_based_[label].push_back(temp_cell);
                myoccupancygrid->object_based_[object].push_back(temp_cell);
            }
        }

        // count the num of objects ---
        unsigned int num_objects = 0;
        for (unsigned int id = 0; id < max_object_id; id++) {
            // count the number of non zero objects!!!
            if (myoccupancygrid->object_based_[id].size() != 0) {
                cout << " Object id: " << id << " Num of points " << myoccupancygrid->object_based_[id].size() << endl;
                num_objects++;
            }
        }

        std::cout << " Total number of objects: " << num_objects << std::endl;
        cout << " Visualizing and publishing data " << endl;

        // build an extended point cloud with all the information from the cuboids, and save to a file.
        // TODO: include normals
        pcl::PointCloud<PointXYZIRGBLabel> save_cloud;
        for (unsigned int i = 0; i < myoccupancygrid->all_cells_.size(); i++) {
            double centre_x, centre_y, centre_z;
            mygrid_fns->get_centroid(myoccupancygrid->all_cells_[i].first, centre_x, centre_y, centre_z);
            int label = myoccupancygrid->all_cells_[i].second->label_;
            unsigned int object = myoccupancygrid->all_cells_[i].second->object_id_;

            PointXYZIRGBLabel p;
            p.x = centre_x * myparams->get_resolution();
            p.y = centre_y * myparams->get_resolution();
            p.z = centre_z * myparams->get_resolution();
            p.class_label = label;
            p.scan_idx = scan_no;
            p.obj_id = object;
            p.r = myoccupancygrid->all_cells_[i].second->r_;
            p.g = myoccupancygrid->all_cells_[i].second->g_;
            p.b = myoccupancygrid->all_cells_[i].second->b_;
            p.intensity = myoccupancygrid->all_cells_[i].second->intensity_;

            save_cloud.points.push_back(p);
        }

        std::stringstream ss;
        ss << "cuboids_irgblabel_" << scan_no << ".pcd";
        pcl::io::savePCDFile(ss.str(), save_cloud, true);
        // visualization
        myvisualizer->add_grid_cells(myoccupancygrid->all_cells_);

        while (!change_scan) {
            static tf::TransformBroadcaster br;
            tf::Transform transform;
            transform.setOrigin(tf::Vector3(0, 0, 0));
            transform.setRotation(tf::Quaternion(0, 0, 0));
            br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "odom", "base_link"));

            for (unsigned int id = 0; id < max_object_id; id++) {
                rmap::cuboids pub_cuboid;
                pub_cuboid.image_number = scan_no;
                pub_cuboid.max_num_objects = num_objects;

                for (unsigned int i = 0; i < myoccupancygrid->object_based_[id].size(); i++) {
                    // fill message
                    rmap::cuboid temp_cuboid;
                    double centre_x, centre_y, centre_z;
                    mygrid_fns->get_centroid(myoccupancygrid->object_based_[id][i].first.first, centre_x, centre_y,
                                             centre_z);

                    string object_label =
                            myparams->get_label(myoccupancygrid->object_based_[id][i].first.second->label_);
                    unsigned int object = myoccupancygrid->object_based_[id][i].first.second->object_id_;

                    temp_cuboid.label = object_label;
                    temp_cuboid.object_id = object;

                    temp_cuboid.cuboid.header.frame_id = myparams->frame_of_reference_;
                    temp_cuboid.cuboid.point.x = centre_x * myparams->get_resolution();
                    temp_cuboid.cuboid.point.y = centre_y * myparams->get_resolution();
                    temp_cuboid.cuboid.point.z = centre_z * myparams->get_resolution();

                    temp_cuboid.normal.x = myoccupancygrid->object_based_[id][i].second.normal_x;
                    temp_cuboid.normal.y = myoccupancygrid->object_based_[id][i].second.normal_y;
                    temp_cuboid.normal.z = myoccupancygrid->object_based_[id][i].second.normal_z;

                    pub_cuboid.object_id = object;

                    temp_cuboid.red = myoccupancygrid->object_based_[id][i].first.second->r_;
                    temp_cuboid.green = myoccupancygrid->object_based_[id][i].first.second->g_;
                    temp_cuboid.blue = myoccupancygrid->object_based_[id][i].first.second->b_;

                    pub_cuboid.cuboids.push_back(temp_cuboid);
                }

                if (pub_cuboid.cuboids.size() > 0) {
                    pub.publish(pub_cuboid);
                }
            }

            // tf::Transform transform_scene;
            // transform_scene.setOrigin( tf::Vector3(quat[0],quat[1],quat[2]) );
            // transform_scene.setRotation( tf::Quaternion(quat[3], quat[4], quat[5],quat[6]) );
            // br.sendTransform(tf::StampedTransform(transform_scene, ros::Time::now(), "base_link",
            // myparams->frame_of_reference_));

            // tf::Transform transform_scene;
            // transform_scene.setOrigin( tf::Vector3(0,0,0) );
            // transform_scene.setRotation( tf::Quaternion(final_rotation.x(), final_rotation.y(),
            // final_rotation.z(),final_rotation.w()) );
            // br.sendTransform(tf::StampedTransform(transform_scene, ros::Time::now(), "odom",
            // myparams->frame_of_reference_));

            myvisualizer->visualize_cubes();

            ros::spinOnce();
            // loop_rate.sleep();
        }
        delete myoccupancygrid;
    }
    return 0;
}

/*
// sample a small num of points from the cloud
unsigned int num_points = myparams->sample_pts_;
srand (time(0));
for(unsigned int j=0;j< num_points;j++){

    unsigned int index = rand() % entire_cloud.size();

    pcl::PointXYZ mypoint;
    mypoint.x = entire_cloud[index].x;
    mypoint.y = entire_cloud[index].y;
    mypoint.z = entire_cloud[index].z;

    temp_cloud.points.push_back(mypoint);

}
*/

/*

pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_ptr;
cloud_ptr.reset(new pcl::PointCloud<pcl::PointXYZ>(temp_cloud));

pcl::MomentOfInertiaEstimation <pcl::PointXYZ> feature_extractor;

feature_extractor.setInputCloud (cloud_ptr);
feature_extractor.compute ();

std::vector <float> moment_of_inertia;
std::vector <float> eccentricity;
pcl::PointXYZ min_point_AABB;
pcl::PointXYZ max_point_AABB;
pcl::PointXYZ min_point_OBB;
pcl::PointXYZ max_point_OBB;
pcl::PointXYZ position_OBB;
Eigen::Matrix3f rotational_matrix_OBB;
float major_value, middle_value, minor_value;
Eigen::Vector3f major_vector, middle_vector, minor_vector;
Eigen::Vector3f mass_center;

feature_extractor.getMomentOfInertia (moment_of_inertia);
feature_extractor.getEccentricity (eccentricity);
feature_extractor.getAABB (min_point_AABB, max_point_AABB);
feature_extractor.getOBB (min_point_OBB, max_point_OBB, position_OBB, rotational_matrix_OBB);
feature_extractor.getEigenValues (major_value, middle_value, minor_value);
feature_extractor.getEigenVectors (major_vector, middle_vector, minor_vector);
feature_extractor.getMassCenter (mass_center);

Eigen::Vector3f translation(0.0,0.0,0.0);

Eigen::Quaternionf quat (rotational_matrix_OBB);

  */

// tf::Quaternion q(quat.x(), quat.y(), quat.z(), quat.w());
// double roll = 0, pitch = 0, yaw = 0;
// tf::Matrix3x3(q).getRPY(roll, pitch, yaw);

// cout << " Roll -- Pitch -- Yaw : " << roll << " " << " " << pitch << " " << yaw << endl;

// roll = 0;
/// yaw = 0;

// tf::Matrix3x3 newmat;
// newmat.setRPY(roll, pitch, yaw);

// tf::Quaternion final_rotation;
// newmat.getRotation(final_rotation);

// cout << quat.x() << " " << quat.y() << " " << quat.z() << " " << quat.w() <<endl;
// cout << final_rotation.getX() << " " << final_rotation.getY() << " " << final_rotation.getZ() << " " <<
// final_rotation.getW() <<endl;
