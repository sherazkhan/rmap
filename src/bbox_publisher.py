import os, sys
sys.path.append('/usr/lib/pymodules/python2.7/')
sys.path.append('/usr/lib/python2.7/dist-packages/')
sys.path.append('/home/chris/lsrhome/pcl_stuff/exp/')
sys.path.append('/opt/ros/groovy/lib/python2.7/dist-packages')
import numpy as np

import roslib
roslib.load_manifest('isertree')
import rospy
import tf
import time

# messages
from isertree.srv import cuboid_service, cuboid_serviceRequest
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Quaternion, Point, Vector3, PointStamped
from std_msgs.msg import ColorRGBA, Int8
from visualization_msgs.msg import Marker, MarkerArray
from isertree.msg import cuboid, cuboids
from isertree.msg import read_scan


from features import compute_bbox, calc_geometry_features, get_obj_centroids


def cuboid_client(**kwargs):
    connected = False
    while not connected:
        try:
            rospy.wait_for_service('rmap/cuboid_service', 5)
            connected = True
        except rospy.ROSException:
            print('could not connect, retrying')

    req = cuboid_serviceRequest()
    for k, v in kwargs.items():
        setattr(req, k, v)
    try:
        req_cuboids = rospy.ServiceProxy('rmap/cuboid_service', cuboid_service, )
        resp1 = req_cuboids(req)
        return resp1
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e


def check_img_ix(img_ix):
    from annotate_paths import get_final_objectimg_path
    while not os.path.exists(get_final_objectimg_path(img_ix)):
        img_ix += 1

    return img_ix


def compute_all_features(objects, bboxes):
    import itertools
    allfeatures = []
    for (obj1_ix, obj1), (obj2_ix, obj2) in itertools.product(objects.items(), repeat=2):
        if obj1_ix == obj2_ix:
            continue
        # print 'features for', obj1_ix, obj2_ix
        cuboid_centroids1 = get_obj_centroids(obj1)
        cuboid_centroids2 = get_obj_centroids(obj2)
        allfeatures.append(np.concatenate(([obj1_ix, obj2_ix], calc_geometry_features(cuboid_centroids1, cuboid_centroids2, obj1_ix, obj2_ix, bboxes))))

    return np.vstack(allfeatures)


def obj_cb(ccuboids):
    global waiting, img_ix, objects
    if not ccuboids or not ccuboids.cuboids:
        return

    object_id = ccuboids.object_id
    if ccuboids.image_number == img_ix and waiting:
        objects[object_id] = ccuboids
        print img_ix, ccuboids.max_num_objects, len(objects), max(objects.keys()), waiting

    if len(objects) == ccuboids.max_num_objects:
        bboxes = {}
        # calculate all bounding boxes
        for i, obj in objects.items():
            bbox_edges, bbox_center, bbox_dims, all_edges, tf_matrix = compute_bbox(obj, i)

            inv_tf_matrix = tf.transformations.inverse_matrix(tf_matrix)

            tf_bbox_edges = np.dot(inv_tf_matrix, np.hstack((bbox_edges, np.ones((len(bbox_edges), 1)))).T).T[:, :-1]
            tf_all_edges = np.dot(inv_tf_matrix, np.hstack((all_edges, np.ones((len(all_edges), 1)))).T).T[:, :-1]
            tf_bbox_center = np.dot(inv_tf_matrix, np.hstack((bbox_center, 1)).T).T[:-1]

            bboxes[i] = tf_bbox_edges, tf_bbox_center, tf_all_edges

        new_img_ix = check_img_ix(img_ix + 1)
        img_ix_pub.publish(new_img_ix)
        waiting = False
        features = compute_all_features(objects, bboxes)

        np.save('features_{}'.format(img_ix), features)
        np.save('objects_{}'.format(img_ix), objects)
        print('done computing features for image {} at {}'.format(img_ix, time.strftime("%H:%M:%S")))
        img_ix = new_img_ix
        rospy.sleep(5)
        waiting = True
        objects = {}


objects = {}
img_ix = check_img_ix(44)
waiting = True

img_ix_pub = None


def main():
    global img_ix_pub, br, listener
    rospy.init_node('bboxes', anonymous=True)
    rospy.init_node('bboxes', anonymous=True)

    img_ix_pub = rospy.Publisher('/iser_tree/scan_number', read_scan)
    rospy.sleep(1)
    img_ix_pub.publish(img_ix)
    rospy.Subscriber('/iser_tree/Objects', cuboids, obj_cb, queue_size=10)

    while not rospy.is_shutdown() and img_ix < 81:
        rospy.sleep(1)


if __name__ == '__main__':
    main()
