import numpy as np
import sys
sys.path.append('/home/chris/lsrhome/pcl_stuff/exp/')
sys.path.append('/home/landsiedel/pcl_stuff/exp/')


from sklearn import svm
from sklearn import preprocessing
from sklearn.cross_validation import train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report, precision_recall_fscore_support
from sklearn.externals import joblib

from annotate_paths import get_support_labels_path

PRED_NAMES = dict(zip('obl', 'On Behind LeftOf'.split()))


def get_supp_labels(img_ix):
    return np.load(get_support_labels_path(img_ix)).item()


def get_gt(sprel, i, j, rel_type):
    return i in sprel[rel_type] and j in sprel[rel_type][i]


def get_feat_labels(img_ix):
    sprel = get_supp_labels(img_ix)
    all_features = np.load('features_{}.npy'.format(img_ix))

    all_objects = set()
    for rel_dict in sprel.values():
        all_objects.update(set(rel_dict.keys()))
        map(all_objects.update, rel_dict.values())
    all_object_ids = np.array(list(all_objects))

    valid_features_mask = np.logical_and(np.in1d(all_features[:, 0], all_object_ids),
                                         np.in1d(all_features[:, 1], all_object_ids))
    valid_features = all_features[valid_features_mask]
    # print all_object_ids.shape, all_features.shape, valid_features.shape

    labels = np.ndarray((valid_features.shape[0], 3), dtype=np.bool)
    # print labels.shape

    for ix, (i, j) in enumerate(valid_features[:, :2]):
        labels[ix, 0] = get_gt(sprel, i, j, 'o')
        labels[ix, 1] = get_gt(sprel, i, j, 'b')
        labels[ix, 2] = get_gt(sprel, i, j, 'l')

    return np.hstack((np.ones((len(valid_features), 1)) * img_ix, valid_features[:, :2])), valid_features[:, 2:], labels


def get_all_feat_labels(images):
    all_relix, all_features, all_labels = [], [], []
    for img_ix in images:
        relix, features, labels = get_feat_labels(img_ix)
        # print(img_ix, relix.shape, features.shape, labels.shape)
        all_relix.append(relix)
        all_features.append(features)
        all_labels.append(labels)

        # print(img_ix, np.where(np.isnan(features)))

    all_relix = np.vstack(all_relix).astype(np.int)
    all_features = np.vstack(all_features)
    all_features[np.where(np.isnan(all_features))] = 0
    all_labels = np.vstack(all_labels).astype(np.bool)

    return all_relix, all_features, all_labels


def prepare(all_features, labels, clf_type='o'):
    # preprocess data
    scaler = preprocessing.StandardScaler().fit(all_features)
    feat_scaled = scaler.transform(all_features)

    label_selector = 'obl'.index(clf_type)

    X_train, X_test, y_train, y_test = train_test_split(
        feat_scaled, labels[:, label_selector], test_size=0.5, random_state=0, )

    # Set the parameters by cross-validation

    tuned_parameters = [  # {'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
        #'C': [1, 10, 100, 1000]},
        {'C': [.1, 1, 10, 100, 1000, 10000]}]

    scoring_measures = ['recall']

    return scaler, scoring_measures, X_train, X_test, y_train, y_test, tuned_parameters


def train_clf(scoring_measures, X_train, X_test, y_train, y_test, tuned_parameters, one_w, svmtype=svm.LinearSVC):
    """build direct support classifier"""
    clf = dict()
    for measure in scoring_measures:
        clf_ = GridSearchCV(svmtype(C=1, class_weight={0: 1, 1: one_w}), tuned_parameters, cv=5, scoring=measure, n_jobs=-1)
        clf_.fit(X_train, y_train)

#         for params, mean_score, scores in clf_.grid_scores_:
#             print("%0.3f (+/-%0.03f) for %r"
#                   % (mean_score, scores.std() / 2, params))
#         print()

        print("Detailed classification report:")
        y_true, y_pred = y_test, clf_.predict(X_test)
        print(classification_report(y_true, y_pred))
        clf[measure] = clf_

    return clf


def parse_mln_result(line, predicate_name):
    """
    this can only do supports.

    :todo: Transitive.
    """
    import re
    supporter = int(re.findall(predicate_name + "\(\'Region_(\d+)", line)[0])
    supported = int(re.findall(",\s+\'Region_(\d+)", line)[0])
    image_no = int(re.findall("Img_(\d+)", line)[0])
    truth_val = eval(re.findall("(True|False)", line)[0])

    assert truth_val
    return image_no, supported, supporter


def get_mln_pred(results_dir):
    """
    read mln (BP) prediction and calculate metrics from it.
    prints a classification report.

    :returns: y_true, mln_res, precision_recall_fscore_support, metrics_as_a_dict

    :todo: transitive support evaluation
    """
    import glob
    import os
    from collections import defaultdict
    print('looking for results in {}'.format(results_dir.rstrip(os.path.sep) + os.path.sep + '*.bp_solution'))
    mln_res_filenames = glob.glob(results_dir.rstrip(os.path.sep) + os.path.sep + '*.bp_solution')

    parse_res = defaultdict(list)
    for filename in mln_res_filenames:
        for line in open(filename, 'r'):
            try:
                parse_res['o'].append(parse_mln_result(line, "On"))
            except IndexError:
                pass
            try:
                parse_res['b'].append(parse_mln_result(line, "Behind"))
            except IndexError:
                pass
            try:
                parse_res['l'].append(parse_mln_result(line, "LeftOf"))
            except IndexError:
                pass

    images = np.unique([x[0] for v in parse_res.values() for x in v])

    all_relix, all_features, all_labels = get_all_feat_labels(images)

    all_nos = all_relix[:, 0]
    all_regions = all_relix[:, 1:]
    mln_res = np.empty((all_nos.shape[0], 3), dtype=bool)
    mln_res[:] = False

    for rel_type in 'obl':
        for img_ix, supported, supporter in parse_res[rel_type]:
            ix = np.where((all_nos == img_ix) & np.all(all_regions == [supported, supporter], axis=1))
            assert len(ix) == 1
            mln_res[ix, 'obl'.index(rel_type)] = True

        y_true = all_labels[:, 'obl'.index(rel_type)]  # for supports, transitive is XXX
        metric = precision_recall_fscore_support(y_true, mln_res[:, 'obl'.index(rel_type)])

        print('*** results for relation type {} ***'.format(PRED_NAMES[rel_type]))
        print(classification_report(y_true, mln_res[:, 'obl'.index(rel_type)]))
    return y_true, mln_res, metric
