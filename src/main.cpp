#include<stdio.h>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/index/rtree.hpp>
#include <boost/geometry/index/detail/rtree/utilities/statistics.hpp>

// roservice includes
#include <isertree/cuboid.h>
#include <isertree/cuboid_service.h>

// opencv includes to read input images
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "E57Foundation.h"

// pcl includes incase rviz visualization is not sufficient
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/pcl_base.h>
#include <pcl/impl/pcl_base.hpp>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/point_representation.h>
#include <pcl/io/pcd_io.h>
#include <pcl/conversions.h>
#include <pcl/filters/filter.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/search/kdtree.h>
#include <pcl/search/impl/flann_search.hpp>
#include <pcl/search/impl/kdtree.hpp>
#include <pcl/features/normal_3d.h>
#include <pcl/features/feature.h>
#include <pcl/features/impl/feature.hpp>
#include <pcl/features/moment_of_inertia_estimation.h>


// for dirichlet calculations
#include <asa266.cpp>

// includes for the occupancy grids
#include <allparams.h>
#include <input_data.h>
#include <occupancy_grid.h>

// ros includes for visualization!!!
#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <visualization_rviz.h>


double timediff(const timeval& start, const timeval& stop){
  return (stop.tv_sec - start.tv_sec) + 1.0e-6 *(stop.tv_usec - start.tv_usec);
}


boost::shared_ptr<pcl::visualization::PCLVisualizer> normalsVis (
    pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud, pcl::PointCloud<pcl::Normal>::ConstPtr normals)
{
  // --------------------------------------------------------
  // -----Open 3D viewer and add point cloud and normals-----
  // --------------------------------------------------------
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
  viewer->setBackgroundColor (0, 0, 0);
  //pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZ> rgb(cloud);
  viewer->addPointCloud<pcl::PointXYZ> (cloud, "sample cloud");
  viewer->addPointCloudNormals<pcl::PointXYZ, pcl::Normal> (cloud, normals, 10, 5, "normals");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "sample cloud");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 1.0, 0.0, 0.0, "normals");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 5, "normals");
  viewer->addCoordinateSystem (1.0, "global");
  viewer->initCameraParameters ();
  return (viewer);
}


int main(int argc, char** argv){

    ros::init(argc, argv, "iser_tree");
    ros::NodeHandle nh("iser_tree");
    ros::Rate loop_rate(0.5);

    timeval start;
    timeval stop;

    Color::Modifier green(Color::FG_GREEN);
    Color::Modifier red(Color::FG_RED);
    Color::Modifier def(Color::FG_DEFAULT);

    //initialize all classes
    params *myparams = new params();
    input_data *my_input_data = new input_data(myparams);
    grid_functions *mygrid_fns = new grid_functions(myparams);
    rtree_occupancy_grid *myoccupancygrid = new rtree_occupancy_grid(myparams,mygrid_fns,16);
    visualizer myvisualizer(nh,myparams,my_input_data->semantic_dict,mygrid_fns);

    // ros service
    ros::ServiceServer service = nh.advertiseService("cuboid_service", &rtree_occupancy_grid::cubes, myoccupancygrid);

    scansirgblabel_with_pose labeled_scan;

    int scan_no = 3;
    my_input_data->get_pointcloud(scan_no,labeled_scan);

    cout << " Inserting observations into the Rtree grid " << endl;

    double total_time = 0.0;

    for(unsigned int i=0;i< labeled_scan.size() ;i++){
        cout << " Robot position " << labeled_scan[i].first.x_ << " " << labeled_scan[i].first.y_ << " " << labeled_scan[i].first.z_ <<endl;
        int not_interesting = 0;
        for(unsigned int j=0;j< labeled_scan[i].second.size(); j++){
            point_xyz sensor_point(labeled_scan[i].first.x_,labeled_scan[i].first.y_,labeled_scan[i].first.z_);

            double range = pow(labeled_scan[i].second[j].x_,2) + pow(labeled_scan[i].second[j].y_,2) + pow(labeled_scan[i].second[j].z_,2);
            range = sqrt(range);

            if(range >= myparams->get_max_range() || range <= myparams->get_min_range()){ // points that are outside/inside the max/min range declared for the grid
                not_interesting++;
                continue;
            }

            gettimeofday(&start, NULL);  // start timer
            myoccupancygrid->insert_observation(sensor_point,labeled_scan[i].second[j]);
            gettimeofday(&stop, NULL);  // stop timer
            double time_to_insert = (stop.tv_sec - start.tv_sec) + 1.0e-6 *(stop.tv_usec - start.tv_usec);
            total_time += time_to_insert;

            // status of point insertion
            if( j% myparams->get_status_update() == 0){
                cout << j <<" points have been inserted into the grid " <<endl;
            }
        }
        cout << " Scan number " << i+1 << " inserted ... " <<endl;
        cout << " Cases that are not interesting: " << not_interesting <<endl;
    }

    cout << red << " Total time taken " << green << total_time << def << endl;
    cout << " Insertion complete... accessing all grid cells " << endl;

    rtree_stats occupancy_grid_stats = myoccupancygrid->get_rtree_stats();

    cout << " Inner nodes: " << occupancy_grid_stats.num_nodes <<endl;
    cout << " Leaf nodes: " << occupancy_grid_stats.num_leaves <<endl;
    cout << " Levels: " << occupancy_grid_stats.num_levels <<endl;

    myvisualizer.remove_grid_cells();
    grid_cells all_cells;
    // access all cells
    gettimeofday(&start, NULL);  // start timer
    myoccupancygrid->access_all_gridcells(all_cells);
    gettimeofday(&stop, NULL);  // stop timer
    double time_to_access = (stop.tv_sec - start.tv_sec) + 1.0e-6 *(stop.tv_usec - start.tv_usec);
    cout << red << " Time taken to access all grid cells :  " << green << all_cells.size() << def << " is " << green << time_to_access << def <<endl;

    pcl::PointCloud<pcl::PointXYZ>::Ptr total_cloud (new pcl::PointCloud<pcl::PointXYZ>);

    // filter cells which have less occupancy probabilities
    for(unsigned int i=0;i < all_cells.size();i++){

        double centre_x, centre_y, centre_z;
        mygrid_fns->get_centroid(all_cells[i].first,centre_x,centre_y,centre_z);
        pcl::PointXYZ mypoint;
        mypoint.x = centre_x;
        mypoint.y = centre_y;
        mypoint.z = centre_z;

        total_cloud->points.push_back(mypoint);

        if(all_cells[i].second->log_odds_ < myparams->get_log_max_prob_occ_vis()){
            continue;
        }else
            myoccupancygrid->all_cells_.push_back(all_cells[i]);

    }

    cout << " Number of grid cells after pruning " << myoccupancygrid->all_cells_.size() <<endl;

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);

    for(unsigned int i=0; i< myoccupancygrid->all_cells_.size();i++){
        // assign labels to each grid cell
        dirichlet_params dist_params = myparams->get_dirichlet_params();
        //unsigned int label_index = std::distance(cell.second->label_count_.begin(),std::max_element(cell.second->label_count_.begin(),cell.second->label_count_.end()));
        int dir_label = mygrid_fns->assign_label(myoccupancygrid->all_cells_[i],dist_params);

        // assign label here!!
        myoccupancygrid->all_cells_[i].second->label_ = dir_label;

        double centre_x, centre_y, centre_z;
        mygrid_fns->get_centroid(myoccupancygrid->all_cells_[i].first,centre_x,centre_y,centre_z);
        pcl::PointXYZ mypoint;
        mypoint.x = centre_x;
        mypoint.y = centre_y;
        mypoint.z = centre_z;

        cloud->points.push_back(mypoint);
    }

    myoccupancygrid->compute_normals(cloud);

    pcl::MomentOfInertiaEstimation <pcl::PointXYZ> feature_extractor;

    feature_extractor.setInputCloud (total_cloud);
    feature_extractor.compute ();

    std::vector <float> moment_of_inertia;
    std::vector <float> eccentricity;
    pcl::PointXYZ min_point_AABB;
    pcl::PointXYZ max_point_AABB;
    pcl::PointXYZ min_point_OBB;
    pcl::PointXYZ max_point_OBB;
    pcl::PointXYZ position_OBB;
    Eigen::Matrix3f rotational_matrix_OBB;
    float major_value, middle_value, minor_value;
    Eigen::Vector3f major_vector, middle_vector, minor_vector;
    Eigen::Vector3f mass_center;

    feature_extractor.getMomentOfInertia (moment_of_inertia);
    feature_extractor.getEccentricity (eccentricity);
    feature_extractor.getAABB (min_point_AABB, max_point_AABB);
    feature_extractor.getOBB (min_point_OBB, max_point_OBB, position_OBB, rotational_matrix_OBB);
    feature_extractor.getEigenValues (major_value, middle_value, minor_value);
    feature_extractor.getEigenVectors (major_vector, middle_vector, minor_vector);
    feature_extractor.getMassCenter (mass_center);

    /*
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
    viewer->setBackgroundColor (0, 0, 0);
    //viewer->addCoordinateSystem (1.0);
    viewer->initCameraParameters ();

    viewer->addPointCloud<pcl::PointXYZ> (total_cloud, "sample cloud");
    viewer->addCube (min_point_AABB.x, max_point_AABB.x, min_point_AABB.y, max_point_AABB.y, min_point_AABB.z, max_point_AABB.z, 1.0, 1.0, 0.0, "AABB");

    Eigen::Vector3f position (position_OBB.x, position_OBB.y, position_OBB.z);
    Eigen::Quaternionf quat (rotational_matrix_OBB);
    viewer->addCube (position, quat, max_point_OBB.x - min_point_OBB.x, max_point_OBB.y - min_point_OBB.y, max_point_OBB.z - min_point_OBB.z, "OBB");

    pcl::PointXYZ center (mass_center (0), mass_center (1), mass_center (2));
    pcl::PointXYZ x_axis (major_vector (0) + mass_center (0), major_vector (1) + mass_center (1), major_vector (2) + mass_center (2));
    pcl::PointXYZ y_axis (middle_vector (0) + mass_center (0), middle_vector (1) + mass_center (1), middle_vector (2) + mass_center (2));
    pcl::PointXYZ z_axis (minor_vector (0) + mass_center (0), minor_vector (1) + mass_center (1), minor_vector (2) + mass_center (2));
    viewer->addLine (center, x_axis, 1.0f, 0.0f, 0.0f, "major eigen vector");
    viewer->addLine (center, y_axis, 0.0f, 1.0f, 0.0f, "middle eigen vector");
    viewer->addLine (center, z_axis, 0.0f, 0.0f, 1.0f, "minor eigen vector");


    //--------------------
    // -----Main loop-----
    //--------------------
    while (!viewer->wasStopped ())
    {
      viewer->spinOnce (100);
      boost::this_thread::sleep (boost::posix_time::microseconds (100000));
    }
    */



    Eigen::Quaternionf quat (rotational_matrix_OBB);

    // visualization
    myvisualizer.add_grid_cells(myoccupancygrid->all_cells_);

    while (ros::ok())
    {
        static tf::TransformBroadcaster br;
        tf::Transform transform;
        transform.setOrigin( tf::Vector3(0,0,0) );
        transform.setRotation( tf::Quaternion(0, 0, 0) );
        br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "odom", "base_link"));

        tf::Transform transform_scene;
        transform_scene.setOrigin( tf::Vector3(0,0,0) );
        transform_scene.setRotation( tf::Quaternion(quat.x(), quat.y(), quat.z(),quat.w()) );
        br.sendTransform(tf::StampedTransform(transform_scene, ros::Time::now(), "odom", "iser_transform"));

        myvisualizer.visualize_cubes();

        ros::spinOnce();
        loop_rate.sleep();

    }


    return 0;


}

/*
boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
viewer->setBackgroundColor (0, 0, 0);
//viewer->addCoordinateSystem (1.0);
viewer->initCameraParameters ();

viewer->addPointCloud<pcl::PointXYZ> (cloud, "sample cloud");
viewer->addCube (min_point_AABB.x, max_point_AABB.x, min_point_AABB.y, max_point_AABB.y, min_point_AABB.z, max_point_AABB.z, 1.0, 1.0, 0.0, "AABB");

Eigen::Vector3f position (position_OBB.x, position_OBB.y, position_OBB.z);
Eigen::Quaternionf quat (rotational_matrix_OBB);
viewer->addCube (position, quat, max_point_OBB.x - min_point_OBB.x, max_point_OBB.y - min_point_OBB.y, max_point_OBB.z - min_point_OBB.z, "OBB");

pcl::PointXYZ center (mass_center (0), mass_center (1), mass_center (2));
pcl::PointXYZ x_axis (major_vector (0) + mass_center (0), major_vector (1) + mass_center (1), major_vector (2) + mass_center (2));
pcl::PointXYZ y_axis (middle_vector (0) + mass_center (0), middle_vector (1) + mass_center (1), middle_vector (2) + mass_center (2));
pcl::PointXYZ z_axis (minor_vector (0) + mass_center (0), minor_vector (1) + mass_center (1), minor_vector (2) + mass_center (2));
viewer->addLine (center, x_axis, 1.0f, 0.0f, 0.0f, "major eigen vector");
viewer->addLine (center, y_axis, 0.0f, 1.0f, 0.0f, "middle eigen vector");
viewer->addLine (center, z_axis, 0.0f, 0.0f, 1.0f, "minor eigen vector");


//--------------------
// -----Main loop-----
//--------------------
while (!viewer->wasStopped ())
{
  viewer->spinOnce (100);
  boost::this_thread::sleep (boost::posix_time::microseconds (100000));
}
*/

/*
    Eigen::Matrix3f covariance_matrix;
    Eigen::Vector4f xyz_centroid;

    // Estimate the XYZ centroid
    pcl::compute3DCentroid (mycloud, xyz_centroid);

    // Compute the 3x3 covariance matrix
    pcl::computeCovarianceMatrix (mycloud, xyz_centroid, covariance_matrix);

    */

