from __future__ import print_function
test_images = [64, 28, 60, 30, 23, 7, 9, 32, 12, 22, 18, 52, 1, 26]

import sys
import numpy as np

one_w_dict = dict(zip('obl', (2, 4, 2)))
n_best_estimates = dict(zip('obl', (10, 2, 10)))

sys.path.append('/home/landsiedel/code/nyu_dataset/')

from src.solve_sprel import run_db
from src.sprel_utils import get_supp_labels, get_feat_labels, get_gt, get_all_feat_labels, prepare, train_clf
from mlnsupport.classification import get_n_best_pred_labels

valid_images = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 45, 46, 47, 49, 50, 51, 52, 55, 57, 58, 59, 60, 61, 62, 63, 64, 66, 67, 68]


import random
random.seed(12345)
shuffled_images = valid_images[:]
random.shuffle(shuffled_images)
print(shuffled_images)
train_images, test_images = shuffled_images[:48], shuffled_images[48:]
len(train_images), len(test_images)


all_relix, all_features, all_labels = get_all_feat_labels(train_images)
print(all_relix.shape, all_features.shape, all_labels.shape)


# train classifiers

import warnings
warnings.simplefilter("ignore")

clfs = {}

for clf_type in 'obl':
    print('*** training {} classifier ***'.format(clf_type))
    scaler, scoring_measures, X_train, X_test, y_train, y_test, tuned_parameters = prepare(all_features, all_labels, clf_type)
    clfs[clf_type] = (train_clf(scoring_measures, X_train, X_test, y_train, y_test, tuned_parameters, one_w_dict[clf_type]), scaler)


# Interaction with the MLN

# Prepare MLN data

# classify and calculate confidences for all images

all_t_relix, all_t_features, _ = get_all_feat_labels(valid_images)
all_t_features[np.where(np.isnan(all_t_features))] = 0

confidences = {}

for clf_type in 'obl':
    clf, scaler = clfs[clf_type]
    feat_scaled = scaler.transform(all_t_features)
    confidences[clf_type] = clf['recall'].decision_function(feat_scaled).squeeze()


PRED_NAMES = dict(zip('obl', 'On Behind LeftOf'.split()))


def write_db(img_ix, canopies, estimates, sprel, filename, all_regions, prefix=''):
    """write all features and ground truth support relations"""
    # print('writing features for image {} to {}'.format(img_ix, filename))

    with open(filename, 'w') as f:
        print(prefix, file=f)
        # print direct support
        for clf_type in 'obl':
            for i, j in all_regions:
                if (i, j) in estimates[clf_type]:
                    print('clf{3}(Region_{1}_Img_{2}, Region_{0}_Img_{2})'.format(i, j, img_ix, PRED_NAMES[clf_type]), file=f)
                if (i, j) in canopies[clf_type]:
                    suffix = '' if get_gt(sprel, i, j, clf_type) else '!'
                    print(suffix + '{3}(Region_{1}_Img_{2}, Region_{0}_Img_{2})'.format(i, j, img_ix, PRED_NAMES[clf_type]), file=f)


# write all files
for img_ix in valid_images:
    estimates, canopies = {}, {}
    sprel = get_supp_labels(img_ix)
    for clf_type in 'obl':
        s_c_ix = get_n_best_pred_labels(img_ix, confidences[clf_type], all_t_relix[:, 1:], all_t_relix[:, 0], n=n_best_estimates[clf_type], conf_threshold=0)
        assert np.all(all_t_relix[s_c_ix, 0] == img_ix)
        estimates[clf_type] = map(tuple, all_t_relix[s_c_ix, 1:])
        canopies[clf_type] = map(tuple, all_t_relix[all_t_relix[:, 0] == img_ix, 1:]) if clf_type != 'b' else map(tuple, all_t_relix[s_c_ix, 1:])
        # map(tuple, all_t_relix[all_t_relix[:, 0] == img_ix, 1:])

    filename = 'db/sprel/sprel_{}.db'.format(img_ix)
    write_db(img_ix, canopies, estimates, sprel, filename, all_t_relix[all_t_relix[:, 0] == img_ix, 1:])


# write training files
for img_ix in train_images:
    estimates, canopies = {}, {}
    sprel = get_supp_labels(img_ix)
    for clf_type in 'obl':
        s_c_ix = get_n_best_pred_labels(img_ix, confidences[clf_type], all_t_relix[:, 1:], all_t_relix[:, 0], n=n_best_estimates[clf_type], conf_threshold=0)
        assert np.all(all_t_relix[s_c_ix, 0] == img_ix)
        estimates[clf_type] = map(tuple, all_t_relix[s_c_ix, 1:])
        canopies[clf_type] = map(tuple, all_t_relix[all_t_relix[:, 0] == img_ix, 1:]) if clf_type != 'b' else map(tuple, all_t_relix[s_c_ix, 1:])
        # map(tuple, all_t_relix[all_t_relix[:, 0] == img_ix, 1:])

    filename = 'db/train/sprel_{}.db'.format(img_ix)
    write_db(img_ix, canopies, estimates, sprel, filename, all_t_relix[all_t_relix[:, 0] == img_ix, 1:])

# Train MLN

from mlnsupport.mln_training_call import train
train('db/train/', 'mlnfiles/sprel.mln', query_atoms=PRED_NAMES.values(), discriminative=True, add_unit_clauses=True, )
train('db/train/', 'mlnfiles/sprel.mln', query_atoms=PRED_NAMES.values(), discriminative=False, add_unit_clauses=True, )


# mlnfile = 'mlnfiles/sprel-train__d-out.mln'
# run_db(mlnfile, test_images)
mlnfile = 'mlnfiles/sprel-train__g-out.mln'
run_db(mlnfile, test_images)

from src.sprel_utils import get_mln_pred

import warnings
warnings.simplefilter("ignore")

get_mln_pred('./db/sprel-train__g/')
