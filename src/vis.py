from features import compute_bbox, get_object_rotation
import numpy as np
import scipy
from scipy import stats

import rospy
import tf

from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import Quaternion, Point, Vector3
from std_msgs.msg import ColorRGBA

hash_fn = {
    'car': ([0, 0, 255], 'car'),
    'house': ([0, 255, 0], 'building (house)'),
    'street': ([255, 0, 0], 'street'),
    'side_walk': ([0, 0, 128], 'sidewalk'),
    'bike': ([184, 134, 11], 'bicycle'),
    'other': ([128, 128, 128], 'other'),
    'tree': ([69, 139, 0], 'tree'),
    'ppole': ([0, 139, 139], 'pole'),
    'sky': ([128, 128, 128], 'sky (up)'),
    'grass': ([34, 139, 34], 'grass')
}


def pub_obj_markers(obj, pub, col=(1, 1, 1,), start_id=0):
    markers = []
    l = start_id
    for c in obj.cuboids:
        if np.random.rand() > .02:
            continue
        m = Marker()
        m.id = l
        m.ns = 'cuboids'
        m.type = Marker.CUBE
        m.header.frame_id = "chris_iser_transform"
        m.header.stamp = rospy.get_rostime()
        m.color = ColorRGBA(*(col + (1, )))
        m.pose.position = Point(c.cuboid.point.x, c.cuboid.point.y, c.cuboid.point.z)
        m.scale = Vector3(.1, .1, .1)
        markers.append(m)
        l += 1

        a = Marker()
        a.id = l
        a.ns = 'normals'
        a.type = Marker.ARROW
        a.header.frame_id = "chris_iser_transform"
        a.header.stamp = rospy.get_rostime()
        a.color = ColorRGBA(1, 1, 1, 1)
        a.pose.position = Point(c.cuboid.point.x, c.cuboid.point.y, c.cuboid.point.z)
        normal = [c.normal.x, c.normal.y, c.normal.z]
        x, y, z = np.cross([1, 0, 0], normal)
        q = Quaternion(x, y, z, 1 + np.dot([1, 0, 0], normal))
        a.pose.orientation = q
        a.scale = Vector3(1.5, .05, .05)

        markers.append(a)
        l += 1

    ma = MarkerArray()
    ma.markers = markers
    pub.publish(ma)

    return l


def get_obj_class(obj):
    from operator import attrgetter
    object_classes = map(attrgetter('label'), obj.cuboids)
    class_counts = scipy.stats.itemfreq(object_classes)
    return class_counts[np.argmax(class_counts[:, 1]), 0]


def build_bbox_text(obj, marker_id):
    this_obj_class = get_obj_class(obj)

    t = Marker()
    t.action = Marker.ADD
    t.header.frame_id = "object_{}".format(marker_id)
    t.header.stamp = rospy.rostime.get_rostime()
    t.ns = 'sortree_numbers'
    t.type = Marker.CUBE
    x, y, z = (0, 0, 0)
    t.pose.position.x, t.pose.position.y, t.pose.position.z = x, y, z
    t.scale.x, t.scale.y, t.scale.z = (3, 3, 3)

    t.type = Marker.TEXT_VIEW_FACING
    t.id = marker_id + 100
    t.text = str(marker_id) + this_obj_class
    t.color.r, t.color.g, t.color.b, t.color.a = (1, 1, 1, 1)

    return t


def build_bbox_marker(marker_id, obj, bbox_edges, bbox_center):
    m = Marker()
    m.id = marker_id
    m.action = Marker.ADD
    m.header.frame_id = "object_{}".format(marker_id)
    m.header.stamp = rospy.rostime.get_rostime()
    m.ns = 'sortree_bboxes'
    m.type = m.CUBE
    m.pose.position.x, m.pose.position.y, m.pose.position.z = bbox_center
    bbox_dims = bbox_edges[1] - bbox_edges[0]
    m.scale.x, m.scale.y, m.scale.z = bbox_dims
    m.color.a = .4
    col = np.array(hash_fn.get(get_obj_class(obj), ([0, 0, 255], None))[0]) / 255.0
    m.color.r, m.color.g, m.color.b = col

    return m


def publish_arrows(obj, pub):
    normals = np.array([(c.normal.x, c.normal.y, c.normal.z) for c in obj.cuboids])
    centroids = np.array([(c.cuboid.point.x, c.cuboid.point.y, c.cuboid.point.z) for c in obj.cuboids])

    valid = np.logical_not(np.any(np.isnan(normals), axis=1))
    valid_centroids = centroids[valid, :]

    plane_eqs = np.hstack((normals[valid, :], np.atleast_2d(np.sum(normals[valid, :] * valid_centroids, axis=1)).T))
    np.sum(plane_eqs * np.hstack((valid_centroids, -np.ones((len(valid_centroids), 1)))), axis=1)
    from scipy.cluster.vq import whiten, kmeans2
    w_plane_eqs = whiten(plane_eqs)
    clu_centroids, clu_labels = kmeans2(w_plane_eqs, 7)

    for l, c in enumerate(clu_centroids):
        m = Marker()
        m.id = l + 200
        m.type = Marker.ARROW
        m.header.frame_id = "chris_iser_transform"
        m.header.stamp = rospy.get_rostime()
        m.color = ColorRGBA(1, 1, 1, 1)
        center = np.mean(valid_centroids[clu_labels == l, :], axis=0)
        m.pose.position = Point(*tuple(center))
        q = Quaternion(*tf.transformations.quaternion_from_euler(0, np.arctan2(c[0], c[1]), c[2]))
        m.pose.orientation = q
        m.scale = Vector3(5, 1, 1)
        pub.publish(m)
