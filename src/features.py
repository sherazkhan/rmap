# agglomerate cuboid centroids
import numpy as np
from scipy.spatial import ConvexHull

from geometry_msgs.msg import Quaternion
import rospy
import tf
import scipy


def perc_point_in_hull(points, hull):
    offset_points = points.copy()
    offset_points[:, -1] = 1
    return np.sum(np.all(np.dot(hull.equations, offset_points.T) <= 0, axis=0)) / float(len(points))


def calc_cont_hull_perc(cuboids_c1, cuboids_c2, ax=2):
    # compute convex hull
    from scipy.spatial.qhull import QhullError
    iv = np.array([True, True, True])
    iv[ax] = False
    try:
        hull1 = ConvexHull(cuboids_c1[:, iv], qhull_options='Qbb Qc Qz QJ')
        hull2 = ConvexHull(cuboids_c2[:, iv], qhull_options='Qbb Qc Qz QJ')
        return perc_point_in_hull(cuboids_c1, hull2), perc_point_in_hull(cuboids_c2, hull1)
    except QhullError:
        print 'error computing convex hull'
        return (0., 0.)


def calc_angles(obj):
    # centroids = np.array([(c.cuboid.x, c.cuboid.y, c.cuboid.z) for c in cuboids])
    normals = np.array([(c.normal.x, c.normal.y, c.normal.z) for c in obj.cuboids])
    # filter out nan
    valid = np.logical_not(np.any(np.isnan(normals), axis=1))

    phi = np.arctan2(normals[valid, 0], normals[valid, 1])
    phi = phi[np.logical_not(np.isinf(phi))]
    theta = np.arccos(normals[valid, 2])
    theta = theta[np.logical_not(np.isinf(theta))]

    return normals, phi, theta


def calc_normals_related_features(obj1, obj2):
    # cuboid_centroids1 = np.array([(c.cuboid.x, c.cuboid.y, c.cuboid.z) for c in obj1.cuboids])
    # cuboid_centroids2 = np.array([(c.cuboid.x, c.cuboid.y, c.cuboid.z) for c in obj2.cuboids])

    normals1, phi1, theta1 = calc_angles(obj1)
    normals2, phi2, theta2 = calc_angles(obj2)

    angle_hist1, _, _ = np.histogram2d(phi1, theta1, bins=[np.linspace(-np.pi, np.pi, 10), np.linspace(0, np.pi, 10)])
    angle_hist2, _, _ = np.histogram2d(phi2, theta2, bins=[np.linspace(-np.pi, np.pi, 10), np.linspace(0, np.pi, 10)])

    hists = np.vstack((angle_hist1.ravel(), angle_hist1.ravel()))
    valid = np.sum(hists, axis=0) > 0

    hist_dist = np.sum((angle_hist1.ravel()[valid] - angle_hist2.ravel()[valid]) ** 2 /
                       (angle_hist1.ravel()[valid] + angle_hist2.ravel()[valid])) / 2

    vert_region_ix_1 = abs(theta1 - np.pi/2) < .4
    horz_region_ix_1 = abs(theta1 - np.pi/2) > (np.pi / 2 - .4)

    vert_region_ix_2 = abs(theta1 - np.pi/2) < .4
    horz_region_ix_2 = abs(theta1 - np.pi/2) > (np.pi / 2 - .4)

    return [hist_dist, np.log(np.sum(vert_region_ix_1)), np.sum(vert_region_ix_1) / len(vert_region_ix_1),
            np.log(np.sum(vert_region_ix_2)), np.sum(vert_region_ix_2) / len(vert_region_ix_2),
            np.log(np.sum(horz_region_ix_1)), np.sum(horz_region_ix_1) / len(horz_region_ix_1)]


def calc_angular_features(cuboid_centroids1, cuboid_centroids2):
    phis_1 = np.arctan2(cuboid_centroids1[:, 0], cuboid_centroids1[:, 1])
    phis_2 = np.arctan2(cuboid_centroids2[:, 0], cuboid_centroids2[:, 1])

    all_cuboids_centroid_1 = np.mean(cuboid_centroids1, axis=0)
    all_cuboids_centroid_2 = np.mean(cuboid_centroids2, axis=0)

    minphi_1 = np.min(phis_1)
    minphi_2 = np.min(phis_2)
    cphi_1 = np.arctan2(all_cuboids_centroid_1[0], all_cuboids_centroid_1[1])
    cphi_2 = np.arctan2(all_cuboids_centroid_2[0], all_cuboids_centroid_2[1])
    maxphi_1 = np.max(phis_1)
    maxphi_2 = np.max(phis_2)

    lmp1 = cuboid_centroids1[phis_1 == np.min(phis_1), :]
    lmp2 = cuboid_centroids2[phis_2 == np.min(phis_2), :]

    rmp1 = cuboid_centroids1[phis_1 == np.max(phis_1), :]
    rmp2 = cuboid_centroids2[phis_2 == np.max(phis_2), :]

    lmpdist_1 = np.mean(np.linalg.norm(lmp1, axis=1))
    lmpdist_2 = np.mean(np.linalg.norm(lmp2, axis=1))

    rmpdist_1 = np.mean(np.linalg.norm(rmp1, axis=1))
    rmpdist_2 = np.mean(np.linalg.norm(rmp2, axis=1))

    return np.array([minphi_1 - minphi_2, maxphi_1 - maxphi_2, minphi_1 - maxphi_2, maxphi_1 - minphi_2, cphi_1 - cphi_2, lmpdist_1, lmpdist_2, rmpdist_1, rmpdist_2])


def calc_geometry_features(cuboid_centroids1, cuboid_centroids2, obj1_id, obj2_id, bboxes, eps=0.2):
    tf_all_edges1 = bboxes[obj1_id][2]
    tf_all_edges2 = bboxes[obj2_id][2]

    all_cuboids_centroid_1 = np.mean(cuboid_centroids1, axis=0)
    all_cuboids_centroid_2 = np.mean(cuboid_centroids2, axis=0)

    centroid_dist = np.linalg.norm(all_cuboids_centroid_1 - all_cuboids_centroid_2)
    min_corner_dist = np.min(scipy.spatial.distance.cdist(tf_all_edges1, tf_all_edges2))
    max_corner_dist = np.max(scipy.spatial.distance.cdist(tf_all_edges1, tf_all_edges2))

    feat = [np.array([centroid_dist, min_corner_dist, max_corner_dist])]

    for vec in map(np.array, ([1, 0, 0], [0, 1, 0], [0, 0, 1])):
        diffs = np.repeat(np.dot(tf_all_edges1, vec), 8).reshape((8, 8)) - np.repeat(np.dot(tf_all_edges2, vec).T, 8).reshape((8, 8)).T
        # print np.max(diffs), np.min(diffs)
        feat += [np.array([np.max(diffs), np.min(diffs)])]

    # axis-aligned differences between centroids
    feat.append(all_cuboids_centroid_1 - all_cuboids_centroid_2)

    # hull points overlap for all 3 coordinate directions
    feat.append(np.array(calc_cont_hull_perc(cuboid_centroids1, cuboid_centroids2, 0)))
    feat.append(np.array(calc_cont_hull_perc(cuboid_centroids1, cuboid_centroids2, 1)))
    feat.append(np.array(calc_cont_hull_perc(cuboid_centroids1, cuboid_centroids2, 2)))

    min_val = np.min(cuboid_centroids1, axis=0)
    max_val = np.max(cuboid_centroids1, axis=0)
    for vec in map(np.array, ([True, False, False],
                              [False, True, False],
                              [False, False, True])):
        min_ixs = cuboid_centroids1[:, vec] < min_val[vec] + eps
        max_ixs = cuboid_centroids1[:, vec] > max_val[vec] - eps
        opt_ixs = np.logical_or(min_ixs, max_ixs)
        exterior_points = cuboid_centroids1[opt_ixs[:, 0], :]

        dists = scipy.spatial.distance.cdist(exterior_points, cuboid_centroids2)
        feat.append(np.array([np.min(dists)]))

    feat.append(calc_angular_features(cuboid_centroids1, cuboid_centroids2))

    return np.concatenate(feat)


def get_obj_centroids(obj):
    return np.array([(c.cuboid.point.x, c.cuboid.point.y, c.cuboid.point.z) for c in obj.cuboids])


def compute_bbox(obj, object_id, br=None, overshoot_percentage=2.5):
    """returned tf matrix transforms from base frame to object frame.
    returned coordinates are in object frame."""
    cuboid_centroids = get_obj_centroids(obj)
    normals = np.array([(c.normal.x, c.normal.y, c.normal.z) for c in obj.cuboids])

    big_bbox_center = np.mean([np.max(cuboid_centroids, axis=0),
                               np.min(cuboid_centroids, axis=0)], axis=0)
    # bbox_center = np.percentile(cuboid_centroids, 50, axis=0)

    phi = get_object_rotation(normals)
    normal = [np.cos(phi), np.sin(phi), 0]
    x, y, z = np.cross(normal, [1, 0, 0])
    q = Quaternion(x, y, z, 1 + np.dot([1, 0, 0], normal))

    if br:
        br.sendTransform(big_bbox_center,
                         (q.x, q.y, q.z, q.w),
                         rospy.Time.now(),
                         "object_{}".format(object_id),
                         '/chris_iser_transform')

    tf_matrix = np.dot(tf.transformations.translation_matrix(big_bbox_center),
                       tf.transformations.quaternion_matrix((x, y, z, 1 + np.dot([1, 0, 0], normal))))
    tf_matrix = tf.transformations.inverse_matrix(tf_matrix)

    tf_cuboid_centroids = np.dot(tf_matrix, np.hstack((cuboid_centroids, np.ones((len(cuboid_centroids), 1)))).T).T

    bbox_edges = np.vstack((np.percentile(tf_cuboid_centroids, overshoot_percentage, axis=0),
                            np.percentile(tf_cuboid_centroids, 100 - overshoot_percentage, axis=0)))[:, :-1]
    bbox_dims = bbox_edges[1] - bbox_edges[0]

    # print bbox_edges.shape, bbox_dims.shape
    all_edges = np.vstack((bbox_edges[0, :],
                           bbox_edges[0, :] + bbox_dims * np.array((1, 0, 0)),
                           bbox_edges[0, :] + bbox_dims * np.array((0, 1, 0)),
                           bbox_edges[0, :] + bbox_dims * np.array((0, 0, 1)),
                           bbox_edges[1, :],
                           bbox_edges[1, :] - bbox_dims * np.array((1, 0, 0)),
                           bbox_edges[1, :] - bbox_dims * np.array((0, 1, 0)),
                           bbox_edges[1, :] - bbox_dims * np.array((0, 0, 1)),
                           ))

    bbox_center = np.mean(bbox_edges, axis=0)

    return bbox_edges, bbox_center, bbox_dims, all_edges, tf_matrix


def get_object_rotation(normals):
    # filter out nan
    valid = np.logical_not(np.any(np.isnan(normals), axis=1))

    phi = np.arctan2(normals[valid, 0], normals[valid, 1])
    phi = phi[np.logical_not(np.isinf(phi))]
    # theta = np.arccos(normals[valid, 2], np.linalg.norm(normals[valid, 0], normals[valid, 1]))
    # theta = theta[np.logical_not(np.isinf(theta))]

    hist = np.histogram(phi, bins=np.linspace(-np.pi, np.pi, 360))
    combined_hist = hist[0] + np.roll(hist[0], 90)
    rotation = (np.argmax(combined_hist) - 180) / 180. * np.pi
    return rotation


def get_bbox_edges():
    pass
