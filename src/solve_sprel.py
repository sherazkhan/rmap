# coding: utf-8

"""script to ground, order reduce and solve qpbo

first, run

    python mlnsupport/spatial_kb.py build_spatial_with_sql_single_core [<no>] -f -b

to ground.
"""

import os
import cPickle as pickle
import numpy as np
import pprint
# from plop.collector import Collector
from commandr import Run, command

from pyference.variable_types import BinaryVariable, SlackVariable
from pyference.order_reducer import OrderReducer
from pyference.utils import Stopwatch
from pyference.opengm_utils import solve_opengm
from pyference.factor_graph import fg_statistics
from qpbo_wrapper import qpbo_solver

stopwatch = Stopwatch()


def QSSR(fg_original):
    """Quadratize-Solve-Simplify-Repeat procedure"""
    fg_ho = fg_original.copy()

    for i in range(10):
        fg = fg_ho.copy()
        print("Quadratization {}".format(i + 1))
        OrderReducer(fg).reduce()
        print("Solving(QPBO)")
        qpbo = qpbo_solver.QpboSolver(fg, type="int", scale_factor=10000)
        qpbo.build_qpbo()
        stopwatch.start()
        qpbo.solve_weak()
        elapsed_time = stopwatch.lap_time()
        weak_assignments = qpbo.get_weak_assignments()

        map(lambda (x, v): x.set(v).clamp(), weak_assignments.iteritems())  # set to qpbo-solution and clamp
        fg_ho.update_clamped_factors()
        # fg_ho.apply_clamp()

        variables_reduced = fg.get_sorted_variables()
        sol = qpbo.get_qpbo_solution()
        primary_sol = get_primary_solution(sol, variables_reduced)
        try:
            qpbo_lower_bound = qpbo.compute_adjusted_lower_bound(fg, relative_offset=2)
        except Exception, e:
            print("Lower bound fail")
            print e
            qpbo_lower_bound = "FAIL"

        display_results("QPBO", primary_sol, elapsed_time, lower_bound=qpbo_lower_bound)


def improve_qpbo(fg, qpbo, iterations):
    """Runs improve for a number of iterations, changes variables in factor_graph"""
    from random import randint
    stopwatch = Stopwatch()
    for i in range(iterations):
        print "****Improve iter {}****".format(i)
        # solution = qpbo.get_qpbo_solution()
        print "Set fg vars to solution"
        qpbo.set_fg_vars_to_solution()
        [v.set(0) for v in fg.get_variables() if not v.labeled]

        print "Set qpbo vars"
        qpbo.set_qpbo_vars()
        print "old cost: {}".format(qpbo.compute_cost())
        print "Improving"
        stopwatch.start()
        print "Could improve: {}".format(qpbo.improve())
        print "improve time: {}".format(stopwatch.lap_time())
        sol = qpbo.get_qpbo_solution()
        qpbo.set_fg_vars_to_solution()
        qpbo.set_qpbo_vars()
        print "new cost: {}".format(qpbo.compute_cost())
    return qpbo

    pass


def write_result_image(solution, variables, filename):
    import re
    from mlnsupport.utils import get_support_labels, overlay_line_from_to, get_regions, get_image_mp
    from PIL import Image as PILImage

    support_pred = []
    for s, v in zip(solution, variables):
        if s:
            support_pred.append(map(int, re.findall('Region_(\d+)', v.label))[::-1])

    v = variables[0]
    no = np.unique(map(int, re.findall('Img_(\d+)', v.label)))[0]
    support_labels = map(tuple, get_support_labels(no, )[:, :2])

    img = get_image_mp(no)
    region_map = get_regions(no, return_masks=False)

    for rel in support_pred:
        if 0 in rel[:2]:
            continue
        c_img = img
    #     col = [(0), (220, 0, 0), (0, 0, 0)][rel[2]]
        col = (220, 0, 0) if tuple(rel) in support_labels else (0, 0, 220)
        img = overlay_line_from_to(*(map(int, rel[:2])), img=c_img, regions=region_map, return_img=True, col=col)

    im = PILImage.fromarray(img)
    # im.save(infilename.replace('pkl', 'png').replace('db/', 'db/results/'))
    im.save(filename)


def get_primary_solution(solution, variables):
    return [s for (s, v) in zip(solution, variables) if type(v) is BinaryVariable]


def get_primary_solution_and_pos_vars(solution, variables):
    return [v for (s, v) in zip(solution, variables) if type(v) is BinaryVariable and s]


def display_results(description, primary_solution, elapsed_time=None, lower_bound=None, solution_cost=None):
    print("\n***" + description + "***")
    print('Time elapsed: {},\n # of primary variables: {},\n # primaries solved: {},\
        \n # prim. solved to 0: {},\n # prim. solved to 1: {},\n \
         lower bound: {}\n \
         solution_cost: {}'.format(
        elapsed_time or "?",
        len(primary_solution),
        np.sum([np.array(primary_solution) > -1]),
        np.sum([np.array(primary_solution) == 0]),
        np.sum([np.array(primary_solution) == 1]),
        lower_bound or "?",
        solution_cost or "?"))
    print("\n")


def solve(fg, results_path):
    inference_method = "BeliefPropagation"
    qpbo = False
    qpbo_improve = False
    qpbo_smart_improve = False
    do_probe = False  # SLOW
    opengm_bp_ho = True
    opengm_bp_lo = False
    opengm_bp_after_qpbo = False
    do_QSSR = False

    print('Original FG statistics')
    fg_statistics(fg)

    fg.simplify_factors()
    fg_original = fg.copy()
    variables = fg.get_sorted_variables()
    print("Removing redundancy in factors")
    fg_statistics(fg)

    if do_QSSR:
        print "====QSSR procedure===="
        QSSR(fg)

    if opengm_bp_ho:
        print "Solving Higher Order BP(OpenGM)"

        cost_gm_bp_ho, sol_gm_bp_ho, elapsed_time = solve_opengm(fg, method=inference_method, timeit=True)
        primary_sol_bp_ho = get_primary_solution(sol_gm_bp_ho, variables)
        display_results("BP Higher Order", primary_sol_bp_ho, elapsed_time, solution_cost=cost_gm_bp_ho)

        sol = get_primary_solution_and_pos_vars(sol_gm_bp_ho, variables)

        # write solution and image
        # image_filename = os.path.join(results_path + '.png')
        solfilename = os.path.join(results_path + '.bp_solution')
        with open(solfilename, 'w') as f:
            f.write(pprint.pformat(sol))
        # write_result_image(sol_gm_bp_ho, variables, image_filename)

    print("Quadratization")
    OrderReducer(fg).reduce()
    variables_reduced = fg.get_sorted_variables()
    print("Factor Graph Info after Quadratization")
    fg_statistics(fg)

    if opengm_bp_lo:
        print "Solving Lower Order Order BP(OpenGM)"
        cost_gm_bp_lo, sol_gm_bp_lo, elapsed_time = solve_opengm(fg, method=inference_method, timeit=True)
        primary_sol_bp_lo = get_primary_solution(sol_gm_bp_lo, variables_reduced)
        display_results("BP Lower Order", primary_sol_bp_lo, elapsed_time, solution_cost=cost_gm_bp_lo)

        print(get_primary_solution_and_pos_vars(sol_gm_bp_lo, variables_reduced))

    if qpbo:
        print("Solving(QPBO)")
        qpbo = qpbo_solver.QpboSolver(fg, type="long", scale_factor=10000)
        qpbo.build_qpbo()
        stopwatch.start()
        qpbo.solve_weak()
        elapsed_time = stopwatch.lap_time()
        weak_assignments = qpbo.get_weak_assignments()
        sol = qpbo.get_qpbo_solution()
        primary_sol = get_primary_solution(sol, variables_reduced)
        try:
            qpbo_lower_bound = qpbo.compute_adjusted_lower_bound(fg, relative_offset=2)
        except Exception, e:
            print("Lower bound fail")
            print e
            qpbo_lower_bound = "FAIL"
            raise

        map(lambda (x, v): x.set(v), weak_assignments.iteritems())  # set to qpbo-solution
        print "cost in original fg: {}".format(fg_original.compute_cost())

        display_results("QPBO", primary_sol, elapsed_time, lower_bound=qpbo_lower_bound)

        if qpbo_improve:
            improve_qpbo(fg, qpbo, iterations=10)
            print "cost in original fg: {}".format(fg.compute_cost())

        if qpbo_smart_improve:
            print "\n****Improve smart {}****\n"
            # New smaller problem using solution from qpbo
            print "old fg reduced_variables: {}".format(len(fg.get_variables()))
            fg = fg_original.copy()
            map(lambda (x, v): x.set(v).clamp(), weak_assignments.iteritems())  # set to qpbo-solution and clamp
            fg.update_clamped_factors()
            print "clamped fg variables: {}".format(len(fg.get_variables()))
            fg_statistics(fg)
            print "Quadratization"
            # plopper = Collector()
            # plopper.start()
            stopwatch.start()
            OrderReducer(fg).reduce()
            time = stopwatch.lap_time()
            print "time Quadratization ", time
            # plopper.stop()
            # fg_statistics(fg)
            # with open("plop.out", "w") as f:
                # f.write(repr(dict(plopper.stack_counts)))

            print "new reduced_variables: {}".format(len(fg.get_variables(use_c_fac_set=True)))
            # Solve qpbo again
            # print("Solve")
            qpbo = qpbo_solver.QpboSolver(fg, type="int", scale_factor=10000)
            qpbo.build_qpbo()
            stopwatch.start()
            qpbo.solve_weak()
            qpbo.compute_adjusted_lower_bound(fg, relative_offset=2)
            # for variable,value in weak_assignments.iteritems():
            #     try:
            #         qpbo.SetLabel(self.find_node_id(v), v.get())
            #     except:
            #         pass

            improve_qpbo(qpbo, iterations=10)
            print "cost in original fg: {}".format(fg_original.compute_cost())
            print "cost in original fg: {}".format(fg_original.compute_cost(use_clamped_factor_set=True))

            # print "time: ",stopwatch.lap_time()
            # solution = qpbo.get_qpbo_solution()
            # new_weak_assignments = qpbo.get_weak_assignments()
            # qpbo.compute_adjusted_lower_bound(fg,relative_offset=2)
            # qpbo.set_fg_vars_to_solution()
            # qpbo.set_qpbo_vars()
            # print "old cost: {}".format(qpbo.compute_cost())
            # print "Improving"
            # print "Could improve: {}".format(qpbo.improve())
            # sol = qpbo.get_qpbo_solution()
            # qpbo.set_fg_vars_to_solution()
            # qpbo.set_qpbo_vars()
            # print "new cost: {}".format(qpbo.compute_cost())
            # [x.unclamp() for x in variables]
            # import itertools
            # [x.set(v) for x,v in itertools.chain(weak_assignments.iteritems(),new_weak_assignments.iteritems())] #set to qpbo-solution and clamp
            # print "new cost in fg: {}".format(fg_original.compute_cost(use_clamped_factor_set=True))
        print('# primaries: {}, # slacks: {}'.format(
            sum([1 for v in variables_reduced if type(v) is BinaryVariable]),
            sum([1 for v in variables_reduced if type(v) is SlackVariable])))

    if qpbo and do_probe:
        print "Probing"
        qpbo.probe()
        sol_probe = qpbo.get_qpbo_solution()

    if opengm_bp_after_qpbo:
        map(lambda (x, v): x.set(v).clamp(), weak_assignments.iteritems())  # set to qpbo-solution and clamp
        fg_original.update_clamped_factors()
        cost_gm_qpbo_bp_lo, sol_gm_qpbo_lo, elapsed_time = solve_opengm(fg_original, method=inference_method, timeit=True)
        primary_qpbo_sol_bp_lo = get_primary_solution(sol_gm_qpbo_lo, variables)
        display_results("BP Higher Order Clamped", primary_qpbo_sol_bp_lo, elapsed_time, solution_cost=cost_gm_qpbo_bp_lo)

    primary_sol = get_primary_solution(sol, variables_reduced)

    if do_probe:
        primary_sol_probe = get_primary_solution(sol_probe, variables_reduced)
        display_results("QPBO", primary_sol_probe)


def solve_worker((no, (mlnfile, res_path))):
    try:
        from src.sprel_db import build_sprel_with_sql_single_core
        _, fg = build_sprel_with_sql_single_core(no, build_fg=True, from_pbf=True, out=None, mlnfile=mlnfile, transform=False)

        filename = os.path.join(res_path, 'result_{}'.format(no))
        solve(fg, filename)
        print('done solving # {}, results in '.format(no, filename))
    except:
        import sys
        import traceback
        except_type, except_class, tb = sys.exc_info()
        traceback.print_exc()
        raise


@command('run')
def run_db(mlnfile, images):
    import multiprocessing

    # create directory for results
    results_path = os.path.join('db', os.path.split(mlnfile)[1].rstrip('-out.mln'))
    try:
        os.mkdir(results_path)
    except OSError:
        pass

    allargs = zip(images, [(mlnfile, results_path)] * len(images))
    pool = multiprocessing.Pool(processes=32)

    pool.map(solve_worker, allargs)


if __name__ == '__main__':
    Run()
