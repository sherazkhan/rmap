#!/usr/bin/env python
import roslib
roslib.load_manifest('isertree')
from isertree.srv import cuboid_service, cuboid_serviceRequest
import rospy


def cuboid_client():
    rospy.wait_for_service('rtree/cuboid_service')

    req = cuboid_serviceRequest()
    req.label = 'car'
    print 'req', req
    try:
        req_cuboids = rospy.ServiceProxy('rtree/cuboid_service', cuboid_service)
        resp1 = req_cuboids(req)
        return resp1
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e
	raise


if __name__ == "__main__":
    result = cuboid_client()
    print len(result.cuboids)

