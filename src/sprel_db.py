"""
sprel_kb
~~~~~~~~~~~~~~~~~~~~~

Interface to Alchemy and pyference for the spatial/support domain
"""

import re
import numpy as np
from commandr import command, Run

from experiments import cora_db
from pyference.mln import Predicate, Domain, ConstrainedDomain

from sympy import to_cnf, Not, And, Implies, Equivalent
from pyference.sql.sql_grounding import Exists
from pyference.sql.grounding_utils import parse_formula


def get_learnt_sprel_formulas(img_nos, also_data=True, also_labels=False, max_n_formulas=None,
                              learnt_formulas_filename=None, verbose=False):
    db = load_spatial_db(img_nos)

    from experiments.cora_db import analyze_data
    if verbose:
        analyze_data(db)

    # domains
    region = Domain('region')

    # Predicates
    On = Predicate('On', (region, region))
    Behind = Predicate('Behind', (region, region))
    LeftOf = Predicate('LeftOf', (region, region))

    clfOn = Predicate('clfOn', (region, region))
    clfBehind = Predicate('clfBehind', (region, region))
    clfLeftOf = Predicate('clfLeftOf', (region, region))

    predicates = {p.name: p for p in [On, Behind, LeftOf, clfOn, clfBehind, clfLeftOf]}
    # populate domains
    region.objects = db.get_domain('region')

    for p in predicates:
        db.arg_types[p] = ['region', 'region']

    # Supports.all_groundable = True
    # SupportsTransitive.all_groundable = True

    ''' Set values of clamped predicates '''
    # IsSupporttype.clamp(db.get_predicate_groundings('IsSupporttype', type='positive'))
    clfOn.clamp(db.get_predicate_groundings('clfOn', type='positive'))
    clfBehind.clamp(db.get_predicate_groundings('clfBehind', type='positive'))
    clfLeftOf.clamp(db.get_predicate_groundings('clfLeftOf', type='positive'))

    ''' Set canopied values '''
    On.groundable_cases = db.get_predicate_groundings('On', type='all')
    Behind.groundable_cases = db.get_predicate_groundings('Behind', type='all')
    LeftOf.groundable_cases = db.get_predicate_groundings('LeftOf', type='all')

    # out_true = db.get_predicate_groundings('SameBib', type='positive')[0:]
    # out_false = db.get_predicate_groundings('SameBib', type='negative')[0:]
    # true_keys = [(SameBib, assignment) for assignment in out_true]
    # false_keys = [(SameBib, assignment) for assignment in out_false]

    print('loading formulas from {}'.format(learnt_formulas_filename))

    weight_formula_pairs = []
    with open(learnt_formulas_filename, 'r') as f:
        for ind, line in enumerate(f):
            wf = parse_formula(line, predicates)
            if wf:
                # print(ind, line, '\n', wf)
                weight_formula_pairs.append(wf)

            if max_n_formulas and max_n_formulas < ind:
                break

    retval = (weight_formula_pairs, )

    # if also_labels:
    #     retval += (true_keys, false_keys)

    if also_data:
        retval += (db, )

    return retval[0] if len(retval) == 1 else retval


def load_spatial_db(img_nos):
    """
    returns a :py:class:`ParsedData` object that contains the Cora data from the images in  `img_nos`.
    """
    lfile = ['db/sprel/sprel_{}.db'.format(img_nos)]

    pdata = cora_db.parse_files(lfile, remove_type=False)

    return pdata


# def load_spatial_db_split(db_idx):
#     """
#     returns a :py:class:`ParsedData` object that contains the Cora data from the splits in `db_idx`.
#     """
#     from mlnsupport.utils import img_split
#     lfiles = ['db/db{}/mln_features_img{}.db'.format(db_idx, no) for no in img_split(db_idx)]

#     pdata = cora_db.parse_files(lfiles, remove_type=False)

#     return pdata


@command('build_sprel_with_sql_single_core')
def build_sprel_with_sql_single_core(images, mlnfile, build_fg=False, from_pbf=False, out=None, transform=False):
    from pyference.sql.grounding_utils import build_with_sql

    weight_formula_pairs, db = get_learnt_sprel_formulas(images, learnt_formulas_filename=mlnfile, max_n_formulas=None)
    pruned_wfps = [(w, f) for w, f in weight_formula_pairs if abs(w) > 1e-9]

    print('Grounding {1} of total {0} formulas.'.format(len(weight_formula_pairs), len(pruned_wfps)))

    if not from_pbf:
        wfps = [(w, to_cnf(f)) for (w, f) in pruned_wfps]
    else:
        from pyference.mln import cnf_to_pbf
        from pyference.pbf_utils import monomials_from_pbf

        if transform:
            from pyference.compact_pbf import to_multinomial
            casting = to_multinomial
        else:
            casting = lambda x: x

        raw_formulas = []
        for ind, (w, f) in enumerate(pruned_wfps):
            if type(f) is Exists:
                raw_formulas += [(w, f)]
                continue

            try:
                m = monomials_from_pbf(-float(w) * casting(cnf_to_pbf(to_cnf(f), mode='flip_disj')))
                raw_formulas += m
            except:
                print (w, f, casting(cnf_to_pbf(to_cnf(f), mode='flip_disj')))
                raise
        wfps = raw_formulas

    fs_and_data = (wfps, db)
    print('After transformation: Grounding {} formulas.'.format(len(wfps)))
    res = build_with_sql(fs_and_data, redis_prefix='testsprel', build_fg=build_fg, filename=out, filename_grds=None, verbose=False)
    return res


def main():
    Run()


if __name__ == '__main__':
    main()
