from __future__ import print_function

import os, sys
sys.path.append('/usr/lib/pymodules/python2.7/')
sys.path.append('/usr/lib/python2.7/dist-packages/')
sys.path.append('/home/chris/lsrhome/pcl_stuff/exp/')
sys.path.append('..')
sys.path.append('/home/chris/lsrhome/code/nyu_dataset/')


PRED_NAMES = dict(zip('obl', 'On Behind LeftOf'.split()))
from mlnsupport.mln_training_call import train
train('db/sprel/', 'mlnfiles/sprel.mln', query_atoms=PRED_NAMES.values(), discriminative=False, add_unit_clauses=True)

